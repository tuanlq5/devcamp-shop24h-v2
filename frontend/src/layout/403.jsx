

export default function ForbiddenPage () {
    return(
        <>  
            <div className="forbidden-container">
                <p >OPPS! You don't have permission to visit this site</p>
                <div className="forbidden"></div>
            </div>
        </>
    )
}