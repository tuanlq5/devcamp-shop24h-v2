import NavbarAdmin from "../components/admin/Navbar"
import React, { useState } from "react"
import SideBarAdmin from "../components/admin/SideBar"
import AdminBreadcrump from "../components/admin/Breadcrumb";
import AdminContent from "../components/admin/Content";
import { CircularProgress } from "@mui/material";
import { Suspense } from "react";
import AdminFooter from "../components/admin/Footer";
import AdminRouteList from "../adminRoutes";

export default function AdminLayout () {
    const [openSidebar, setSideBar]  = useState(false);
    function toggleSideBar () {
        openSidebar ? setSideBar(false) : setSideBar(true)
    }
    return(
        <React.Fragment>
            {/* Navigation bar */}
            <NavbarAdmin setSideBar={toggleSideBar}/>
            {/* Admin Bread crumb */}
            <AdminBreadcrump/>
            {/* Admin Sidebar*/}
            <SideBarAdmin openSidebar={openSidebar} setSideBar={toggleSideBar}/>
            {/* Admin Content */}
            <Suspense 
                fallback={<div 
                style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    textAlign: "center",
                    minHeight: "100vh",
                }}><CircularProgress/></div>}
            >
                <AdminContent AdminRouteList={AdminRouteList}/>
            </Suspense>
            {/* Admin Content */}
            <AdminFooter/>
        </React.Fragment>
    )
}