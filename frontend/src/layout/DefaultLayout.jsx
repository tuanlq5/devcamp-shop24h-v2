import Footer from '../components/user/AppFooter';
import Navigator from '../components/user/Navigator';
import Content from '../components/user/AppContent';
import SideBar from '../components/user/AppSidebar';
import ShopBreadcrump from "../components/user/AppBreadcrumb";
import CircularProgress from '@mui/material/CircularProgress';
import "../styles/style.css";
import { Suspense } from 'react';
import React from 'react';
import RouteList from '../Routes';
function DefaultLayout() {

    return( 
        <React.Fragment>
            {/* Side bar */}
            <SideBar></SideBar>
            {/* Navbar */}
            <Navigator></Navigator>
            {/* Breadcrump */}
            <ShopBreadcrump></ShopBreadcrump>
            {/*Content*/}
            <Suspense fallback={<div 
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                textAlign: "center",
                minHeight: "100vh",
            }}> <CircularProgress /></div>}>
                <Content RouteList={RouteList}/>
            </Suspense>
            {/* footer */}
            <Footer></Footer>
        </React.Fragment>
    )
}

export default DefaultLayout