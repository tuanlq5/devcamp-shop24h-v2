import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

// TODO: Replace the following with your app's Firebase project configuration
// See: https://firebase.google.com/docs/web/learn-more#config-object
const firebaseConfig = {
    apiKey: "AIzaSyDGP21qG501en-MnA-4QVX1ezp-PuYZ8MQ",
    authDomain: "devcamp-firebase-6a51a.firebaseapp.com",
    projectId: "devcamp-firebase-6a51a",
    storageBucket: "devcamp-firebase-6a51a.appspot.com",
    messagingSenderId: "28614019334",
    appId: "1:28614019334:web:19895f1dc9f2ab1e32f687"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Firebase Authentication and get a reference to the service
const auth = getAuth(app);

export default auth;