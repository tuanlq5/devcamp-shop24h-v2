import GoogleIcon from '@mui/icons-material/Google';
import { GoogleAuthProvider, signInWithPopup, onAuthStateChanged} from "firebase/auth";
import auth from "../firebase";
import { useNavigate} from 'react-router-dom';
import { useDispatch, useSelector} from "react-redux";
import { signInGoogleAccountSuccessful } from "../actions/action";
import { onChangeEmailLogin, onChangePasswordLogIn, onClickLogInCustomer } from '../actions/action';
import {  useEffect } from 'react';
import axios from "axios";
const provider = new GoogleAuthProvider();

export default function LoginUser (){
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {userLogin, customerData, loginFail} = useSelector(state => state.productReducer);
    const onClickSignIn = () => {
        signInWithPopup(auth, provider)
            .then((res) => {
                onAuthStateChanged(auth, (user) => {
                    if(user){
                        user.getIdToken().then((token) => {
                            axios.post('http://localhost:8000/LoginGoogleAccount', {token})
                            .then((response)=> {
                                dispatch(signInGoogleAccountSuccessful(res.user));
                                navigate("/");
                            })
                            .catch((error) => {
                                console.log(error)
                            })
                        })
                    }
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }
    const onChangeEmail = (event) => {
        dispatch(onChangeEmailLogin(event.target.value));
    }

    const onChangePassword = (event) => {
        dispatch(onChangePasswordLogIn(event.target.value));
    }

    const handlerlogIn = ()=> {
        dispatch(onClickLogInCustomer(userLogin));
    }
    
    useEffect(()=> {
        if(JSON.stringify(customerData) !== JSON.stringify({})){
            navigate("/")
        }
    }, [customerData, navigate]);
    return(
        <div>
            <div className="card-container">
                <div className="card-login">
                    <h2>Log In Form</h2>
                    <button className="google-login" onClick={onClickSignIn}><GoogleIcon></GoogleIcon> &ensp; Log in with Google  </button>
                    <div className="middle-line"/>
                    <input onChange={onChangeEmail} type="text" placeholder="email"/>
                    <input onChange={onChangePassword} type="password" placeholder="password"/>
                    {loginFail  //Incase login is fail
                        ? 
                        <>
                            <p style={{color:"red"}}>
                                Email or password is in correct
                            </p>
                        </>
                        :
                        <></>
                    }
                    <button className="normal-login" onClick={handlerlogIn}>Log in</button>
                    
                </div>
            </div>
        </div>
    )
}