import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  brands} from '@fortawesome/fontawesome-svg-core/import.macro';
import SnackbarComponent from '../../components/user/SnackBar';
export default function HomePage(){
    
    return(
        <>
            <SnackbarComponent></SnackbarComponent>
            {/* <!-- header --> */}
            <section id="header">
                <div className="header-container">
                    <p>LOTUS FURNITURE</p>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor tenetur vel quod expedita</p>
                </div>
            </section>
            <section id="feature">
                <div className="feature-container">
                    <div>
                        <img src= {require("../../assets/images/icon/car-delivery-logo-vector-removebg-preview.png")} alt="Shipping">   
                        </img>
                        <p>fast Shipping</p>
                        <p>Delivery will transport within 24 hours</p>
                    </div>
                    <div>
                        <img src= {require("../../assets/images/icon/high-quality-removebg-preview.png")} alt="high quality">
                        </img>
                        <p>High Quality</p>
                        <p>We promise to deliver the highest quality for the price</p>
                    </div>
                    <div>
                        <img src={require("../../assets/images/icon/refund.png")} alt="refund">
                        </img>
                        <p>Refundable</p>
                        <p>Product will be return or refund if it had problem</p>
                    </div>
                    <div>
                        <img src={require("../../assets/images/icon/heart.png")} alt="heart">
                        </img>
                        <p>Favorite Brand</p>
                        <p>We pround to receive a lot of positive feed back from customer</p>
                    </div>
                </div>
            </section>
            {/* <!-- Collection --> */}
            <section id="Collection">
                <div className="collection-container">
                    <div className="collection-box1">
                        <img src= {require("../../assets/images/collection/desk.jpg")} alt="desk"/>
                        <p>Desk Collection</p>
                        <a href="/#">Shop now</a>
                    </div>
                    <div className="collection-box2">
                        <img src= {require("../../assets/images/collection/chair.jpg")} alt="chair"/>
                        <p>Chair Collection</p>
                        <a href="/#">Shop now</a>
                    </div>
                    <div className="collection-box3">
                        <img src= {require("../../assets/images/collection/bookshelves.jpg")} alt="bookshelves"/>
                        <p>Bookshelves Collection</p>
                        <a href="/#">Shop now</a>
                    </div>
                </div>
            </section>
            {/* <!-- Product --> */}
            <section id="Product">
                <div className="product-box">
                    <div className="product-card">
                        <img src= {require("../../assets/images/products/Sofa.jfif")} alt="1"/>
                        <p>Sofa Staring From <span>400$</span></p>
                        <a href="/#">SEE MORE &ensp;<ArrowForwardIosIcon></ArrowForwardIosIcon></a>
                        <div className="new-product">New</div>
                    </div>
                    <div className="product-card">
                        <img src= {require("../../assets/images/products/Table-1.jpg")} alt="2"/>
                        <p>Table Just Under <span>600$</span></p>
                        <a href="/#">SEE MORE  &ensp;<ArrowForwardIosIcon></ArrowForwardIosIcon></a>
                    </div>
                    <div className="product-card">
                        <img src= {require("../../assets/images/products/TV-stand.jpg")} alt="3"/>
                        <p>TV Desk Starting <span>300$</span></p>
                        <a href="/#">SEE MORE  &ensp;<ArrowForwardIosIcon></ArrowForwardIosIcon></a>
                        <div className="new-product">New</div>
                    </div>
                </div>
            </section>
            {/* <!-- Instagram --> */}
            <section id="instagram">
                <div className="instagram-container">
                    <div className="instagram-picture-box">
                        <img src= {require("../../assets/images/setup/set-up-1.jpg")} alt=""/>
                        <img src= {require("../../assets/images/setup/set-up-2.jpg")} alt=""/>
                        <img src= {require("../../assets/images/setup/set-up-3.jpg")} alt=""/>
                        <img src= {require("../../assets/images/setup/set-up-4.jpg")} alt=""/>
                    </div>
                    <div className="instagram-header">
                        <img src= {require("../../assets/images/logos/instagram-removebg-preview.png")} alt="instagram"/>
                        <a href="https://instagram.com"><FontAwesomeIcon icon={brands('instagram')} /></a>
                        <p>Share Your Setup With Our Community on Instagram </p>
                    </div>
                </div>
            </section>
            {/* <!-- Email --> */}
            <section id="email">
                <div className="email-container">
                    <p>Interested in Our New <span>Sales and Releases?</span></p>
                    <p>Sign up to get for better deal and support</p>
                    <input type="text" placeholder="Email..."/>
                    <button className="button-sign-up-email-home-page" >Sign Up</button>
                </div>
            </section>
        </>
    )
}