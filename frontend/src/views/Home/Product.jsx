import {Container,Row, Col, } from 'reactstrap';
import { useEffect } from 'react';
import { fetchDataProduct } from '../../actions/action';
import {useDispatch, useSelector} from "react-redux";
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
export default function Product () {
    const dispatch = useDispatch();
    const {products} = useSelector(state => state.productReducer);
    useEffect(()=> {
        dispatch(fetchDataProduct());
    }, [dispatch])
    return(
        <>
        <div>
            <Container className='Best-Seller'>
                <Row>
                    <Col lg={12} className="text-center">
                        <a href="/Products" className='catagories-link'>
                            Products  <ArrowForwardIcon></ArrowForwardIcon>
                        </a>
                    </Col>
                    <Col lg={12} className="mt-3">
                        <Row>
                            { products.map((items,index) => {
                                return (<Col lg={3} className="box mt-3 text-center" key={index}>
                                            <div className='card'>
                                                <a  href='/'><img className='selling-images' alt='123' key={items._id} src={items.imageURL}/></a>
                                                <p className='mt-2' key={items.type} >{`Name: ${items.name}`}</p>
                                                <p key={items.createAt}>{`Price: ${items.buyPrice}`}</p>
                                            </div>
                                        </Col>
                                        )
                                    }
                                )
                            }
                        </Row> 
                    </Col>
                </Row>
            </Container>
        </div>
        </>
    )
}