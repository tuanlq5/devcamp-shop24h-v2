import { Checkbox } from "@mui/material";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { 
    onChangePolicy, 
    onChangeCity, 
    onChangeAddress, 
    onChangeFullname, 
    onChangeCountry, 
    onChangeEmail, 
    onChangePassword, 
    onChangePhone,
    onChangeAdministrativeDivision,
    onChangeAdminCode,
    registerNewAdmin
} from "../../actions/action";
import {TextField, FormControl, InputLabel, Select, MenuItem, OutlinedInput} from "@mui/material";
import { useSelector } from "react-redux";
import {store} from "../../index";
import {Snackbar} from '@mui/material';
import MuiAlert from '@mui/material/Alert';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
      style: {
        maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      },
    },
};

const SnackbarAdminRegister = ({adminAccount,  registerErrorDuplicate, handleCloseRegisterErrorDuplicate, registerErrorCode ,handleCloseRegisterErrorAdmincode}) => {
    const [openRegisterAdmin, setOpenRegisterAdmin] = useState(false);
 
    
    const handleCloseRegister = () => {
        setOpenRegisterAdmin(false);
    }
    
    useEffect(()=> {
        if(adminAccount === true){
            setOpenRegisterAdmin(true)
        }else{
            setOpenRegisterAdmin(false)
        }
    }, [adminAccount])
    return(
        <>
            {/* Snackbar when user Login  */}
            <Snackbar open = {openRegisterAdmin} autoHideDuration = {3000} onClose = {handleCloseRegister}>
                <MuiAlert  elevation={6}  variant="filled" severity='success' sx={{width:"100%", backgroundColor:"rgb(104 90 53)"}}>
                    Register successful!
                </MuiAlert>
            </Snackbar>
            <Snackbar open = {registerErrorDuplicate} autoHideDuration = {3000} onClose = {handleCloseRegisterErrorDuplicate}>
                <MuiAlert  elevation={6}  variant="filled" severity='error' sx={{width:"100%", backgroundColor:"red"}}>
                    Your Email or Phone is duplicate!
                </MuiAlert>
            </Snackbar>
            <Snackbar open = {registerErrorCode} autoHideDuration = {3000} onClose = {handleCloseRegisterErrorAdmincode}>
                <MuiAlert  elevation={6}  variant="filled" severity='error' sx={{width:"100%", backgroundColor:"red"}}>
                    Your Admin Code is not correct!
                </MuiAlert>
            </Snackbar>
        </>
    )
}
  
export default function RegisterAdmin () {
    const dispatch = useDispatch();
    const { adminAccount} = useSelector(state => state.productReducer);
    const [policyChecked, setPolicyChecked] = useState(false);
    const [countryData, setCountryData] = useState([]); 
    const [administrativDivisionData, setAdministrativeDivisionData] = useState([]);
    const [country, setCountry] = useState("");
    const [administrativeDivision, setAdministrativeDivision] = useState("");
    const [validateFullName, setValidateFullName] = useState(false);
    const [validatePhone, setValidatePhone] = useState(false);
    const [validateEmail, setValidateEmail] = useState(false);
    const [validatePassword, setValidatePassword] = useState(false);
    const [validateAddress, setValidateAddress] = useState(false);
    const [validateCountry, setValidateCountry] = useState(false);
    const [validateAdministrativeDivision, setValidateAdministrativeDivision] = useState(false);
    const [validateCity, setValidateCity] = useState(false);
    const [validateAdminCode, setValidateAdminCode] = useState(false);
    const [validatePolicy, setvalidatePolicy] = useState(false);
    const [registerErrorDuplicate, setRegisterErrorDuplicate] = useState(false);
    const [registerErrorCode, setRegisterErrorCode] = useState(false);
    //đóng snack bar regsiter error duplicate
    const  handleCloseRegisterErrorDuplicate = () => {
        setRegisterErrorDuplicate(false);
    }

    //đóng snack bar regsiter error admin code
    const  handleCloseRegisterErrorAdmincode = () => {
        setRegisterErrorCode(false);
    }
    //Lưu dữ liệu khi người dungg tích policy
    const handlerPolicyChecked = (event) => {
        setPolicyChecked(event.target.checked)
        if(!policyChecked){
            dispatch(onChangePolicy(true))
        } else{
            dispatch(onChangePolicy(false))
        }
    }
    //lưu dữ liệu khi người dung nhập fullname
    const handlerFullName = (event) => {
        dispatch(onChangeFullname(event.target.value));
    }
    //Lưu dữ liệu khi người dung nhập email
    const handlerEmail = (event) => {
        dispatch(onChangeEmail(event.target.value));
    }
    //Lưu dữ liệu khi người dùng nhập Phone
    const handlerPhone = (event) => {
        dispatch(onChangePhone(event.target.value));
    }

    //lưu dữ liệu khi người dùng nhập password
    const handlerPassword =  (event) => {
        dispatch(onChangePassword(event.target.value));
    }

    //lưu dữ liệu khi người dùng nhập address
    const handlerAddress = (event) => {
        dispatch(onChangeAddress(event.target.value));
    } 

    //Lưu dữ liệu khi người dùng nhập city
    const handlerCity = (event) => {
        dispatch(onChangeCity(event.target.value));
    }

    //Lưu dữ liệu khi người dùng nhập country
    const handlerCountry  = (event) => {
        setCountry(event.target.value);
        dispatch(onChangeCountry(event.target.value));
    }

    //Lưu giữ liệu khi người dùng chọn administrative
    const handleChangeAdministrativeDivision = (event) => {
        dispatch(onChangeAdministrativeDivision(event.target.value));
        setAdministrativeDivision(event.target.value);
    }

    //khi admin nhập admin code
    const handlerAdminCode = (event) => {
        dispatch(onChangeAdminCode(event.target.value));
    }

    //khi người dùng ấn nút Register
    const handlerRegister = () => {
        const state = store.getState();
        const {customerRegister} = state.productReducer;
        // Validate the input
        if(customerRegister.fullName === ""){
            setValidateFullName(true)
            return false;
        }else{ 
            setValidateFullName(false)
        }
        if(customerRegister.phone === ""){
            setValidatePhone(true)
            return false;
        }else{
            setValidatePhone(false)
        }
        if(customerRegister.email === ""){
            setValidateEmail(true)
            return false;
        }else{
            setValidateEmail(false)
        }
        if(customerRegister.password === ""){
            setValidatePassword(true)
            return false;
        }else{
            setValidatePassword(false)
        }
        if(customerRegister.address === ""){
            setValidateAddress(true)
            return false;
        }else{
            setValidateAddress(false)
        }
        if(customerRegister.country === ""){
            setValidateCountry(true)
            return false;
        }else{
            setValidateCountry(false)
        }
        if(customerRegister.administrativeDivision === ""){
            setValidateAdministrativeDivision(true)
            return false;
        }else{
            setValidateAdministrativeDivision(false)
        }
        if(customerRegister.city === ""){
            setValidateCity(true)
            return false;
        }else{
            setValidateCity(false)
        }
        if(customerRegister.adminCode === ""){
            setValidateAdminCode(true)
            return false;
        }else{
            setValidateAdminCode(false)
        }
        if(!policyChecked){
            setvalidatePolicy(true);
            return false
        }else{
            setvalidatePolicy(false);
        }
        dispatch(registerNewAdmin(customerRegister));
    }

    //Lấy dữ liệu country
    const handleRequestCountryData = async (err, data ) => {
        try{
            const requestCountryData = await fetch ("http://localhost:8000/country", {method:"GET"});
            const data = await requestCountryData.json();
            setCountryData(data.data);
        }catch(err){
            console.log(err);
        }
    }
    useEffect(()=> {
        handleRequestCountryData();
        //change Adminstrative division selection when changing country 
        if(country === "None"){
            setAdministrativeDivision("");
            setAdministrativeDivisionData([]);
        }
        for (let index = 0; index < countryData.length; index++){
            if(countryData[index].code === country){
                setAdministrativeDivisionData(countryData[index].administrativeDivision);
            }
        }

    },[country, countryData])

    useEffect(()=> {
        console.log(adminAccount)
        if(adminAccount === 500){
            setRegisterErrorDuplicate(true)
        }else{
            setRegisterErrorDuplicate(false)
        }
        if(adminAccount === 400){
            setRegisterErrorCode(true);
        }else{
            setRegisterErrorCode(false)
        }
    },[adminAccount])
    return(
        <>  
            <SnackbarAdminRegister 
            adminAccount={adminAccount} 
            registerErrorDuplicate={registerErrorDuplicate} 
            handleCloseRegisterErrorDuplicate={handleCloseRegisterErrorDuplicate}
            registerErrorCode={registerErrorCode}
            handleCloseRegisterErrorAdmincode={handleCloseRegisterErrorAdmincode}
            ></SnackbarAdminRegister>

            <div className="register-box">
                <div className="register-inlustrate"></div>
                <div className="register-container">
                    <h2 style={{marginBottom: "30px", marginRight:"-10px"}}>Register Form</h2>
                    <div className="fullname-register">
                        <TextField label="Fullname*" style={{
                            width:"100%"
                        }}  onChange={handlerFullName}/>
                        {validateFullName? <p style={{color: "red", width: "100%"}}>Fullname is empty!</p>: <></>}
                    </div>
                    <div className="phone-register">
                        <TextField label="Phone*" style={{
                            width:"100%"
                        }}  onChange={handlerPhone}/>
                        {validatePhone? <p style={{color: "red", width: "100%"}}>phone is empty!</p>: <></>}
                    </div>
                    <div className="email-register">
                        <TextField label="Email*" style={{
                            width:"100%"
                        }}  onChange={handlerEmail}/>
                        {validateEmail? <p style={{color: "red", width: "100%"}}>Email is empty!</p>: <></>}
                    </div>
                    <div className="password-register">
                        <TextField label="Password*" type="password" style={{
                            width:"100%"
                        }}  onChange={handlerPassword}/>
                        {validatePassword? <p style={{color: "red", width: "100%"}}>Password is empty!</p>: <></>}
                    </div>
                    <div className="address-register">
                        <TextField label="Address*" style={{
                            width:"100%"
                        }}  onChange={handlerAddress}/>
                        {validateAddress? <p style={{color: "red", width: "100%"}}>Address is empty!</p>: <></>}
                    </div>
                    <div className="country-register">
                        <FormControl sx={{  minWidth: "100%" }} size="small">
                            <InputLabel sx={{ translate: "-5px" }}>Country</InputLabel>
                            <Select
                                value={country}
                                label="Country"
                                input={<OutlinedInput label="Country" />}
                                onChange={handlerCountry}
                                MenuProps={MenuProps}
                            >
                                {countryData.map((element,index) => 
                                    <MenuItem value={element.code} key={index}>
                                        <em>{element.name}</em>
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                        {validateCountry? <p style={{color: "red", width: "100%"}}>Country is empty!</p>: <></>}
                    </div>
                    <div className="city-register">
                        <FormControl sx={{  minWidth: "100%" }} size="small">
                            <InputLabel id="demo-select-small" sx={{ translate: "-5px" }}>Adminitrative Division</InputLabel>
                                <Select
                                    value={administrativeDivision}
                                    label="Adminitrative Division"
                                    input={<OutlinedInput label="Adminitrative Division" />}
                                    onChange={handleChangeAdministrativeDivision}
                                    MenuProps={MenuProps}
                                >
                                    {administrativDivisionData.map((element,index) => 
                                        <MenuItem value={element} key={index}>
                                            <em>{element}</em>
                                        </MenuItem>
                                    )}
                            </Select>
                        </FormControl>
                        {validateAdministrativeDivision? <p style={{color: "red", width: "100%"}}>Adminitrative Division is empty!</p>: <></>}
                    </div>
                    <div className="city-register">
                        <TextField label="City*" style={{
                            width:"100%"
                        }}  onChange={handlerCity} />
                        {validateCity? <p style={{color: "red", width: "100%"}}>City is empty!</p>: <></>}
                    </div>
                    <div className="password-register">
                        <TextField label="AdminCode*" style={{
                            width:"100%"
                        }}  onChange={handlerAdminCode} />
                        {validateAdminCode? <p style={{color: "red", width: "100%"}}>AdminCode is empty!</p>: <></>}
                    </div>
                    <div className="term-policy-register">
                        <Checkbox  
                            checked={policyChecked}
                            onChange={handlerPolicyChecked}
                            sx={{ 
                                color: "rgb(68, 63, 63)",
                                '&.Mui-checked': {
                                color:'rgb(97, 94, 68)',
                                },

                            }}>
                        </Checkbox><p>I have read and accepted the&ensp;<a href="/#">privacy policy</a>. </p>
                    </div>
                    {validatePolicy ? <p style={{color: "red", width: "100%"}}>You have to accept the policy</p> :  <></> } 
                    <div className="register-button">
                        <a href="/Login">I already have a admin account</a>
                        <button onClick={handlerRegister}>Continue</button>
                    </div>
                </div>
            </div>
        </>
    )
} 