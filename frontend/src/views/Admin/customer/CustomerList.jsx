import React, { useState, useEffect } from "react";
import TablePaginationComponent from "../../../components/admin/Table/TablePanigation";
import { useSelector, useDispatch } from "react-redux";
import { loadCustomerData } from "../../../actions/admin/admin.customerAction";
import { TableBody, TableCell, tableCellClasses, TableRow, } from "@mui/material";
import { styled } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import CustomerModal from "../../../components/admin/Modal/CustomerModal";


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

export default function CustomerList (){
    const dispatch = useDispatch();
    const {customerList, customerLength} = useSelector(state => state.adminCustomerReducer);
    const [currentPage, setCurrentPage] = useState(0);
    const [rowPerPage, setRowsPerPage] = useState(5);
    const [openEditModal, setEditModal] = useState(false);
    const [openDeleteModal, setDeleteModal] = useState(false);
    const [customerInfo, setCustomerInfo] = useState();
    const rowPerPageOption = [5, 10, 15];
    //funciton handle change page number
    const handleChangePage = (event, value) => {
        setCurrentPage(value);
    }

    //function handle change row per page
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
    }   

    const handleOpenEditModal = (value) => {
        setEditModal(true);
        setCustomerInfo(value)
    }

    const handleCloseEditModal = () => {
        setEditModal(false);
    }

    const handleOpenDeleteModal = (value) => {
        setDeleteModal(true);
        setCustomerInfo(value);
    }

    const handleCloseDeleteCustomerModal = () => {
        setDeleteModal(false);
    }

    //Columns header content
    const columns = [
        { id: 'STT', label: 'Index', minWidth: 50, align:'center'},
        { id: 'fullName', label: 'Fullname', minWidth: 100, align:'center' },
        {
          id: 'Phone',
          label: 'Phone',
          minWidth: 130,
          align: 'center',
        },
        {
          id: 'Email',
          label: 'Email',
          minWidth: 130,
          align: 'center',
        },
        {
            id: 'Adress',
            label: 'Adress',
            minWidth: 150,
            align: 'center',
        },
        {
            id: 'Country',
            label: 'Country',
            minWidth: 100,
            align: 'center',
        },
        {
            id: 'Administrative Division',
            label: 'Administrative Division',
            minWidth: 150,
            align: 'center',
        },
        {
            id: 'City',
            label: 'City',
            minWidth: 100,
            align: 'center',
        },
        {
            id: 'Action',
            label: 'Action',
            minWidth: 150,
            align: 'center',
        },
    ];


    useEffect (()=> {
        dispatch(loadCustomerData(rowPerPage, currentPage))
    },[dispatch, rowPerPage, currentPage])

    //Table Content
    const TableRowComponent = () => {
        return(
            <TableBody>
                {customerList.map((row, index)=>{
                    return(
                        <React.Fragment key={index}>
                            <StyledTableRow hover role="checkbox" >
                                <StyledTableCell align='center'>
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell >
                                    {row.fullName}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.phone}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.email}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.address}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.country}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.administrativeDivision}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.city}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    <button className='table-admin-button-edit' onClick={() =>handleOpenEditModal(row)} ><EditIcon/></button>
                                    <button className='table-admin-button-delete' onClick={() => handleOpenDeleteModal(row)}><DeleteIcon/></button>
                                </StyledTableCell>
                            </StyledTableRow>
                        </React.Fragment>
                        )
                    }) 
                }
            </TableBody>
        )
    }

    return(
        <>
            <CustomerModal customerInfo={customerInfo} openEditModal ={openEditModal} handleCloseEditModal={handleCloseEditModal} deleteCustomerModal={openDeleteModal}  handleCloseDeleteCustomerModal= {handleCloseDeleteCustomerModal}></CustomerModal>
            <TablePaginationComponent 
                columns={columns}
                currentPage = {currentPage}
                rowPerPage = {rowPerPage}
                rowPerPageOption = {rowPerPageOption} 
                TableRowComponent = {TableRowComponent}
                handleChangePage = {handleChangePage}
                handleChangeRowsPerPage = {handleChangeRowsPerPage}
                length = {customerLength}
                headerColor= {"#E7D284"}
                Height = {"400px"}
            />
        </>
    )
}