import React, { useState, useEffect } from "react";
import TablePaginationComponent from "../../../components/admin/Table/TablePanigation";
import { useSelector, useDispatch } from "react-redux";
import { loadVoucherData } from "../../../actions/admin/admin.couponAction";
import { TableBody, TableCell, tableCellClasses, TableRow} from "@mui/material";
import { styled } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import CouponModal from "../../../components/admin/Modal/CouponModal";
import { updateEditCouponStatus, updateDeleteCouponStatus } from "../../../actions/admin/admin.couponAction";
const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

export default function CouponList (){
    const dispatch = useDispatch();
    const {couponList, couponLength} = useSelector(state => state.adminCouponReducer);
    const [currentPage, setCurrentPage] = useState(0);
    const [rowPerPage, setRowsPerPage] = useState(5);
    const [editCoupon, setEditCoupon] = useState(false);
    const [deleteCoupon, setDeleteCoupon] = useState(false);
    const [couponData, setCouponData] = useState({});
    const rowPerPageOption = [5, 10, 15];

    //funciton handle change page number
    const handleChangePage = (event, value) => {
        setCurrentPage(value);
    }

    //function handle change row per page
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
    }   

    //function open delete coupon modal
    const handleShowDeleteCouponModal = (value) => {
        dispatch(updateDeleteCouponStatus())
        setCouponData(value)
        setDeleteCoupon(true)
    }

    //fuction handle Close delete coupon modal
    const handleCloseDeleteCounponModal =() => {
        setDeleteCoupon(false)
    }

    //function open edit coupon modal
    const handleShowEditCouponModal = (value) => {
        dispatch(updateEditCouponStatus());
        setCouponData(value)
        setEditCoupon(true)
    }

    //function close edit coupon modal
    const handleCloseEditCouponModal = () => {
        setEditCoupon(false)
    }

    //Columns header content
    const columns = [
        { id: 'STT', label: 'Index', minWidth: 50, align:'center'},
        { id: 'fullName', label: 'Fullname', minWidth: 100, align:'center' },
        {
          id: 'Voucher Code',
          label: 'Coupon Code',
          minWidth: 130,
          align: 'center',
        },
        {
          id: 'Update Time',
          label: 'Update Time',
          minWidth: 130,
          align: 'center',
        },
        {
            id: 'Action',
            label: 'Action',
            minWidth: 150,
            align: 'center',
        },
    ];

    useEffect (()=> {
        dispatch(loadVoucherData(rowPerPage, currentPage))
    },[dispatch, rowPerPage, currentPage])

    //Table Content
    const TableRowComponent = () => {
        return(
            <TableBody>
                {couponList.map((row, index)=>{
                    return(
                        <React.Fragment key={index}>
                            <StyledTableRow hover role="checkbox" >
                                <StyledTableCell align='center'>
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell  align='center'>
                                    {row.voucherCode}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.discountPercent}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.updatedAt.slice(0,16)}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    <button className='table-admin-button-edit' onClick={() => handleShowEditCouponModal(row)} ><EditIcon/></button>
                                    <button className='table-admin-button-delete' onClick={() => handleShowDeleteCouponModal(row)}><DeleteIcon/></button>
                                </StyledTableCell>
                            </StyledTableRow>
                        </React.Fragment>
                        )
                    }) 
                }
            </TableBody>
        )
    }

    return(
        <>
            <TablePaginationComponent 
                columns={columns}
                currentPage = {currentPage}
                rowPerPage = {rowPerPage}
                rowPerPageOption = {rowPerPageOption} 
                TableRowComponent = {TableRowComponent}
                handleChangePage = {handleChangePage}
                handleChangeRowsPerPage = {handleChangeRowsPerPage}
                length = {couponLength}
                headerColor= {"#E7D284"}
                Height = {"300px"}
            />
            <CouponModal couponData ={couponData} openEditModal ={editCoupon} handleCloseEdit={handleCloseEditCouponModal} deleteCoupon={deleteCoupon} handlecloseDelete ={handleCloseDeleteCounponModal}></CouponModal>
        </>
    )
} 