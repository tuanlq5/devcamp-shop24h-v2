import { loadOrderList } from '../../../actions/admin/admin.orderAction';
import { useDispatch, useSelector} from "react-redux";
import { useEffect, useState } from 'react';
import TablePagination from '../../../components/admin/Table/TablePanigation';
import { TableCell, TableBody, TableRow, FormControl, Select, MenuItem, Box, Typography, Collapse, Table, TableHead, tableCellClasses} from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown"
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp"
import { IconButton } from "@mui/material";
import { styled } from '@mui/material/styles';
import { onChangeStatusOrder } from '../../../actions/admin/admin.orderAction';
import React from 'react';
import OrderModal from '../../../components/admin/Modal/OrderModal';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

function TableContentComponent ({row, index, handleDeleteOrder, handleEditOrderStatus}){
    const dispatch = useDispatch();
    const [open, setOpen] = useState(false);   

    //function truncate a string to specific length
    function truncate(str, n){
        return (str.length > n) ? str.slice(0, n-1) + '....' : str;
    };    

    //handle change status order
    const handleChangeOrderStatus = (orderObject, event) => {
        dispatch(onChangeStatusOrder(event.target.value, orderObject));
    }
    
    return(
            <React.Fragment key={index}>
                <StyledTableRow hover role="checkbox"  >
                    <StyledTableCell>
                            <IconButton
                                aria-label="expand row"
                                size="small"
                                onClick={() => setOpen(!open)}
                            >
                                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                            </IconButton>
                    </StyledTableCell>
                    <StyledTableCell align='center'>
                        {index + 1}
                    </StyledTableCell>
                    <StyledTableCell >
                        {row.orderDate.slice(0,10)}
                    </StyledTableCell>
                    <StyledTableCell align='center'>
                       {
                        row.customer === undefined? "Tuan" 
                        : row.customer.fullName
                       }
                    </StyledTableCell>
                    <StyledTableCell align='center'>
                        {row.cost}
                    </StyledTableCell>
                    <StyledTableCell align='center'>
                        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                            <Select
                            labelId="demo-simple-select-standard-label"
                            id="demo-simple-select-standard"
                            value={row.status}
                            onChange={(event) => handleChangeOrderStatus(row, event)}
                            label="Age"
                            >
                                <MenuItem value={"Confirmed"}>Confirmed</MenuItem>
                                <MenuItem value={"Canceled"}>Canceled</MenuItem>
                                <MenuItem value={"Open"}>Open</MenuItem>
                                <MenuItem value={"Completed"}>Completed</MenuItem>
                            </Select>
                        </FormControl>
                    </StyledTableCell>
                    <StyledTableCell align='center'>
                        <button className='table-admin-button-edit' onClick={() =>handleEditOrderStatus(row)}><EditIcon/></button>
                        <button className='table-admin-button-delete' onClick={() => handleDeleteOrder(row)}><DeleteIcon/></button>
                    </StyledTableCell>
                </StyledTableRow>
                <StyledTableRow   >
                    <StyledTableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Box sx={{ margin: 1 }}>
                            <Typography variant="h6" gutterBottom component="div">
                                Order Detail
                            </Typography>
                            <Table size="small" aria-label="purchases">
                                <TableHead>
                                <StyledTableRow>
                                    <StyledTableCell align="center">STT</StyledTableCell>
                                    <StyledTableCell align="center">Product Name</StyledTableCell>
                                    <StyledTableCell align="center">Quantity</StyledTableCell>
                                    <StyledTableCell align="center">Price</StyledTableCell>
                                </StyledTableRow>
                                </TableHead>
                                <TableBody>
                                { 
                                    row.orderDetail.map((element, index) => {
                                        return(
                                            <StyledTableRow key={index}>
                                                <StyledTableCell  align="center">
                                                    {index + 1}
                                                </StyledTableCell>
                                                <StyledTableCell align="center">{truncate(element.product.name, 14)}</StyledTableCell>
                                                <StyledTableCell align="center">{element.quantity}</StyledTableCell>
                                                <StyledTableCell align="center">
                                                    {element.product.buyPrice}
                                                </StyledTableCell>
                                            </StyledTableRow>
                                        )
                                    })

                                }
                                </TableBody>
                            </Table>
                            </Box>
                        </Collapse>
                    </StyledTableCell>
                </StyledTableRow>
            </React.Fragment>
    )
}

export default function OrderList (){
    const dispatch = useDispatch();
    const {orderList, orderLength} = useSelector(state => state.adminOrderReducer);
    const [currentPage, setCurrentPage] = useState(0);
    const [rowPerPage, setRowsPerPage] = useState(5);
    const [openEditModal, setEditModal] = useState(false);
    const [deleteOrderModal, setDeleteOrderModal] = useState(false);
    const [orderObject, setOrderObject] = useState({});
    const rowPerPageOption = [5, 10, 15];
    //funciton handle change page number
     const handleChangePage = (event, value) => {
        setCurrentPage(value);
    }

    //function handle change row per page
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
    }   

    //handle Close Edit order Modal
    const handleCloseEditModal = () => {
        setEditModal(false);
    }

    //handle Edit order status
    const handleEditOrderStatusModal = (orderObject) => {
        setEditModal(true);
        setOrderObject(orderObject);
    }

    //handle Delete order modal
    const handleDeleteOrderModal = (orderObject) => {
        setDeleteOrderModal(true);
        setOrderObject(orderObject);
    }

    //handle Close Delete order modal
    const handleCloseDeleteOrderModal = () => {
        setDeleteOrderModal(false);
    }

    //Columns header content
    const columns = [
        { id: "OrderDetail", label:"", minWidth:50},
        { id: 'STT', label: 'Index', minWidth: 50, align:'center'},
        { id: 'orderDate', label: 'Order Date', minWidth: 100 },
        {
          id: 'customer.address',
          label: 'Customer Name',
          minWidth: 170,
          align: 'center',
        },
        {
          id: 'cost',
          label: 'Cost',
          minWidth: 170,
          align: 'center',
        },
        {
            id: 'status',
            label: 'Status',
            minWidth: 170,
            align: 'center',
        },
        {
            id: 'Action',
            label: 'Action',
            minWidth: 170,
            align: 'center',
        },
    ];
    useEffect(()=> {
        dispatch(loadOrderList(rowPerPage, currentPage));
        
    },[dispatch, rowPerPage, currentPage])

    const TableRowComponent = ()=>{
        return(
            <TableBody>
                {orderList
                .map((row, index) => {
                    return (
                        <React.Fragment key={index}>
                            <TableContentComponent row={row} index={index} handleDeleteOrder = {handleDeleteOrderModal} handleEditOrderStatus = {handleEditOrderStatusModal}/>
                        </React.Fragment>
                    );
                })}
            </TableBody>
        )
    }
      
    return(
        <>
            <OrderModal 
                openEditModal = {openEditModal}
                deleteOrderModal = {deleteOrderModal}
                handleCloseEditModal = {handleCloseEditModal}
                handleCloseDeleteOrderModal= {handleCloseDeleteOrderModal}
                orderObject={orderObject}
                ></OrderModal>
            <TablePagination 
                columns={columns}
                currentPage = {currentPage}
                rowPerPage = {rowPerPage}
                rowPerPageOption = {rowPerPageOption} 
                handleChangePage = {handleChangePage}
                handleChangeRowsPerPage = {handleChangeRowsPerPage}
                TableRowComponent = {TableRowComponent}
                orderList = {orderList}
                length = {orderLength}
                headerColor= {"#E7D284"}
                Height = {"470px"}
            />   
        </>
    )
}