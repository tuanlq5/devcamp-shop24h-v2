import LineChart from "../../../components/admin/Chart/lineChart";
// import BarChart from "../../../components/admin/Chart/barChart";
import DonutChart from "../../../components/admin/Chart/donutChart";
// import RadarChart from "../../../components/admin/Chart/radarChart";
// import BubbleChart from "../../../components/admin/Chart/bubbleChart";
// import PolarAreaChart from "../../../components/admin/Chart/polarAreaChart";
// import ScatterChart from "../../../components/admin/Chart/scatterChart";
import WorldMapChart from "../../../components/admin/Chart/worldMapChart";
import { useEffect, useState } from "react";
import {useSelector} from "react-redux";
import TrendingUpIcon from '@mui/icons-material/TrendingUp';
import TrendingDownIcon from '@mui/icons-material/TrendingDown';
import PaidIcon from '@mui/icons-material/Paid';
import PersonAddAltIcon from '@mui/icons-material/PersonAddAlt';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import PersonIcon from '@mui/icons-material/Person';
import { Tooltip } from "react-tooltip";
import HorizontalBarChart from "../../../components/admin/Chart/horizontalBarChart";
import ComboChart from "../../../components/admin/Chart/comboChart";
import "react-tooltip/dist/react-tooltip.css";
import TablePaginationComponent from "../../../components/admin/Table/TablePanigation";
import {  useDispatch } from "react-redux";
import { TableBody, TableCell, tableCellClasses, TableRow, } from "@mui/material";
import { styled } from '@mui/material/styles';
import React from "react";
import { loadOrderList } from '../../../actions/admin/admin.orderAction';
import { json2csv } from 'json-2-csv';
import { sendRequestAllOrder, sendRequestComboChartData } from "../../../actions/admin/admin.dataAnalyzeAction";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  // [`&.${tableCellClasses.head}`]: {
  //   backgroundColor: theme.palette.common.black,
  //   color: theme.palette.common.white,
  //   padding: "12px"
  // },n
  [`&.${tableCellClasses.body}`]: {
    fontSize: 13,
    padding: "13px"
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const Overview = () => {
  const [greeting, setGreeting] = useState("Good Morning");
  const {customerData, googleAccount} = useSelector(state => state.productReducer);
  const {
    monthlySale, 
    orderData, 
    monthlyOrder, 
    percentageSaleLastMonthComparison, 
    percentageOrderLastMonthComparison, 
    percentageCustomerLastMonthComparison,
    addProductLabel,
    addProductData,
    userActivities,
    orderCancel,
    acceptedOrder,
    refundOrder,
    orderTime,
    donutChartData,
  } = useSelector(state => state.dataAnalyzeReducer);
  const [country, setCountry] = useState("");
  const time = new Date();
  const hours = time.getHours();

  // Greeting on time
  useEffect(()=>{
    if(hours >= 1 && hours >= 12){
      setGreeting("Good Morning")
    }
    if(hours > 12 && hours <= 18){
      setGreeting("Good Afternoon")
    }
    if(hours > 18 && hours <= 24){
      setGreeting("Good Everning")
    }
   
    
  },[hours])

  const dispatch = useDispatch();
  const {orderList, orderLength, } = useSelector(state => state.adminOrderReducer);
  const [earnings, setEarning] = useState(0);
  const [conversionRate, setConversionrate] = useState(0);
  const [orderSuccess, setAcceptedOrder] = useState([]);
  const [orderCanceled, setCanceledOrder] = useState([]);
  const [orderMonth, setOrderMonth] = useState([]);
  const [comboChartName] = useState("Number of Order in Year");
  //set state for combo table data when it is update
  useEffect(() => {
    setAcceptedOrder(acceptedOrder);
    setCanceledOrder(refundOrder);
    setOrderMonth(orderTime);
  },[acceptedOrder, refundOrder, orderTime]);

  // Bar chart data
  useEffect(() => {
    // set Earning data
    let cost = 0;
    for(let i = 0; i < orderData.length; i++){
      cost += orderData[i].cost
    }
    setEarning(Math.round((cost)*100)/100);

    // set conversion rate
    let viewProductCount = 0;
    
    for(let i =0 ;i < userActivities.length; i++){
      if(userActivities[i].productView.length !== undefined){
      viewProductCount += userActivities[i].productView.length;
      }
    }
    setConversionrate(() => {
      return(Math.round((((orderData.length/ viewProductCount)*100)*100)/100))
    })
  }, [orderData, orderList, userActivities])
  
  // ComboChart Data
  useEffect(() => {
      dispatch(sendRequestComboChartData());
  }, [dispatch])

  const [currentPage, setCurrentPage] = useState(0);
  const [rowPerPage, setRowsPerPage] = useState(5);
  const rowPerPageOption = [5, 10, 15];
  //funciton handle change page number
  const handleChangePage = (event, value) => {
      setCurrentPage(value);
  }

  //function handle change row per page
  const handleChangeRowsPerPage = (event) => {
      setRowsPerPage(event.target.value);
  }   

  //Columns header content
  const columns = [
    { id: 'STT', label: 'Index', minWidth: 30, align:'center'},
    { id: 'orderDate', label: 'Order Date', minWidth: 70 , align:"center"},
    {
      id: 'customer.name',
      label: 'Customer Name',
      minWidth: 120,
      align: 'center',
    },
    {
      id: 'cost',
      label: 'Cost',
      minWidth: 60,
      align: 'center',
    },
    {
        id: 'status',
        label: 'Status',
        minWidth: 60,
        align: 'center',
    },
  ];

  //option for csv file (json2csv)
  let options = {
    delimiter : {
        wrap  : '"', // Double Quote (") character
        field : ',', // Comma field delimiter
        eol   : '\n' // Newline delimiter
    },
    prependHeader    : true,
    sortHeader       : false,
    excelBOM         : true,
    trimHeaderValues : true,
    trimFieldValues  : true,
    keys             : ['_id', 'orderDetail.product._id', 'customer._id','cost' ,'orderDate', 'status']
  };

  //download CSV
  const downloadCSV = (fileName, csvData) => {
    const blob = new Blob([csvData], { type: 'text/csv' });
    const downloadUrl = URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = downloadUrl;
    link.download = fileName;
    // const fileSize = blob.size;
    link.click();
    URL.revokeObjectURL(downloadUrl);
  }
  
  //convert array to csv
  const handleExportOrder = async() => {
    const csv = await json2csv(orderData, options); 
    downloadCSV("order.csv", csv);
  }

  //request to take all order and userActivities;
  useEffect(()=>{
    dispatch(sendRequestAllOrder());
  },[dispatch]);


  //load order List by page 
  useEffect(()=> {
      dispatch(loadOrderList(rowPerPage, currentPage));
  },[dispatch, rowPerPage, currentPage])

  //Table Content
  const TableRowComponent = () => {
      return(
          <TableBody>
              {orderList.map((row, index)=>{
                  return(
                      <React.Fragment key={index}>
                          <StyledTableRow hover role="checkbox" >
                              <StyledTableCell align='center'>
                                  {index}
                              </StyledTableCell>
                              <StyledTableCell align='center'>
                                  {row.orderDate.slice(0,10)}
                              </StyledTableCell>
                              <StyledTableCell align='center'>
                                  {row.customer.fullName}
                              </StyledTableCell>
                              <StyledTableCell align='center'>
                                  {row.cost} $
                              </StyledTableCell>
                              <StyledTableCell align='center'>
                                  {row.status}
                              </StyledTableCell>
                          </StyledTableRow>
                      </React.Fragment>
                      )
                  }) 
              }
          </TableBody>
      )
  }
  return(
      <div className="overview-conatiner">
        <div className="overview-box1">
            <div className="overview-box1-1">
              <div>
                <p>{greeting}</p>
                {
                  JSON.stringify(customerData) !== JSON.stringify({}) ? <p>{customerData.fullName}</p>:<></>
                }
                {
                  JSON.stringify(googleAccount) !== JSON.stringify({}) ? <p>{googleAccount}</p>:<></>
                }
                <p>Good To See You Today 😀</p>
              </div>
              <div>
                <p>Monthly Sale</p>
                <p>$ {monthlySale}</p>
                <section>
                  { percentageSaleLastMonthComparison > 0 
                    ?
                    <p style={{color:"rgb(13, 234, 160)"}}><TrendingUpIcon sx={{ fontSize: 40}}/>{percentageSaleLastMonthComparison}%</p>
                    :
                    <p style={{color:"rgb(229, 34, 20)"}}><TrendingDownIcon sx={{ fontSize: 40}}/>{percentageOrderLastMonthComparison}%</p>
                  }
                  <p><PaidIcon fontSize="medium"/></p>
                </section>
              </div>
              <div>
              <p>Monthly Orders</p>
                <p><AddShoppingCartIcon/> {monthlyOrder}</p>
                <section>
                  {
                    percentageOrderLastMonthComparison > 0
                    ?
                    <p style={{color:"rgb(13, 234, 160)"}}><TrendingUpIcon sx={{ fontSize: 40 }}/>{percentageOrderLastMonthComparison}%</p>
                    :
                    <p style={{color:"rgb(229, 34, 20)"}}><TrendingDownIcon sx={{ fontSize: 40 }}/>{percentageOrderLastMonthComparison}%</p>
                  }
                  <p><ShoppingCartIcon fontSize="medium"/></p>
                </section>
                </div>
              <div>
              <p>New Customers</p>
                <p><PersonAddAltIcon/> 732</p>
                <section>
                  {
                    percentageCustomerLastMonthComparison > 0
                    ?
                    <p style={{color:"rgb(13, 234, 160)"}}><TrendingUpIcon sx={{ fontSize: 40 }}/>{percentageCustomerLastMonthComparison}%</p>
                    :
                    <p style={{color:"rgb(229, 34, 20)"}}><TrendingDownIcon sx={{ fontSize: 40 }}/>{percentageCustomerLastMonthComparison}%</p>
                  }
                  <p><PersonIcon fontSize="medium"/></p>
                </section>
              </div>
            </div>
            <div className="overview-box1-2">
                <div>
                  <p>Cart Overview</p>
                  <div>
                    <button>All</button>
                    <button>1M</button>
                    <button>6M</button>
                    <button>1Y</button>
                  </div>
                </div>
                <LineChart width={"90%"} marginTop={"20px"} label={addProductLabel} data={addProductData}/>
            </div>
        </div>
        <div className="overview-box2">
            <div className="overview-box2-1">
              <div>
                <p>{comboChartName}</p>
                <div>
                  <button>All</button>
                  <button>1M</button>
                  <button>6M</button>
                  <button>1Y</button>
                </div>
              </div>
              <ComboChart label={orderMonth} orderSuccess={orderSuccess} refund={orderCanceled}/>
              <div  className="overview-box-2-1-2">
                  <div>
                    <p>{orderData.length}</p>
                    <p>Orders</p>
                  </div>
                  <div>
                    <p>{earnings}$</p>
                    <p>Earnings</p>
                  </div>
                  <div>
                    <p>{orderCancel}</p>
                    <p>Refunds</p>
                  </div>
                  <div>
                    <p>{conversionRate}%</p>
                    <p>Conversion Ratio</p>
                  </div>
              </div>
            </div >
            <div className="overview-box2-2">
                <div className="country-sale-header">
                  <p>Sale By Country</p>
                </div>
                <WorldMapChart setNewCountry={setCountry} country={country}/>
                <Tooltip  anchorSelect="#world-map-chart" content={country} float />
                <HorizontalBarChart/>
            </div>  
        </div>
        <div className="overview-box3">
          <div  className="overview-box3-1">
            <div className="overview-box3-1-1">
              <p>Catergory Popular</p>
                <div>
                  <button>All</button>
                  <button>1M</button>
                  <button>6M</button>
                  <button>1Y</button>
                </div>
            </div>
            <DonutChart data={donutChartData}></DonutChart>
            <div className="overview-box3-1-2">
                {
                 JSON.stringify(donutChartData) === JSON.stringify([])?
                 <></>
                 :
                 <div>
                  <img src={donutChartData[2][0].avatar} alt="img-1"/>
                    <p>{donutChartData[2][0].percentageOrder}%</p>
                    <p>{donutChartData[2][0].name}</p>
                  <button>More Detail</button>
                </div>
                }
                {
                 JSON.stringify(donutChartData) === JSON.stringify([])?
                 <></>
                 :
                 <div>
                  <img src={donutChartData[2][1].avatar} alt="img-1"/>
                    <p>{donutChartData[2][1].percentageOrder}%</p>
                    <p>{donutChartData[2][1].name}</p>
                  <button>More Detail</button>
                </div>
                }
                {
                 JSON.stringify(donutChartData) === JSON.stringify([])?
                 <></>
                 :
                 <div>
                  <img src={donutChartData[2][2].avatar} alt="img-1"/>
                    <p>{donutChartData[2][2].percentageOrder}%</p>
                    <p>{donutChartData[2][2].name}</p>
                  <button>More Detail</button>
                </div>
                }
            </div>
          </div>  
          <div className="overview-box3-2">
            <div className="overview-box3-2-1">
                <p>Recent Login</p>
                <div>
                  <button onClick={handleExportOrder}>Export File</button>
                </div>
            </div>
            <div className="overview-box3-2-2">
              <div className="overview-box3-2-2-1">
                <div className="overview-box3-2-2-1-1">
                  <div>
                    <p>login</p>
                    <p>172</p>
                  </div>
                  <LineChart lineColor={"#00BD48"} width={"60%"} marginTop={"0px"} legendDisplay={false} xDisplay={false} yDisplay={false}/>
                </div>  
                <p>+3% from lastweek</p>
              </div>
              <div className="overview-box3-2-2-2">
                <div className="overview-box3-2-2-2-1">
                  <div>
                    <p>Logout</p>
                    <p>31</p>  
                  </div>  
                  <LineChart lineColor={"#BD0027"} width={"60%"} marginTop={"0px"} legendDisplay={false} xDisplay={false} yDisplay={false}/>
                </div>
                <p>-10% from lastmonth</p>
              </div>
            </div>
            <div className="overview-box3-2-3">
              <TablePaginationComponent 
                  columns={columns}
                  currentPage = {currentPage}
                  rowPerPage = {rowPerPage}
                  rowPerPageOption = {rowPerPageOption} 
                  TableRowComponent = {TableRowComponent}
                  handleChangePage = {handleChangePage}
                  handleChangeRowsPerPage = {handleChangeRowsPerPage}
                  length = {orderLength}
                  headerColor= {"#E7D284"}
                  Height = {"100%"}
              />
            </div>
          </div>
        </div>
      </div>
  )
}

export default Overview