import React, { useState, useEffect, useCallback} from "react";
import TablePaginationComponent from "../../../components/admin/Table/TablePanigation";
import { useSelector, useDispatch } from "react-redux";
import { loadProductData } from "../../../actions/admin/admin.productAction";
import { TableBody, TableCell, tableCellClasses, TableRow} from "@mui/material";
import { styled } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { loadAllCategory } from "../../../actions/admin/admin.categoryAction";
import ModalEditProduct from "../../../components/admin/Modal/ProductModal";
import { setBackEditProductStatus, setBackDeleteProductStatus } from "../../../actions/admin/admin.productAction";

//function truncate a string to specific length
function truncate(str, n){
    return (str.length > n) ? str.slice(0, n-1) + '....' : str;
};    

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));




function  ProductList () {
    const dispatch = useDispatch();
    const {categoryList} = useSelector(state => state.adminCategoryReducer);
    const {productList, productLength} = useSelector(state => state.adminProductReducer);
    const [currentPage, setCurrentPage] = useState(0);
    const [rowPerPage, setRowsPerPage] = useState(5);
    const rowPerPageOption = [5, 10, 15];

    const [openEditModal, setOpenEditModal] = useState(false);
    const [openDeleteModal, setOpenDeleteModal] = useState(false); 
    const [productInfo, setProductInfo] = useState({});
    const [acceptedFiles, setAcceptedFiles] = useState([]);
    
    //funciton handle change page number
    const handleChangePage = (event, value) => {
        setCurrentPage(value);
    }

    //function handle change row per page
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
    }   

    //Open Edit Modal
    const handleShowModalEditProduct = (value) => {
        dispatch(setBackEditProductStatus());
        setProductInfo(value)
        setOpenEditModal(true)
    }
    const handleCloseEdit = useCallback(() => {
        setAcceptedFiles([]);
        setOpenEditModal(false);
    }, [setOpenEditModal])
    const handleShowModalDeleteProduct = (value) => {
        dispatch(setBackDeleteProductStatus());
        setProductInfo(value)
        setOpenDeleteModal(true);
    }
    
    const handleCloseDelete = useCallback(() => setOpenDeleteModal(false), [setOpenDeleteModal]);
    //Columns header content
    const columns = [
        { id: 'STT', label: 'Index', minWidth: 50, align:'center'},
        { id: 'product Name', label: 'Name', minWidth: 100, align:'center' },
        {
            id: 'picture',
            label: 'Picture',
            minWidth: 170,
            align: 'center',
        },
        {
          id: 'category',
          label: 'Category',
          minWidth: 100,
          align: 'center',
        },
        {
          id: 'Price',
          label: 'Price',
          minWidth: 130,
          align: 'center',
        },
        {
            id: 'Promotion Price',
            label: 'Promotion Price',
            minWidth: 130,
            align: 'center',
        },
        {
            id: 'Quntity',
            label: 'Quantity',
            minWidth: 100,
            align: 'center',
        },
        {
            id: 'Action',
            label: 'Action',
            minWidth: 150,
            align: 'center',
        },
    ];
    
    useEffect (()=> {
        dispatch(loadProductData(rowPerPage, currentPage))
        
    },[dispatch, rowPerPage, currentPage])
    useEffect(()=> {             
        dispatch(loadAllCategory());
    },[dispatch])
    //Table Content
    const TableRowComponent = () => {
        return(
            <React.Fragment>
                    <TableBody>
                        {productList.map((row, index)=>{
                            return(
                                <React.Fragment key={index}>
                                    <StyledTableRow hover role="checkbox" >
                                        <StyledTableCell align='center'>
                                            {index + 1}
                                        </StyledTableCell>
                                        <StyledTableCell >
                                            {truncate(row.name,34)}
                                        </StyledTableCell>
                                        <StyledTableCell align='center'>
                                            <img src={row.imageURL[0]} alt={row.imageURL[0]} style={{width: "130px"}}/>
                                        </StyledTableCell>
                                        <StyledTableCell align='center'>
                                            {row.type.name}
                                        </StyledTableCell>
                                        <StyledTableCell align='center'>
                                            ${row.buyPrice}
                                        </StyledTableCell>
                                        <StyledTableCell align='center'>
                                            ${row.promotionPrice}
                                        </StyledTableCell>
                                        <StyledTableCell align='center'>
                                            {row.amount}
                                        </StyledTableCell>
                                        <StyledTableCell align='center'>
                                            <button className='table-admin-button-edit' onClick={()=> handleShowModalEditProduct(row)}><EditIcon/></button>
                                            <button className='table-admin-button-delete' onClick={() => handleShowModalDeleteProduct(row)}><DeleteIcon/></button>
                                        </StyledTableCell>
                                    </StyledTableRow>
                                </React.Fragment>
                                )
                            }) 
                        }
                    </TableBody>
            </React.Fragment>
        )
    }

    return(
        <>
            <TablePaginationComponent 
                columns={columns}
                currentPage = {currentPage}
                rowPerPage = {rowPerPage}
                rowPerPageOption = {rowPerPageOption} 
                TableRowComponent = {TableRowComponent}
                handleChangePage = {handleChangePage}
                handleChangeRowsPerPage = {handleChangeRowsPerPage}
                length = {productLength}
                headerColor= {"#E7D284"}
                Height = {"470px"}
            />
            <ModalEditProduct 
                categoryList = {categoryList}
                productInfo = {productInfo}
                openEditModal = {openEditModal}  
                handleCloseEdit = {handleCloseEdit}
                openDeleteModal= {openDeleteModal}
                handleCloseDelete = {handleCloseDelete}
                acceptedFiles = {acceptedFiles}
                setAcceptedFiles = {setAcceptedFiles}
            />
        </>
    )
}


export default React.memo(ProductList);