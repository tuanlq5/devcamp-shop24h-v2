import React, { useState, useEffect, useCallback } from "react";
import TablePaginationComponent from "../../../components/admin/Table/TablePanigation";
import { useSelector, useDispatch } from "react-redux";
import { TableBody, TableCell, tableCellClasses, TableRow} from "@mui/material";
import { styled } from '@mui/material/styles';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { loadCategoryAdmin } from "../../../actions/admin/admin.categoryAction";
import CategoryModal from "../../../components/admin/Modal/CategoryModal";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

export default function CategoryList (){
    const dispatch = useDispatch();
    const [currentPage, setCurrentPage] = useState(0);
    const [rowPerPage, setRowsPerPage] = useState(5);
    const [editCategory, setEditCategory] = useState(false);
    const [deleteCategory, setDeleteCategory] = useState(false);
    const [categoryObject, setCategoryObject] = useState({});
    const rowPerPageOption = [5, 10, 15];
    const [acceptedFiles, setAcceptedFiles] = useState([]);    
    const {categoryList, categoryLength} = useSelector(state => state.adminCategoryReducer);

    //funciton handle change page number
    const handleChangePage = (event, value) => {
        setCurrentPage(value);
    }

    //function handle change row per page
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
    }   

    //show edit category modal
    const handleShowEditCategory = (value) => {
        setEditCategory(true)
        setCategoryObject(value)
    }

    //show delete category modal
    const handleShowDeleteCategory = (value) => {
        setDeleteCategory(true)
        setCategoryObject(value)
    }

    //close edit category modal
    const handleCloseEditCategory = useCallback(() => {
        setEditCategory(false);
        setAcceptedFiles([])
    }, [setEditCategory])

    // close delete category modal
    const handleCloseDeleteCategory = useCallback(() => {
        setDeleteCategory(false);
    }, [setDeleteCategory]);

    // load category data
    useEffect(() => {
        dispatch(loadCategoryAdmin(currentPage, rowPerPage));   
    },[dispatch, currentPage, rowPerPage])
    
    //Columns header content
    const columns = [
        { id: 'STT', label: 'Index', minWidth: 50, align:'center'},
        { id: 'name', label: 'name', minWidth: 100, align:'center' },
        {
          id: 'Avatar',
          label: 'Avatar',
          minWidth: 130,
          align: 'center',
        },
        {
          id: 'createAt',
          label: 'Create At',
          minWidth: 130,
          align: 'center',
        },
        {
            id: 'Action',
            label: 'Action',
            minWidth: 150,
            align: 'center',
        },
    ];

    //Table Content
    const TableRowComponent = () => {
        return(
            <TableBody>
                {categoryList.map((row, index)=>{
                    return(
                        <React.Fragment key={index}>
                            <StyledTableRow hover role="checkbox" >
                                <StyledTableCell align='center'>
                                    {index + 1}
                                </StyledTableCell>
                                <StyledTableCell  align='center'>
                                    {row.name}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    <img src={row.avatar} alt={index} style={{width: "100px"}}/>
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    {row.createdAt}
                                </StyledTableCell>
                                <StyledTableCell align='center'>
                                    <button className='table-admin-button-edit'  onClick={() => handleShowEditCategory(row)}><EditIcon/></button>
                                    <button className='table-admin-button-delete' onClick={() => handleShowDeleteCategory(row)}><DeleteIcon/></button>
                                </StyledTableCell>
                            </StyledTableRow>
                        </React.Fragment>
                        )
                    }) 
                }
            </TableBody>
        )
    }

    return(
        <>
            <TablePaginationComponent 
                columns={columns}
                currentPage = {currentPage}
                rowPerPage = {rowPerPage}
                rowPerPageOption = {rowPerPageOption} 
                TableRowComponent = {TableRowComponent}
                handleChangePage = {handleChangePage}
                handleChangeRowsPerPage = {handleChangeRowsPerPage}
                length = {categoryLength}
                headerColor= {"#E7D284"}
                Height = {"700px"}
            />
            <CategoryModal 
                editCategory = {editCategory}
                handleOpenEditCategory = {handleShowEditCategory}
                handleCloseEditCategory = {handleCloseEditCategory}
                handleOpenDeleteCategory = {handleShowDeleteCategory}
                handleCloseDeleteCategory = {handleCloseDeleteCategory}
                deleteCategory = {deleteCategory}
                productTypeData = {categoryObject}
                acceptedFiles = {acceptedFiles}
                setAcceptedFiles = {setAcceptedFiles}
            />
        </>
    )
}