import ProgressCheckout from "../../components/user/ProgressCheckout"
import ShippingAddress from "../../components/user/shippingAddress"
import OrderSummary from "../../components/user/OrderSummary"
import ShippingMethod from "../../components/user/ShippingMethod"
import PaidIcon from '@mui/icons-material/Paid';
import Payment from "../../components/user/Payment";
import auth from "../../firebase";
import { onClickProceedPayment } from "../../actions/productDetail.action";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { onSendShippingAddress, validateShipping } from "../../actions/shippingAction";
import {store} from "../../index";
export default function Checkout (){
    const dispatch = useDispatch();
    const {payment,  confirmOrder }  = useSelector(state => state.productDetailReducer);
    const {customerData} = useSelector(state => state.productReducer);
    const {ShippingInfo } = useSelector(state => state.shippingReducer);
    const handleProceedPayment = () => {
        dispatch(validateShipping());
        const state = store.getState();
        const {validateFullName, validateEmail, validatePhone, validateZipcode, validateAddress, validateCity, validateCountry, validateAdministrativeDivision} = state.shippingReducer;
        if(JSON.stringify(customerData) !== JSON.stringify({})){
            dispatch(onClickProceedPayment());
        }
        
        if (validateFullName || validateEmail || validatePhone || validateZipcode || validateAddress || validateCity || validateCountry || validateAdministrativeDivision) {
            // At least one validation check failed, so exit the function early
            return false;
        }
        if(auth.currentUser !== null){
            dispatch(onSendShippingAddress(ShippingInfo));
        }
    }
    return(
        <>
            <div className="checkout-container" >
                <div className="checkout-box1"
                    style={
                        confirmOrder ? {width:"100%"} : {width:"65%"}
                    }
                >
                    <ProgressCheckout></ProgressCheckout>
                    <ShippingAddress></ShippingAddress>
                    <Payment></Payment>
                </div>
                <div className="checkout-box2">
                    <OrderSummary></OrderSummary>
                </div>
                <div className="checkout-box3">
                    <ShippingMethod></ShippingMethod>
                </div>
                {   
                    payment ? <></>
                    :
                    <div className="checkout-box4">
                    <button onClick={handleProceedPayment}><PaidIcon/>Proceed Payment</button>
                    </div>
                }
            </div>
        </>
    )
}