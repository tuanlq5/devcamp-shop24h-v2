import { useLocation } from 'react-router-dom';
import { findProductByID } from '../../actions/productDetail.action';
import { useDispatch} from 'react-redux';
import ProductDescription from '../../components/user/ProductDescription';
import ProductDetailComponent from '../../components/user/ProductDetailComponent';
import ProductRelate from '../../components/user/ProductRelated';
import { useEffect } from 'react';

export default function ProductDetail  () {
    const dispatch = useDispatch();
    const location = useLocation().pathname;
    const paramID = decodeURIComponent(location.slice(6));
 
    
    useEffect(()=>{
        //Tìm product theo ID
        dispatch(findProductByID(paramID));
    },[dispatch, paramID])
    return(
        <>
            <div className="product-detail-container">
                <ProductDetailComponent></ProductDetailComponent>
                <ProductDescription></ProductDescription>
                <ProductRelate></ProductRelate>
            </div>
        </> 
    )
}