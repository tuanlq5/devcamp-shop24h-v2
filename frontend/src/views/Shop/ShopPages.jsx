import { useSelector, useDispatch } from 'react-redux';

import { fetchDataProduct } from '../../actions/action';
import { useEffect } from 'react';
import { Pagination } from '@mui/material';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import SelectNumberPage from '../../components/user/SelectNumberPage';
import ProductFilter from '../../components/user/ProductFilter';
import { onChangeCurrentPage } from '../../actions/shop.action';




export default function ShopPage(){
    const dispatch = useDispatch();
    const {customerData} = useSelector (state => state.productReducer);
    const {products, currentPage, noPage, limit, filterObject} = useSelector(state => state.productReducer);  
    // Hàm rút ngắn câu (truncate a string to String...)
    function truncate(str, n){
        return (str.length > n) ? str.slice(0, n-1) + '...' : str;
      };
    //Hàm thực thi khi nút chuyển trang được ấn
    const onChangePagination = (event, value) => {
        dispatch(onChangeCurrentPage(value));
    }

    // Hàm thực thi khi ấn nút more detail
    const handleViewProduct = async(product) => {
        const productView = {
            product: product._id
        }
  
        if(JSON.stringify(customerData) !== JSON.stringify({})){
            try{
            
                const condition = {
                    method: "POST",
                    headers:{
                        "Content-type": "application/json; charset=utf-8",
                    },
                    body: JSON.stringify(productView),
                }
                await fetch("http://localhost:8000/createProductView/" + customerData._id, condition);
            }catch(error){
                console.log(error);
            }
        }
        
    }
    
    // fetch data product while loading page
    useEffect(()=> {
        dispatch(
            fetchDataProduct(
                limit, 
                currentPage, 
                filterObject.productName, 
                filterObject.minPrice, 
                filterObject.maxPrice, 
                filterObject.filterType));
    }, [dispatch, limit, currentPage, filterObject.minPrice,filterObject.maxPrice, filterObject.productName,filterObject.filterType])
    return(
        <>
            <div className='shop-container'>
                <div className='shop-search-container'>
                    <ProductFilter></ProductFilter>
                </div>
                <div className='shop-product-container'>
                    {   
                    // Map product Card
                    products.map((elements, index) => {
                        return(
                            <div className='shop-product-card' key={index}>
                                <img src={elements.imageURL[0]} alt={elements.name} />
                                <p>{truncate(elements.name, 35)}</p>
                                <p>Price: {elements.buyPrice} $</p>
                                <a href={"/Shop/" + elements._id} onClick={()=> handleViewProduct(elements)}>More Detail <FontAwesomeIcon icon={solid("arrow-right")} /></a>
                            </div>
                            )
                        })
                    }
                </div>
                <div className='shop-product-panigation'>
                    <div>
                        <p>Products number: </p>
                    </div>
                    <div>
                        <SelectNumberPage></SelectNumberPage>
                    </div>
                    <div>
                        <Pagination count={noPage} page={currentPage} onChange={onChangePagination} />
                    </div>
                </div>
            </div>
        </>
    )
}