import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { useDispatch, useSelector } from 'react-redux';
import ClearIcon from '@mui/icons-material/Clear';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import {Snackbar} from '@mui/material';
import { useNavigate} from 'react-router-dom';
import MuiAlert from '@mui/material/Alert';
import auth from '../../firebase';
import { 
    handlerTakeDataFromLocalStorageCart, 
    handlerDecreaseQuantityInCart, 
    handlerIncreaseQuantityInCart,
    handlerDeleteProductInCart 
} from '../../actions/productDetail.action';
import {useMemo, useEffect, useState } from 'react';
import { onChangeCoupon, checkCoupon } from '../../actions/shop.action';
import { handleProceedCheckout,  onSendOrderForGoogleAccount } from '../../actions/productDetail.action';

// Create our number formatter.
const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0,
    maximumFractionDigits: 0, 
    //These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
  });

export default function ProductCart (){
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const {customerData, googleAccount} = useSelector (state => state.productReducer);
    const { productCart, coupon, percentDiscount, couponId } = useSelector(state => state.productDetailReducer);
    const [openCoupon, setCouponSnackbar] = useState(false);
    const [openCartWarning, setCartWarning] = useState(false);

    // Lấy dữ liệu từ localstorage
    const memoizedProductInCart = useMemo(() => {
        const productInCart =  JSON.parse(localStorage.getItem("cart"));
        return productInCart || [];
    }, [])

    // totalPay
    let totalPay = 0;
    if( productCart !== null){
        for(var i = 0; i< productCart.length ;i++){
            totalPay += ((productCart[i].price * productCart[i].quantity) * (100 - percentDiscount))/100 
        }
    }
    
    // hàm tăng quantity
    const handlerDecreaseQuantity = async(value) => {
        dispatch(handlerDecreaseQuantityInCart(value.name))
        try {
            const product ={
                product: value.id,
                quantity: 1
            }
            if(JSON.stringify(customerData) !== JSON.stringify({})){
                const condition = {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json; charset=utf-8",
                    },
                    body: JSON.stringify(product),
                }
                await fetch("http://localhost:8000/removeProductCart/" + customerData._id, condition);
            }
            const condition = {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(product),
                credentials: "include",
            }
             await fetch("http://localhost:8000/removeProdcutCartNonUser", condition);
        } catch (error) {
            console.assert(error)
        }
    }

    // hàm giảm quantity
    const handlerIncreaseQuantity = async (value) => {
        dispatch(handlerIncreaseQuantityInCart(value.name))
        try {
            const product ={
                product: value.id,
                quantity: 1
            }
            if(JSON.stringify(customerData) !== JSON.stringify({})){
                const condition = {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json; charset=utf-8",
                    },
                    body: JSON.stringify(product),
                }
                await fetch("http://localhost:8000/addProductCart/" + customerData._id, condition);
            }

            const condition = {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(product),
                credentials: "include",
            }
            await fetch("http://localhost:8000/addProductCartNonUser", condition);

        } catch (error) {
            console.assert(error)
        }
    }

    // hàm delete product card
    const handlerDeleteProductCart = async(value)=> {
        dispatch(handlerDeleteProductInCart(value.name))
        try {
            const product ={
                product: value.id,
                quantity: value.quantity
            }
            if(JSON.stringify(customerData) !== JSON.stringify({})){
                const condition = {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json; charset=utf-8",
                    },
                    body: JSON.stringify(product),
                }
                await fetch("http://localhost:8000/addProductCart/" + customerData._id, condition);
            }

            const condition = {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                },
                body: JSON.stringify(product),
                credentials: "include",
            }
            await fetch("http://localhost:8000/removeProdcutCartNonUser", condition);
        } catch (error) {
            console.assert(error)
        }
    }

    //Lữu dữ liệu coupon
    const handlerCoupon = (event) => {
        dispatch(onChangeCoupon(event.target.value));
    }

    //hàm check coupon 
    const applyCoupon = () => {
        dispatch(checkCoupon(coupon));
        
        if(percentDiscount > 0){

            //If voucher is exist open success snack bar
            
            setCouponSnackbar(true);

        }
    }

    //đóng snack bar
    const handleCloseSnackbar = () => {
        setCouponSnackbar(false);
        setCartWarning(false);
    }

    const continueShopping = () => {
        navigate("/Shop");
    }
    
    //current fireabase user
    const user = auth.currentUser;

    //hàm xử lý khi người dùng ấn nút proceed to check out
    const handleCheckOut = () => {

        //Nếu người dùng có login và có product trong cart

        if(JSON.stringify(customerData) !== JSON.stringify({}) && 
        (JSON.stringify(memoizedProductInCart) !== JSON.stringify([]))){
            dispatch(handleProceedCheckout(productCart, customerData, totalPay));
            navigate("/ProductCart/Checkout")
            return
        }
        
        //Nếu người dùng nhập thêm voucher hợp lệ
        if(JSON.stringify(customerData) !== JSON.stringify({}) 
        && (JSON.stringify(memoizedProductInCart ) !== JSON.stringify([]))
        && percentDiscount > 0
        ){
            dispatch(handleProceedCheckout(productCart, customerData, totalPay, couponId));
            navigate("/ProductCart/Checkout")
            return
        }
        //Nếu người dùng có login Google account và có product trong cart

        if(JSON.stringify(googleAccount) !== JSON.stringify({}) 
        && (JSON.stringify(memoizedProductInCart) !== JSON.stringify([]))){
            dispatch(onSendOrderForGoogleAccount(productCart, totalPay, user.uid, couponId));
            navigate("/ProductCart/Checkout")
            return
        }

        //Nếu người dùng chưa login

        if(JSON.stringify(customerData) === JSON.stringify({}) && JSON.stringify(googleAccount) === JSON.stringify({})){
            navigate("/Register")
        }

        //Nếu người dùng đã login nhưng chưa có product trong cart 
        if((JSON.stringify(memoizedProductInCart) === JSON.stringify([]))
        ){
            setCartWarning(true);
        }
    }

    useEffect(()=>{
        dispatch(handlerTakeDataFromLocalStorageCart(memoizedProductInCart));
        if(percentDiscount > 0){

            //Save the information in local storage

            localStorage.setItem("voucher", 
            JSON.stringify(
                {
                code: coupon, 
                discountPercent: percentDiscount,
                id: couponId
                }
            ))
        }
    },[memoizedProductInCart,dispatch, coupon, couponId, percentDiscount])
    return (
        <>  
            {/* Snackbar when coupon is exist */}
            <Snackbar open = {openCoupon} autoHideDuration = {5000} onClose = {handleCloseSnackbar}>
                <MuiAlert  elevation={6}  variant="filled" severity='success' sx={{width:"100%", backgroundColor:"rgb(104 90 53)"}}>
                    You Got {percentDiscount}% off from coupon !
                </MuiAlert>
            </Snackbar>
            <Snackbar open = {openCartWarning} autoHideDuration = {5000} onClose = {handleCloseSnackbar}>
                <MuiAlert  elevation={6}  variant="filled" severity='error' sx={{width:"100%"}}>
                    Your Cart is Empty!
                </MuiAlert>
            </Snackbar>
            <div className="product-cart-container">
                <div className="cart-table">
                <TableContainer component={Paper}>
                    <Table sx={{ minWidth: 650 }} aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell >Products</TableCell>
                            <TableCell align="center">Price</TableCell>
                            <TableCell align="center">Quantity</TableCell>
                            <TableCell align="center">Total</TableCell>
                            <TableCell align="center"></TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {   productCart !== null 
                            ?
                            productCart.map((row, index) => (
                                <TableRow
                                key={index}
                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <TableCell >
                                        <img src={row.image[0]} alt="product" style={{width: "10%"}}/>
                                        <p >{row.name}</p>
                                    </TableCell>
                                    <TableCell align="center">
                                        {row.price}
                                    </TableCell>
                                    <TableCell align="center" sx={{
                                    width:"20%"
                                    }}>
                                        <div sx={{
                                            display:"flex",
                                            alignItems: "center",
                                            justifyContent: "center", 
                                            translate: "20px",
                                        }}>
                                            <button style={{
                                                backgroundColor:"transparent",
                                                border: "0px"
                                                }}
                                                onClick={() => handlerDecreaseQuantity(row)}
                                            >
                                                <ArrowBackIosNewIcon></ArrowBackIosNewIcon>
                                            </button>
                                            <p>{row.quantity}</p>
                                            <button style={{
                                                backgroundColor:"transparent",
                                                border: "0px"
                                                }}
                                                onClick={() => handlerIncreaseQuantity(row)}    
                                            >
                                                <ArrowForwardIosIcon></ArrowForwardIosIcon>
                                            </button>
                                        </div>
                                    </TableCell>
                                    <TableCell align='center'>
                                        {/* Total Price */}
                                        {row.quantity * row.price}
                                    </TableCell> 
                                    <TableCell align='center'>
                                        <button style={{
                                            backgroundColor:"transparent",
                                            border: "0px"
                                            }}
                                            onClick={() => handlerDeleteProductCart(row)}
                                            >
                                            <ClearIcon></ClearIcon>
                                        </button>
                                    </TableCell>

                                </TableRow>
                            ))
                            :
                            <></>                
                        }
                        </TableBody>
                    </Table>
                    </TableContainer>
                </div>
                <div className="button-continue-shoping">
                    <button onClick={continueShopping}>Continue Shopping</button>
                </div>
                <div className="button-update-cart">
                    <button>Update Cart</button>
                </div>
                <div className="discount-code">
                    <p>Discount Codes</p>
                    <div>
                        <input type="text" onChange={handlerCoupon}  placeholder='Enter your coupon code'/>
                        <button onClick={applyCoupon}> Apply coupon</button>
                        {
                            percentDiscount > 0 ? 
                            <p>You got {percentDiscount}% off</p>
                            : <></>
                        }
                    </div>                         
                </div>
                <div className="Checkout">
                    <p>Cart Total</p>
                    <div>
                        <p>Subtotal</p>
                        <p>{formatter.format(totalPay)}</p>
                    </div>
                    <div>
                        <p>Total</p>
                        <p> {formatter.format(totalPay)}</p>
                    </div>     
                    <button onClick={handleCheckOut}>PROCEED TO CHECKOUT</button>               
                </div>
            </div>
        </>
    )
}