import React from "react"


const Home = React.lazy(() => import('./views/Home/HomePage'));
const Shop = React.lazy(() => import("./views/Shop/ShopPages"));
const LoginForm = React.lazy(() => import("./views/LoginUser"));
const ProductDetail = React.lazy(() => import("./views/Shop/ProductDetail"));
const ProductCart = React.lazy(() => import("./views/Shop/ProductCart"));
const RegisterForm = React.lazy(() => import("./views/RegisterUser"));
const Checkout = React.lazy(() => import("./views/Shop/Checkout"));
const adminRegister = React.lazy(() => import("./views/Admin/RegisterAdmin"));


const RouteList = [
    {path:"/",name: "HomePage", element: Home},
    {path:"/Shop", name: "Shop", element:Shop},
    {path:"/Login", name:"LogIn", element: LoginForm},
    {path:"/Shop/:ProductDetail", name:"Product Detail", element: ProductDetail},
    {path:"/ProductCart", name: "Cart", element: ProductCart},
    {path:"/Register", name: "Register", element: RegisterForm},
    {path:"/ProductCart/Checkout", name: "CheckOut", element: Checkout},
    {path:"/AdminRegister", name: "Admin Register", element: adminRegister},
]
export default RouteList