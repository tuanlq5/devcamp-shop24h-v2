import { 
    SEND_REQUEST_TAKE_PRODUCT_PENDING,
    SEND_REQUEST_TAKE_PRODUCT_START,
    SEND_REQUEST_TAKE_PRODUCT_END, 
    SEND_AUTHENTICATION_TO_GOOGLE_PENDING,
    SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS, 
    SEND_AUTHENTICATION_TO_GOOGLE_END, 
    SIGN_OUT_GOOGLE_ACCOUNT,
    ON_CHANGE_FULLNAME_REGISTER,
    ON_CHANGE_PHONE_REGISTER,
    ON_CHANGE_EMAIL_REGISTER,
    ON_CHANGE_PASSWORD_REGISTER,
    ON_CHANGE_ADDRESS_REGISTER,
    ON_CHANGE_CITY_REGISTER,
    ON_CHANGE_COUNTRY_REGISTER,
    ON_CHANGE_POLICY_REGISTER,
    ON_CHANGE_ADMINISTRATOR_REGISTER,
    ON_CHANGE_ADMIN_REGISTER,
    REGISTER_CUSTOMER_ACCOUNT_PENDING,
    REGISTER_CUSTOMER_ACCOUNT_SUCCESS,
    REGISTER_CUSTOMER_ACCOUNT_ERROR,
    ON_CHANGE_EMAIL_LOGIN,
    ON_CHANGE_PASSWORD_LOGIN,
    ON_LOGIN_CUSTOMER_ERROR,
    ON_LOGIN_CUSTOMER_PENDING,
    ON_LOGIN_CUSTOMER_SUCCESS,
    ON_REFRESH_CUSTOMER_ACCOUNT_PENDING,
    ON_REFRESH_CUSTOMER_ACCOUNT_SUCCESS,
    ON_REFRESH_CUSTOMER_ACCOUNT_ERROR,
    ON_LOGOUT_CUSTOMER_ACCOUNT_PENDING,
    ON_LOGOUT_CUSTOMER_ACCOUNT_SUCCESS,
    ON_LOGOUT_CUSTOMER_ACCOUNT_ERROR,
    SET_LOADING_CUSTOMER,
    ON_REGISTER_ADMIN_ACCOUNT_PENDING,
    ON_REGISTER_ADMIN_ACCOUNT_SUCCESS,
    ON_REGISTER_ADMIN_ACCOUNT_ERROR
 } from "../constant/constant";

import { 
    CHANGE_PRODUCT_NUMBER,
    ON_CHANGE_FILTER_NAME,
    ON_CHANGE_FILTER_MIN_PRICE,
    ON_CHANGE_FILTER_MAX_PRICE,
    ON_CHANGE_FILTER_TYPE_SOFA,
    ON_CHANGE_FILTER_TYPE_DESK,
    ON_CHANGE_FILTER_TYPE_CHAIR,
    ON_CHANGE_FILTER_TYPE_BOOKSHELF,
    ON_CHANGE_FILTER_TYPE_TVSTAND,
    ON_CHANGE_FILTER_TYPE_BED,
    ON_CHANGE_FILTER_TYPE_DECORITEM,
    ON_CHANGE_CURRENT_PAGE_NUMBER,
    ON_CLICK_FILTER_PRODUCT,
} from "../constant/shop.constant";



const initialState = {
    sideBarOpen: false,
    products: [],
    limit: 9,
    noPage: 4,
    currentPage: 1,
    //filter state from here
    maxPrice: 0, //filter mã price
    minPrice: 0,    //filter minPrice
    productName: "", //filter name
    filterType: [], // filter Type 
    Sofa: false,
    Desk: false,
    Chair: false,
    BookShelf: false,
    TvStand: false,
    Bed: false,
    DecorItem: false,
    filterObject: {
        maxPrice: 0, 
        minPrice: 0,   
        productName: "", 
        filterType: [], 
    },
    productType: [],
    productTypeID: {
        Sofa: "",
        Desk: "",
        Chair: "",
        BookShelf: "",
        TvStand: "",
        Bed: "",
        DecorItem:"",
    },
    productFindByType: [],
    policy: false,
    customerRegister :{
        fullName: '',
        phone:  '',
        email: '',
        password:'',
        address:'',
        city:'',
        country: '',
        administrativeDivision:'',
        adminCode: ''
    },
    loadingUser: false,
    userLogin: {
        email:"",
        password:""
    },
    loginFail: false,
    googleAccount:{},
    customerData:{
    },
    customerAccount: false,
    adminAccount: false,
};

const productReducer = (state = initialState, action) => {
    switch (action.type){
        case SEND_REQUEST_TAKE_PRODUCT_PENDING:
            break;
        case SEND_REQUEST_TAKE_PRODUCT_START:
            state.products = action.data.data;
            state.noPage = action.page;
            state.productType = action.productType.data;
            for(let i = 0; i < state.productType.length; i++){
                if(state.productType[i].name.toUpperCase() ==="SOFA"){
                    state.productTypeID.Sofa =  state.productType[i]._id
                }
                if(state.productType[i].name.toUpperCase() ==="DESK"){
                    state.productTypeID.Desk =  state.productType[i]._id
                }
                if(state.productType[i].name.toUpperCase() ==="CHAIR"){
                    state.productTypeID.Chair =  state.productType[i]._id
                }
                if(state.productType[i].name.toUpperCase() ==="BOOKSHELF"){
                    state.productTypeID.BookShelf =  state.productType[i]._id
                }
                if(state.productType[i].name.toUpperCase() ==="TVSTAND"){
                    state.productTypeID.TvStand =  state.productType[i]._id
                }
                if(state.productType[i].name.toUpperCase() ==="BED"){
                    state.productTypeID.Bed =  state.productType[i]._id
                }
                if(state.productType[i].name.toUpperCase() ==="DECORITEM"){
                    state.productTypeID.DecorItem =  state.productType[i]._id
                }
            }
            break;
        case ON_CHANGE_CURRENT_PAGE_NUMBER:
            state.currentPage = action.value;
            break
        case SEND_REQUEST_TAKE_PRODUCT_END:
            break;
        case SEND_AUTHENTICATION_TO_GOOGLE_PENDING:
            break;
        case SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS:
            state.googleAccount = action.payload;
            break;
        case SEND_AUTHENTICATION_TO_GOOGLE_END:
            state.googleAccount = {}
            break;
        case SIGN_OUT_GOOGLE_ACCOUNT:
            state.customerData = {};
            break;
        case CHANGE_PRODUCT_NUMBER:
            state.limit = action.value;
            state.currentPage = 1;
            break;
        case ON_CHANGE_FILTER_NAME:
            state.productName = action.value;
            break;
        case ON_CHANGE_FILTER_MIN_PRICE:
            state.minPrice = action.value;
            break;
        case ON_CHANGE_FILTER_MAX_PRICE:
            state.maxPrice = action.value;
            break;
        case ON_CHANGE_FILTER_TYPE_SOFA:
            state.Sofa = action.value;
            break;
        case ON_CHANGE_FILTER_TYPE_DESK:
            state.Desk = action.value;
            break;
        case ON_CHANGE_FILTER_TYPE_CHAIR:
            state.Chair = action.value;
            break;
        case ON_CHANGE_FILTER_TYPE_BOOKSHELF:
            state.BookShelf = action.value;
            break;
        case ON_CHANGE_FILTER_TYPE_TVSTAND:
            state.TvStand = action.value;
            break;
        case ON_CHANGE_FILTER_TYPE_BED:
            state.Bed = action.value;
            break;
        case ON_CHANGE_FILTER_TYPE_DECORITEM:
            state.DecorItem = action.value;
            break;
        case ON_CLICK_FILTER_PRODUCT:
            //reset gia tri filter
            state.filterObject.productName = "";
            state.filterObject.minPrice = 0;
            state.filterObject.maxPrice = 0;
            state.filterObject.filterType = [];
            state.currentPage = 1;
            //add gia tri filter
            state.filterObject.productName = state.productName;
            state.filterObject.minPrice = state.minPrice;
            state.filterObject.maxPrice = state.maxPrice;
            if(state.Sofa){
                state.filterObject.filterType.push(state.productTypeID.Sofa)
            }
            if(state.Desk){
                state.filterObject.filterType.push(state.productTypeID.Desk)
            }
            if(state.Chair){
                state.filterObject.filterType.push(state.productTypeID.Chair)
            }
            if(state.BookShelf){
                state.filterObject.filterType.push(state.productTypeID.BookShelf)
            }
            if(state.DecorItem){
                state.filterObject.filterType.push(state.productTypeID.DecorItem)
            }
            if(state.TvStand){
                state.filterObject.filterType.push(state.productTypeID.TvStand)
            }
            if(state.Bed){
                state.filterObject.filterType.push(state.productTypeID.Bed)
            } 
            break;
        case ON_CHANGE_FULLNAME_REGISTER:
            state.customerRegister.fullName = action.payload;
            break;
        case ON_CHANGE_PHONE_REGISTER:
            state.customerRegister.phone = action.payload;
            break;
        case ON_CHANGE_EMAIL_REGISTER:
            state.customerRegister.email = action.payload;
            break;
        case ON_CHANGE_PASSWORD_REGISTER:
            state.customerRegister.password  = action.payload;
            break;
        case ON_CHANGE_ADDRESS_REGISTER:
            state.customerRegister.address = action.payload;
            break;
        case ON_CHANGE_CITY_REGISTER:
            state.customerRegister.city = action.payload;
            break;
        case ON_CHANGE_COUNTRY_REGISTER:
            state.customerRegister.country = action.payload;
            break;
        case ON_CHANGE_ADMINISTRATOR_REGISTER:
            state.customerRegister.administrativeDivision = action.payload;
            break;
        case ON_CHANGE_ADMIN_REGISTER:
            state.customerRegister.adminCode = action.payload;
            break;
        case ON_CHANGE_POLICY_REGISTER:
            state.policy = action.payload;
            break;
        case REGISTER_CUSTOMER_ACCOUNT_PENDING:
            state.customerAccount = false
            break;
        case REGISTER_CUSTOMER_ACCOUNT_SUCCESS:
            state.customerAccount = true;
            break;
        case REGISTER_CUSTOMER_ACCOUNT_ERROR:
            state.customerAccount = false
            break;
        case ON_CHANGE_EMAIL_LOGIN:
            state.userLogin.email = action.payload;
            break;
        case ON_CHANGE_PASSWORD_LOGIN:
            state.userLogin.password = action.payload;
            break;
        case ON_LOGIN_CUSTOMER_PENDING:
            break;
        case ON_LOGIN_CUSTOMER_SUCCESS:
            state.loginFail = false;
            state.customerData = action.data;
            break;
        case ON_LOGIN_CUSTOMER_ERROR:
            state.loginFail = true;
            break;
        case ON_REFRESH_CUSTOMER_ACCOUNT_PENDING:
            break;
        case ON_REFRESH_CUSTOMER_ACCOUNT_SUCCESS:
            state.customerData = action.data;
            break;
        case ON_REFRESH_CUSTOMER_ACCOUNT_ERROR:
            break;
        case ON_LOGOUT_CUSTOMER_ACCOUNT_PENDING:
            break;
        case ON_LOGOUT_CUSTOMER_ACCOUNT_SUCCESS:
            state.customerData = {};
            break;
        case ON_LOGOUT_CUSTOMER_ACCOUNT_ERROR:
            break;
        case SET_LOADING_CUSTOMER:
            state.loadingCustomerData = action.payload
            break;
        case ON_REGISTER_ADMIN_ACCOUNT_PENDING:
            state.adminAccount = false;
            break;
        case ON_REGISTER_ADMIN_ACCOUNT_SUCCESS:
            console.log(state.adminAccount)
            state.adminAccount = true;
            break;
        case ON_REGISTER_ADMIN_ACCOUNT_ERROR:
            console.log("Hello")
            state.adminAccount = action.error;
            break;
        default:
            break;
    }
    return{...state};
}

export default productReducer