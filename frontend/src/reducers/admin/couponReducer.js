import { 
    LOAD_LIST_COUPON_PENDING ,
    LOAD_LIST_COUPON_SUCCESS, 
    LOAD_LIST_COUPON_ERROR,

}from "../../constant/admin/admin.constant";

import { 
    ON_DELETE_COUPON_PENDING,
    ON_DELETE_COUPON_SUCCESS,
    ON_DELETE_COUPON_ERROR,
    ON_EDIT_COUPON_PENDING,
    ON_EDIT_COUPON_SUCCESS,
    ON_EDIT_COUPON_ERROR,
    ON_UPDATE_EDIT_COUPON_STATUS,
    ON_UPDATE_DELETE_COUPON_STATUS
} from "../../constant/admin/admin.CouponConstant";
const initialState = {
    couponList: [],
    couponLength: 0,
    editCouponStatus: false,
    deleteCouponStatus: false
};

const adminCouponReducer = (state = initialState, action) => {
    switch(action.type){
        case LOAD_LIST_COUPON_PENDING:
            break;
        case LOAD_LIST_COUPON_SUCCESS:
            state.couponList = action.data
            state.couponLength = action.length;
            break;
        case LOAD_LIST_COUPON_ERROR:
            break;

        case ON_EDIT_COUPON_PENDING:
            state.editCouponStatus = false
            state.deleteCouponStatus = false
            break;
            
        case ON_EDIT_COUPON_SUCCESS:
            state.editCouponStatus = true
            break;

        case ON_EDIT_COUPON_ERROR:
            break;
        
        case ON_DELETE_COUPON_PENDING:
            state.editCouponStatus =false
            state.deleteCouponStatus = false
            break;

        case ON_DELETE_COUPON_SUCCESS:
            state.deleteCouponStatus = true
            break;
        
        case ON_DELETE_COUPON_ERROR:
            break;
        
        case ON_UPDATE_EDIT_COUPON_STATUS:
            state.editCouponStatus = false;
            break;

        case ON_UPDATE_DELETE_COUPON_STATUS:
            state.deleteCouponStatus = false;
            break;
            
        default:
            break;
    }
    return{...state}
} 

export default adminCouponReducer