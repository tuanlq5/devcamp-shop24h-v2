import { 
    LOAD_CUSTOMER_LIST_PENDING,
    LOAD_CUSTOMER_LIST_SUCCESS,
    LOAD_CUSTOMER_LIST_ERROR,
    ADMIN_REGISTER_PENDING,
    ADMIN_REGISTER_SUCCESS,
    ADMIN_REGISTER_ERROR,
} from "../../constant/admin/admin.constant";

import { 
    UPDATE_CUSTOMER_PENDING,
    UPDATE_CUSTOMER_SUCCESS,
    UPDATE_CUSTOMER_ERROR,
    DELETE_CUSTOMER_PENDING,
    DELETE_CUSTOMER_SUCCESS,
    DELETE_CUSTOMER_ERROR
} from "../../constant/admin/admin.CustomerConstant";


const initialState = {
    customerList: [],
    customerLength: 0,
    editCustomerStatus: false,
    deleteCustomerStatus: false
}

const adminCustomerReducer = (state = initialState, action) => {
    switch(action.type){
        case LOAD_CUSTOMER_LIST_PENDING:
            break;
        case LOAD_CUSTOMER_LIST_SUCCESS:
            state.customerList = action.data;
            state.customerLength = action.length;
            break;
        case LOAD_CUSTOMER_LIST_ERROR:
            break;
        case ADMIN_REGISTER_PENDING:
            break;
        case ADMIN_REGISTER_SUCCESS:
            break;
        case ADMIN_REGISTER_ERROR:
            break;
        case UPDATE_CUSTOMER_PENDING:
            state.editCustomerStatus = false
            state.deleteCustomerStatus = false
            break;
        case UPDATE_CUSTOMER_SUCCESS:
            state.editCustomerStatus = true
            break;
        case UPDATE_CUSTOMER_ERROR:
            break;
        case DELETE_CUSTOMER_PENDING:
            state.deleteCustomerStatus = false
            state.editCustomerStatus = false
            break;
        case DELETE_CUSTOMER_SUCCESS:
            state.deleteCustomerStatus = true
            break;
        case DELETE_CUSTOMER_ERROR:
            break;
        default:
            break;
    }
    return{...state}
}

export default adminCustomerReducer