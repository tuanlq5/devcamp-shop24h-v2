import { 
    MONTHLY_ORDER,
    MONTHLY_REVENUE,
    MONTHLY_NEW_CUSTOMER,
    SEND_REQUEST_ALL_ORDER_PENDING,
    SEND_REQUEST_ALL_ORDER_SUCCESS,
    SEND_REQUEST_ALL_ORDER_ERROR,
    SEND_REQUEST_DONUT_CHART_DATA_PENDING,
    SEND_REQUEST_DONUT_CHART_DATA_SUCCESS,
    SEND_REQUEST_DONUT_CHART_DATA_ERROR
} from "../../constant/admin/admin.DataAnalyzeConstant";

const initialState = {
    orderData:[],
    monthlySale: 0,
    monthlyOrder: 0,
    monthlyNewCustomer:0,
    percentageSaleLastMonthComparison:0,
    percentageOrderLastMonthComparison:0,
    percentageCustomerLastMonthComparison: -20,
    addProductLabel : [],
    addProductData : [],
    userActivities: [],
    orderCancel:0,
    acceptedOrder: [],
    refundOrder:[],
    orderTime:[],
    donutChartData: []
}

const dataAnalyzeReducer =  (state = initialState, action) =>{
    switch(action.type){
        case SEND_REQUEST_ALL_ORDER_PENDING:
            break;
        case SEND_REQUEST_ALL_ORDER_SUCCESS:
            state.orderData = action.data;
            state.monthlySale = action.monthlySale;
            state.monthlyOrder  = action.orderLength;
            state.percentageSaleLastMonthComparison = action.compareToLastMonthSale;
            state.percentageOrderLastMonthComparison = action.compareToLastMonthOrder;
            state.addProductLabel = action.addProductMonth;
            state.addProductData = action.addProductDataInMonth;
            state.userActivities = action.userActivities;
            state.orderCancel = action.orderCancel;
            state.acceptedOrder = action.acceptedOrder;
            state.orderTime = action.orderTime;
            state.refundOrder = action.refundOrder;
            break;
        case SEND_REQUEST_ALL_ORDER_ERROR:
            break;
        case MONTHLY_ORDER:
            break;
        case MONTHLY_REVENUE:
            break;
        case MONTHLY_NEW_CUSTOMER:
            break;
        case SEND_REQUEST_DONUT_CHART_DATA_PENDING:
            break;
        case SEND_REQUEST_DONUT_CHART_DATA_SUCCESS:
            state.donutChartData = action.donutChartData;
            break;
        case SEND_REQUEST_DONUT_CHART_DATA_ERROR:
            break;
        default:
            break;
    }
    return{...state}
}

export default dataAnalyzeReducer