import { 
    LOAD_ORDER_LIST_PENDING,  
    LOAD_ORDER_LIST_ERROR, 
    LOAD_ORDER_LIST_SUCCESS,
    ON_CHANGE_STATUS_ORDER,
    ON_EDIT_ORDER_STATUS_PENDING,
    ON_EDIT_ORDER_STATUS_SUCCESS,
    ON_EDIT_ORDER_STATUS_ERROR,
} from "../../constant/admin/admin.constant";
const initialState = {
    orderList:[],
    currentPage: 1,
    rowPerPage: 5,
    numberOfPage: 1,
    rowPerPageOption: [5, 10, 15],
    orderLength: 0,
    orderStatus: "",
};

const adminOrderReducer = (state = initialState, action) => {
    switch(action.type){
        case LOAD_ORDER_LIST_PENDING:
            break;
        case LOAD_ORDER_LIST_SUCCESS:
            state.orderList = action.data;
            state.orderLength = action.length;
            break;
        case LOAD_ORDER_LIST_ERROR:
            break;
        case ON_CHANGE_STATUS_ORDER:
            for(let i = 0 ; i < state.orderList.length; i++){
                if(state.orderList[i]._id === action.orderObject._id){
                    state.orderList[i].status = action.status
                }
            }
            break;
        case ON_EDIT_ORDER_STATUS_PENDING:
            break;
        case ON_EDIT_ORDER_STATUS_SUCCESS:
            break;
        case ON_EDIT_ORDER_STATUS_ERROR:
            break;
        default:
            break;

    }
    return{...state};
}

export default adminOrderReducer