import { 
    SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_PENDING,
    SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_SUCCESS,
    SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_ERROR,
    ON_REQUEST_EDIT_CATEGORY_PENDING,
    ON_REQUEST_EDIT_CATEGORY_SUCCESS,
    ON_REQUEST_EDIT_CATEGORY_ERROR,
    ON_LOAD_ALL_CATEGORY_PENDING,
    ON_LOAD_ALL_CATEGORY_SUCCESS, 
    ON_LOAD_ALL_CATEGORY_ERROR,
    ON_DELETE_CATEGORY_PENDING,
    ON_DELETE_CATEGORY_SUCCESS,
    ON_DELETE_CATEGORY_ERROR
} from "../../constant/admin/admin.CategoryConstant";

const initialState = {
    categoryList : [],
    categoryLength: 0,
    editCategoryPending: false,
    editCategoryStatus: false,
    deleteCategoryStatus:  false,
};

const adminCategoryReducer = (state = initialState, action) => {
    switch(action.type){
        case SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_PENDING:
            break;
        case SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_SUCCESS:
            state.categoryList = action.data;
            state.categoryLength = action.productTypeLength
            break;
        case SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_ERROR:
            break;
        case ON_REQUEST_EDIT_CATEGORY_PENDING:
            state.editCategoryPending = true;
            state.editCategoryStatus = false;
            state.deleteCategoryStatus = false;
            break;
        case ON_REQUEST_EDIT_CATEGORY_SUCCESS:
            state.editCategoryPending = false;
            state.editCategoryStatus  = true;
            break;
        case ON_REQUEST_EDIT_CATEGORY_ERROR:
            
            break;
        case ON_LOAD_ALL_CATEGORY_PENDING:
            break;
        case ON_LOAD_ALL_CATEGORY_SUCCESS:
            state.categoryList = action.data;
            break;
        case ON_LOAD_ALL_CATEGORY_ERROR:
            break;
        case ON_DELETE_CATEGORY_PENDING:
            state.editCategoryStatus = false;
            state.deleteCategoryStatus = false;
            break;
        case ON_DELETE_CATEGORY_SUCCESS:
            state.deleteCategoryStatus = true;
            break;
        case ON_DELETE_CATEGORY_ERROR:
            
            break;
        default:
            break;

    }
    return{...state};
}

export default adminCategoryReducer