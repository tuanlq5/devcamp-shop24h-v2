import { 
    LOAD_PRODUCT_LIST_PENDING,
    LOAD_PRODUCT_LIST_SUCCESS,
    LOAD_PRODUCT_LIST_ERROR
} from "../../constant/admin/admin.constant";
import { 
    EDIT_PRODUCT_ERROR, 
    EDIT_PRODUCT_SUCCESS, 
    EDIT_PRODUCT_PENDING,
    SET_BACK_EDIT_STATUS,
    DELETE_PRODUCT_PENDING,
    DELETE_PRODUCT_SUCCESS,
    DELETE_PRODUCT_ERROR,
    SET_BACK_DELETE_STATUS
} from "../../constant/admin/admin.ProductConstant";


const initialState = {
    productList: [],
    productLength: 0,
    editProductStatus: false,
    deleteProductStatus: false,
    pendingEditProduct:  false
}

const adminProductReducer = (state = initialState, action) => {
    switch(action.type){
        case LOAD_PRODUCT_LIST_PENDING:
            break;
        case LOAD_PRODUCT_LIST_SUCCESS:
            state.productList = action.data;
            state.productLength = action.length;
            break;
        case LOAD_PRODUCT_LIST_ERROR:
            break;
        case EDIT_PRODUCT_PENDING:
            state.editProductStatus = false;
            state.deleteProductStatus = false;
            state.pendingEditProduct = true;
            break;
        case EDIT_PRODUCT_SUCCESS:
            state.editProductStatus = true;
            state.pendingEditProduct =  false;
            break;
        case EDIT_PRODUCT_ERROR:
            break;
        case SET_BACK_EDIT_STATUS:
            state.editProductStatus = false;
            break;
        case SET_BACK_DELETE_STATUS:
            state.deleteProductStatus = false;
            break;
        case DELETE_PRODUCT_PENDING:
            state.deleteProductStatus = false;
            state.editProductStatus = false;
            break;
        case DELETE_PRODUCT_SUCCESS:
            state.deleteProductStatus = true;
            break;
        case DELETE_PRODUCT_ERROR:
            console.log(action.error);
            break;
    default:
        break;
    }
    return{...state}
}

export default adminProductReducer