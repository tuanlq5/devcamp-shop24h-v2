import { 
    OPEN_SIDE_BAR,
    ClOSE_SIDE_BAR 
} from "../constant/constant";

const initialState = {
    openSideBar: false
}

const sideBarReducer = ( state = initialState, action) => {
    switch (action.type){
        case OPEN_SIDE_BAR:
            state.openSideBar = true;
            break;
        case ClOSE_SIDE_BAR:
            state.openSideBar = false;
            break;
        default:
            break;
    }
    return{...state};
}

export default sideBarReducer;