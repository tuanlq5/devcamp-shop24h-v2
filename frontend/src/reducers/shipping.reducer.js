import {
    ON_CHANGE_ADDRESS_SHIPPING_ADDRESS,
    ON_CHANGE_ADMINISTRATIVE_DIVISION_SHIPPING_ADDRESS,
    ON_CHANGE_CITY_SHIPPING_ADDRESS,
    ON_CHANGE_COUNTRY_SHIPPING_ADDRESS,
    ON_CHANGE_EMAIL_SHIPPING_ADDRESS,
    ON_CHANGE_FULL_NAME_SHIPPING_ADDRESS,
    ON_CHANGE_PHONE_SHIPPING_ADDRESS,
    ON_CHANGE_ZIPCODE_SHIPPING_ADDRESS,
    ON_SEND_SHIPPING_ADDRESS_PENDING,
    ON_SEND_SHIPPING_ADDRESS_SUCCESS,
    ON_SEND_SHIPPING_ADDRESS_ERROR,
    VALIDATE_SHIPPING_ADDRESS
} from "../constant/shipping.constant"


const initialState =  {
    validateFullName: false,
    validateEmail: false,
    validatePhone: false,
    validateZipcode: false,
    validateAddress: false,
    validateCity: false,
    validateCountry: false,
    validateAdministrativeDivision: false,
    ShippingInfo: {
        fullName: "",
        email:"",
        phone:"",
        zipcode :"",
        address:"",
        city:"",
        country: "",
        administrativeDivision:"",
        order: ""
    },
};

const shippingReducer = (state = initialState, action) => {
    switch(action.type) {
        case ON_CHANGE_FULL_NAME_SHIPPING_ADDRESS:
            state.ShippingInfo.fullName = action.value;
            state.ShippingInfo.order = action.order;
            break;

        case ON_CHANGE_EMAIL_SHIPPING_ADDRESS:
            state.ShippingInfo.email = action.value;
            break;

        case ON_CHANGE_ADDRESS_SHIPPING_ADDRESS:
            state.ShippingInfo.address = action.value;
            break;

        case ON_CHANGE_PHONE_SHIPPING_ADDRESS:
            state.ShippingInfo.phone = action.value;
            break;

        case ON_CHANGE_ZIPCODE_SHIPPING_ADDRESS:
            state.ShippingInfo.zipcode = action.value;
            break;

        case ON_CHANGE_CITY_SHIPPING_ADDRESS:
            state.ShippingInfo.city = action.value;
            break;

        case ON_CHANGE_COUNTRY_SHIPPING_ADDRESS:
            state.ShippingInfo.country = action.value;
            break;

        case ON_CHANGE_ADMINISTRATIVE_DIVISION_SHIPPING_ADDRESS:
            state.ShippingInfo.administrativeDivision = action.value;
            break;

        case VALIDATE_SHIPPING_ADDRESS:
            if(state.ShippingInfo.fullName === ""){
                state.validateFullName = true;   
            }else{
                state.validateFullName = false;   
            }

            if(state.ShippingInfo.email === ""){
                state.validateEmail = true;  
            }else{
                state.validateEmail = false;   
            }

            if(state.ShippingInfo.phone === ""){
                state.validatePhone = true;   
            }else{
                state.validatePhone = false;
            }

            if(state.ShippingInfo.address === ""){
                state.validateAddress = true;
            }else{
                state.validateAddress = false
            }

            if(state.ShippingInfo.zipcode === ""){
                state.validateZipcode = true;    
            }else{
                state.validateZipcode = false
            }

            if(state.ShippingInfo.city === ""){
                state.validateCity = true;    
            }else{
                state.validateCity = false
            }

            if(state.ShippingInfo.country === ""){
                state.validateCountry = true;    
            }else{
                state.validateCountry = false
            }
            
            if(state.ShippingInfo.administrativeDivision === ""){
                state.validateAdministrativeDivision = true;   
            }else{
                state.validateAdministrativeDivision = false;
            }
            break;

        case ON_SEND_SHIPPING_ADDRESS_PENDING:
            break;

        case ON_SEND_SHIPPING_ADDRESS_SUCCESS:
            break;

        case ON_SEND_SHIPPING_ADDRESS_ERROR:
            break;

    default:
        break
    }
    return {...state}
}

export default shippingReducer