import { 
    ON_CLICK_MORE_DETAIL_PRODUCT,
    ON_SEND_FIND_PRODUCT_BY_ID_PENDING,
    ON_SEND_FIND_PRODUCT_BY_ID_SUCCESS,
    ON_SEND_FIND_PRODUCT_BY_ID_ERROR,
    ON_CHANGE_INPUT_NUMBER_OF_QUANTITY,
    ON_CLICK_ADD_PRODUCT_TO_CART,
    SEND_DATA_FROM_LOCALSTORAGE_TO_STORE_CART,
    ON_CLICK_DECREASE_QUANTITY_IN_CART, 
    ON_CLICK_INCREASE_QUANTITY_IN_CART,
    ON_CLICK_DELETE_PRODUCT_IN_CART,
    ON_PROCEED_CHECKOUT_PENDING, 
    ON_PROCEED_CHECKOUT_SUCCESS,
    ON_PROCEED_CHECKOUT_ERROR,
    ON_CLICK_SHIPPING_EMS,
    ON_CLICK_SHIPPING_FEDEX,
    ON_CLICK_SHIPPING_DHL,
    ON_CLICK_SHIPPING_NHATTIN,
    ON_CLICK_SHIPPING_UPS,
    ON_CLICK_SHIPPING_TNT,
    ON_CLICK_PROCEDD_PAYMENT,
    ON_CLICK_PAY,
    ON_CHANGE_ORDER_STATUS_ERROR,
    ON_CHANGE_ORDER_STATUS_SUCCESS,
    ON_SEND_ORDER_GOOGLE_ACCOUNT_PENDING,
    ON_SEND_ORDER_GOOGLE_ACCOUNT_SUCCESS,
    ON_SEND_ORDER_GOOGLE_ACCOUNT_ERROR,
    
} from "../constant/productDetail.constant";

import { 
    ON_CHANGE_COUPON_CODE,
    ON_CHECK_COUPON_ERROR,
    ON_CHECK_COUPON_SUCCESS,
    ON_CHECK_COUPON_PENDING
} from "../constant/shop.constant";

import {
    UPDATE_PAYMENT_FOR_GOOGLE_ACCOUNT
}from "../constant/shipping.constant";

const initialState = {
    productId : "",
    productObject : {},
    productFindByType : [],
    productAddtoCart: {
        name:"",
        price: 0,
        quantity: 0,
        image:[],
        id: ""
    },
    productCart: [],
    quantity: 0,
    numberOfProductInCart : 0,
    coupon: "",
    percentDiscount: 0,
    couponId:"",
    customer: {},
    shippingCost : 0,
    payment: false,
    confirmOrder: false,
};


const productDetailReducer = (state = initialState, action) => {
    switch(action.type){
        case ON_CLICK_MORE_DETAIL_PRODUCT:
            state.productId = action.productId;
            break;
        case ON_SEND_FIND_PRODUCT_BY_ID_PENDING:
            break;
        case ON_SEND_FIND_PRODUCT_BY_ID_SUCCESS:
            state.productObject = action.data;
            state.productFindByType = action.dataProducType;
            break;
        case ON_SEND_FIND_PRODUCT_BY_ID_ERROR:
            break;
        case ON_CHANGE_INPUT_NUMBER_OF_QUANTITY:
            state.quantity = Number(action.quantity);
            state.productAddtoCart.quantity = state.quantity;
            state.productAddtoCart.name = action.data.name;
            state.productAddtoCart.price = action.data.buyPrice;
            state.productAddtoCart.image = action.data.imageURL;
            state.productAddtoCart.id = action.data._id
            break;
        case ON_CLICK_ADD_PRODUCT_TO_CART:
            break;
        case SEND_DATA_FROM_LOCALSTORAGE_TO_STORE_CART:
            state.productCart = action.data;
            break;
        case ON_CLICK_INCREASE_QUANTITY_IN_CART:
            //find the product base on name
            const index =  state.productCart.findIndex((item) => item.name === action.data);
            //change the quantity
            state.productCart[index].quantity += 1;
            // change the quantity in localstorage
            const cart1 = JSON.parse(localStorage.getItem("cart"));
            const index5 = cart1.findIndex((item) => item.name === action.data);
            cart1[index5].quantity += 1;
            localStorage.setItem('cart',JSON.stringify(cart1));
            break;
        case ON_CLICK_DECREASE_QUANTITY_IN_CART:
            //find the product base on name
            const index2 =  state.productCart.findIndex((item) => item.name === action.data);
            //change the quantity
            state.productCart[index2].quantity -= 1; 
            // the quantity can not be less than 1
            if(state.productCart[index2].quantity < 1) {
                state.productCart[index2].quantity = 1
            } 
            const cart = JSON.parse(localStorage.getItem("cart"));
            const index4 = cart.findIndex((item) => item.name === action.data);
            cart[index4].quantity -= 1;
            // the quantity can not be less than 1
            if(cart[index4].quantity < 1) {
                cart[index4].quantity = 1
            } 
            // change the quantity in localstorage
            localStorage.setItem('cart',JSON.stringify(cart));
            break;
        case ON_CLICK_DELETE_PRODUCT_IN_CART:
            //find the product base on name
            const index3 = state.productCart.findIndex((item) => item.name === action.data);
            //delete the product from cart
            state.productCart.splice(index3 , 1); //1 mean only remove 1 element only
            // edit in local storage
            const cart2 =  JSON.parse(localStorage.getItem("cart"));
            const index6 = cart2.findIndex((item) => item.name === action.data);
            cart2.splice(index6,1);
            localStorage.setItem("cart", JSON.stringify(cart2))
            break;
        case ON_CHANGE_COUPON_CODE:
            state.coupon = action.value;
            break;
        case ON_CHECK_COUPON_PENDING:
            state.percentDiscount = 0;
            break;
        case ON_CHECK_COUPON_SUCCESS:
            state.percentDiscount = action.data[0].discountPercent;
            state.couponId = action.data[0]._id;
            break;
        case ON_CHECK_COUPON_ERROR:
            state.percentDiscount = 0;
            state.coupon = ""
            break;
        case ON_PROCEED_CHECKOUT_PENDING:
            break;
        case ON_PROCEED_CHECKOUT_SUCCESS:
            break;
        case ON_PROCEED_CHECKOUT_ERROR:
            break;
        case ON_CLICK_SHIPPING_EMS:
            state.shippingCost = 10;
            break;
        case ON_CLICK_SHIPPING_DHL:
            state.shippingCost = 30;
            break;
        case ON_CLICK_SHIPPING_FEDEX:
            state.shippingCost = 40;
            break;
        case ON_CLICK_SHIPPING_UPS:
            state.shippingCost = 35;
            break;
        case ON_CLICK_SHIPPING_TNT:
            state.shippingCost = 24;
            break;
        case ON_CLICK_SHIPPING_NHATTIN:
            state.shippingCost = 15;
            break;
        case ON_CLICK_PROCEDD_PAYMENT:
            state.payment = true;
            break;
        case ON_CLICK_PAY:
            state.confirmOrder = true;
            break;
        case ON_CHANGE_ORDER_STATUS_SUCCESS:
            break;
        case ON_CHANGE_ORDER_STATUS_ERROR:
            break;
        case ON_SEND_ORDER_GOOGLE_ACCOUNT_PENDING:
            break;
        case ON_SEND_ORDER_GOOGLE_ACCOUNT_SUCCESS:
            break;
        case ON_SEND_ORDER_GOOGLE_ACCOUNT_ERROR:
            console.log(action.error);
            break;
        case UPDATE_PAYMENT_FOR_GOOGLE_ACCOUNT:
            state.payment = true;
            break;
    default: 
        break;
    }
    return{...state};
}

export default productDetailReducer