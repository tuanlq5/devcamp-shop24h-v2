import { combineReducers } from "redux";
import productReducer from "./product.reducer";
import sideBarReducer from "./sidebar.reducer";
import productDetailReducer from "./productDetail.reducer";
import shippingReducer from "./shipping.reducer";
import adminOrderReducer from "./admin/orderReducer";
import adminCustomerReducer from "./admin/customerReducer";
import adminCouponReducer from "./admin/couponReducer";
import adminCategoryReducer from "./admin/categoryReducer";
import adminProductReducer from "./admin/productReducer";
import dataAnalyzeReducer from "./admin/dataAnalyzeReducer";
const rootReducer = combineReducers({
    productReducer,
    sideBarReducer,
    productDetailReducer,
    shippingReducer,
    adminOrderReducer,
    adminCustomerReducer,
    adminCouponReducer,
    adminCategoryReducer,
    adminProductReducer,
    dataAnalyzeReducer
});

export default rootReducer;