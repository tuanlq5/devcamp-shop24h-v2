export const ON_CHANGE_FULL_NAME_SHIPPING_ADDRESS = "Khi thay đổi giá trị full name trên trang shipping";

export const ON_CHANGE_EMAIL_SHIPPING_ADDRESS = "Khi thay đổi giá trị email trên trang shipping";

export const ON_CHANGE_ADDRESS_SHIPPING_ADDRESS = "Khi người dùng thay đổi giá trị address trên shipping address";

export const ON_CHANGE_PHONE_SHIPPING_ADDRESS = "Khi người dùng thay đổi giá trị Phone trên shipping address";

export const ON_CHANGE_CITY_SHIPPING_ADDRESS = "Khi người dùng thay đổi giá trị City trên shipping address";

export const ON_CHANGE_ZIPCODE_SHIPPING_ADDRESS = "Khi người dùng thay đổi giá trị zipcode trên shipping address";

export const ON_CHANGE_COUNTRY_SHIPPING_ADDRESS  = "Khi người dùng thay đổi giá trị country trên shipping address";

export const ON_CHANGE_ADMINISTRATIVE_DIVISION_SHIPPING_ADDRESS = "Khi người dùng thay đổi giá trị administrative division trên shipping address";

export const ON_SEND_SHIPPING_ADDRESS_PENDING = "Bắt đầu gửi thông tin shipping tới máy chủ";

export const ON_SEND_SHIPPING_ADDRESS_SUCCESS  = "Gửi thông tin shipping tới máy chủ thành công";

export const ON_SEND_SHIPPING_ADDRESS_ERROR = "Gửi thông tin shipping tới máy chủ không thành công";

export const VALIDATE_SHIPPING_ADDRESS = "VALIDATE SHIPPINNG ADDRESS";

export const UPDATE_PAYMENT_FOR_GOOGLE_ACCOUNT = "update payment for google account";