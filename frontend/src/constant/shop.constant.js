export  const CHANGE_PRODUCT_NUMBER = "thay đổi số sản phẩm";

export  const ON_CHANGE_FILTER_NAME = "lưu giá trị khi tên sản phẩm được nhập";

export  const ON_CHANGE_FILTER_MIN_PRICE = "Lưu giá trị khi giá trị min được nhập";

export  const ON_CHANGE_FILTER_MAX_PRICE = "Lưu giá trị khi giá trị max được nhập";

export  const ON_CHANGE_FILTER_TYPE_SOFA = "Lưu giá trị filter type khi nút sofa được tick";

export const ON_CHANGE_FILTER_TYPE_DESK = "Lưu giá trị filter type khi nút desk được tick";

export const ON_CHANGE_FILTER_TYPE_CHAIR = "Lưu giá trị filter type khi nút chair được tick";

export const ON_CHANGE_FILTER_TYPE_BOOKSHELF = "Lưu giá trị filter type khi nút bookshelf được tick";

export const ON_CHANGE_FILTER_TYPE_TVSTAND = "Lưu giá trị filter type khi nút tv stand được tick";

export const ON_CHANGE_FILTER_TYPE_BED = "Lưu giá trị filter type khi nút bed được tick";

export const ON_CHANGE_FILTER_TYPE_DECORITEM = "Lưu giá trị filter type khi nút decor item được tick";

export const ON_CHANGE_CURRENT_PAGE_NUMBER = "Thay đổi số page trên panigation";

export const ON_CLICK_FILTER_PRODUCT = "Khi nút filter được ấn";

export const ON_CHANGE_COUPON_CODE = "Lưu dữ liệu khi người dùng nhập coupon";

export const ON_CHECK_COUPON_PENDING = "Bắt đầu check mã vouccher ";

export const ON_CHECK_COUPON_SUCCESS = "mã coupon hợp lệ";

export const ON_CHECK_COUPON_ERROR = "Mã coupon không hợp lệ"

