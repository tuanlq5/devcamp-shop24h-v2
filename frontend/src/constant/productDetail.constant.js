export const ON_CLICK_MORE_DETAIL_PRODUCT = 'Khi người dùng ấn nút more detail trên sản phẩm ';

export const ON_SEND_FIND_PRODUCT_BY_ID_PENDING = "Bắt đầu tìm sản phẩm theo object id ";

export const ON_SEND_FIND_PRODUCT_BY_ID_SUCCESS = "Tìm sản phẩm theo Id thành công";

export const ON_SEND_FIND_PRODUCT_BY_ID_ERROR = "Tìm sản phẩm theo Id thất bại";

export const ON_CHANGE_INPUT_NUMBER_OF_QUANTITY = "Khi quantity được nhập ";

export const ON_CLICK_ADD_PRODUCT_TO_CART = "Khi nút thêm sản phẩm vào giỏ hàng được ấn";

export const SEND_DATA_FROM_LOCALSTORAGE_TO_STORE_CART = "Gửi thông tin local store đến redux store";

export const ON_CLICK_INCREASE_QUANTITY_IN_CART = "Khi ấn nút thêm quantity trên giỏ hàng";

export const ON_CLICK_DECREASE_QUANTITY_IN_CART = "Khi ấn nút trừ quantity trên giỏ hàng";

export const ON_CLICK_DELETE_PRODUCT_IN_CART  = "Khi người dùng ấn nút delete product trên giỏ hàng";

export const ON_PROCEED_CHECKOUT_PENDING = "Bắt đầu check out";

export const ON_PROCEED_CHECKOUT_SUCCESS = "Check out thành công";

export const ON_PROCEED_CHECKOUT_ERROR = "Check out không thành công";

export const ON_CLICK_SHIPPING_EMS = "Khi nggười dùng chọn shipping bằng ems";

export const ON_CLICK_SHIPPING_UPS = "Khi người dùng chọn shipping bằng UPS";

export const ON_CLICK_SHIPPING_DHL = "Khi người dùng chọn shipping bằng DHL";

export const ON_CLICK_SHIPPING_FEDEX = "Khi người dùng chọn shipping bằng Fedex";

export const ON_CLICK_SHIPPING_TNT = "Khi người dùng chọn shipping bằng TNT";

export const ON_CLICK_SHIPPING_NHATTIN = "Khi người dùng chọn shipping bằng Nhật Tín";

export const ON_CLICK_PROCEDD_PAYMENT = "Khi người dùng ấn nút Proceed Payment";

export const ON_CLICK_PAY = "Khi người dùng ấn nút pay";

export const ON_CHANGE_ORDER_STATUS_SUCCESS = "Khi thay đổi status order thành công";

export const ON_CHANGE_ORDER_STATUS_ERROR = "Khi thay đổi status order thất bại";

export const ON_SEND_ORDER_GOOGLE_ACCOUNT_PENDING = "Khi người dùng ấn nút proceed payment with google account";

export const ON_SEND_ORDER_GOOGLE_ACCOUNT_SUCCESS = "Send order bằng google account thành công";

export const ON_SEND_ORDER_GOOGLE_ACCOUNT_ERROR = "Send order bằng google account thất bại";

