export const ADD_PRODUCT_DATA = "Thêm dữ liệu vào updateProduct khi người dúng ấn edit trên table"

export const EDIT_PRODUCT_PENDING = "Thay đổi dữ liệu product ";

export const EDIT_PRODUCT_SUCCESS = "Thay đổi dữ liệu product thành công";

export const EDIT_PRODUCT_ERROR = "Thay đổi dữ liệu product không thành công";

export const SET_BACK_EDIT_STATUS = "Thay đổi status của edit product khi đóng modal";

export const DELETE_PRODUCT_PENDING = "Delete dữ liệu product ";

export const DELETE_PRODUCT_SUCCESS = "Delete dữ liệu product thành công";

export const DELETE_PRODUCT_ERROR = "Delete dữ liệu product không thành công";

export const SET_BACK_DELETE_STATUS = "Thay đổi status của delete product khi đóng modal";

