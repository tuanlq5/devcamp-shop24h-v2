export const LOAD_ORDER_LIST_PENDING = "Bắt đấu lấy dữ liệu order";

export const LOAD_ORDER_LIST_SUCCESS = "Lấy dữ liệu order thành công";

export const LOAD_ORDER_LIST_ERROR = "lấy dữ liệu order không thành công";

export const ON_CHANGE_STATUS_ORDER = "Khi người dùng thay đổi status";

export const ON_EDIT_ORDER_STATUS_PENDING = "Bắt đầu thay đổi giá trị status order";

export const ON_EDIT_ORDER_STATUS_SUCCESS = "Thay đổi giá trị order thành công";

export const ON_EDIT_ORDER_STATUS_ERROR = "Thay đổi giá trị order không thành công";

export const LOAD_PRODUCT_LIST_PENDING = "Bắt đầu lấy dữ liệu Product";

export const LOAD_PRODUCT_LIST_SUCCESS = "lấy dữ liệu product thành công";

export const LOAD_PRODUCT_LIST_ERROR = "Lấy dữ liệu product thất bại";

export const LOAD_CUSTOMER_LIST_PENDING = "Lấy dữ liệu người dùng bắt đầu";

export const LOAD_CUSTOMER_LIST_SUCCESS = "Lấy dữ liệu người dùng thành công";

export const LOAD_CUSTOMER_LIST_ERROR = "Lấy dữ liệu người dùng thất bại";

export const LOAD_LIST_COUPON_PENDING = "Lấy dữ liệu coupon bắt đầu";

export const LOAD_LIST_COUPON_SUCCESS = "Lấy dữ liệu coupon thành công";

export const LOAD_LIST_COUPON_ERROR = "Lấy dữ liệu coupon không thành công";

export const ADMIN_REGISTER_PENDING = "Tạo Thông tin tài khoản admin ";

export const ADMIN_REGISTER_SUCCESS = "Tạo thông tin tài khoản admin thành công";

export const ADMIN_REGISTER_ERROR = "Tạo thông tin tài khoản admin không thành công";

export const lOAD_CATEGORY_ADMIN_PENDING = "lấy dữ liệu category từ server";

export const lOAD_CATEGORY_ADMIN_SUCCESS = "Lấy dữ liệu category thành công";

export const lOAD_CATEGORY_ADMIN_ERROR = "Lấy dữ liệu category thất bại";


