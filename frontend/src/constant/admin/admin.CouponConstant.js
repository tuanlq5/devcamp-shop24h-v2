export const ON_EDIT_COUPON_PENDING = "Bắt đầu gửi coupon tới server "

export const ON_EDIT_COUPON_SUCCESS = "Edit coupon thành công";

export const ON_EDIT_COUPON_ERROR = "Edit coupon không thành công";

export const ON_DELETE_COUPON_PENDING = "Bắt đầu delete coupon tới server "

export const ON_DELETE_COUPON_SUCCESS = "Delete coupon thành công";

export const ON_DELETE_COUPON_ERROR = "Delete coupon không thành công";

export const ON_UPDATE_EDIT_COUPON_STATUS = "Update coupon edit statuts";

export const ON_UPDATE_DELETE_COUPON_STATUS = "Update coupon delete status";