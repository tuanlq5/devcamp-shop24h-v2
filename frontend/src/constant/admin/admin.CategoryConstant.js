export const SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_PENDING = "Send request TAKE ALL category list";

export const SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_SUCCESS = "Send request take all category list success";

export const SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_ERROR = "Send request take all category list error";

export const ON_REQUEST_EDIT_CATEGORY_PENDING = "On request edit category";

export const ON_REQUEST_EDIT_CATEGORY_SUCCESS = "On request edit category success";

export const ON_REQUEST_EDIT_CATEGORY_ERROR = "On request edit category error";

export const ON_LOAD_ALL_CATEGORY_PENDING = "On load all category pending" ;

export const ON_LOAD_ALL_CATEGORY_SUCCESS = "On load all category success";

export const ON_LOAD_ALL_CATEGORY_ERROR = "On load all category error";

export const ON_DELETE_CATEGORY_PENDING = 'on delete category pendning';

export const ON_DELETE_CATEGORY_SUCCESS = "on delete category success";

export const ON_DELETE_CATEGORY_ERROR = "on delete category error";