export const ON_CHANGE_CUSTOMER_FULL_NAME = 'Lưu giá trị customer full name khi nhập';

export const ON_CHANGE_CUSTOMER_EMAIL = "lưu giá trị email khi được nhập";

export const ON_CHANGE_CUSTOMER_PHONE = "Lưu giá trị phone khi được nhập";

export const ON_CHANGE_CUSTOMER_ADDRESS = "Lưu giá trị address khi được nhập";

export const ON_CHANGE_CUSTOMER_CITY = "Lưu giá trị city khi được nhập";

export const ON_CHANGE_CUSTOMER_COUNTRY = "Lưu giá trị country khi được nhập";

export const ON_CHANGE_CUSTOMER_ADMINISTRATIVE_DIVISION = "Lưu giá trị administrative division khi nhập";

export const UPDATE_CUSTOMER_PENDING = "Bắt đầu edit thông tin người dùng";

export const UPDATE_CUSTOMER_SUCCESS = "Edit thông tin người dùng thành công";

export const UPDATE_CUSTOMER_ERROR =  "Edit thông tin người dùng thất bại";

export const DELETE_CUSTOMER_PENDING = "Delete Customer";

export const DELETE_CUSTOMER_SUCCESS = "Delete người dùng thành công";

export const DELETE_CUSTOMER_ERROR = "Delete người dùng không thành công";