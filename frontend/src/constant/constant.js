export const OPEN_SIDE_BAR = "Open Side Bar";

export const ClOSE_SIDE_BAR = "Close Site Bar";

export const SEND_REQUEST_TAKE_PRODUCT_PENDING = "Bắt đầu lấy dữ liệu product từ database";

export const SEND_REQUEST_TAKE_PRODUCT_START = "Lấy dữ liệu product thành công";

export const SEND_REQUEST_TAKE_PRODUCT_END = "Lấy dữ liệu product thất bại";

export const SEND_AUTHENTICATION_TO_GOOGLE_PENDING = "Bắt đầu lấy dữ liệu user google account ";

export const SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS = "Lấy dữ liệu google acount thành công";

export const SEND_AUTHENTICATION_TO_GOOGLE_END = "Lấy dữ liệu google account thất bại";

export const SIGN_OUT_GOOGLE_ACCOUNT = "Log out google account";

export const ON_CHANGE_FULLNAME_REGISTER = "lưu thông tin khi người dung nhập fullname";

export const ON_CHANGE_PHONE_REGISTER = "Lưu thông tin khi người dùng nhập Phone";

export const ON_CHANGE_EMAIL_REGISTER = "lưu thông tin khi người dùng nhập email";

export const ON_CHANGE_PASSWORD_REGISTER = "Lưu thông tin khi người dùng nhập fullname";

export const ON_CHANGE_ADDRESS_REGISTER = "Lưu thông tin khi người dùng nhập address";

export const ON_CHANGE_CITY_REGISTER = "Lưu thông tin khi người dùng nhập city";

export const ON_CHANGE_COUNTRY_REGISTER = "Lưu thông tin khi người dùng nhập country";

export const ON_CHANGE_POLICY_REGISTER = "Lưu thông tin khi người tick accept policy";

export const ON_CHANGE_ADMINISTRATOR_REGISTER = "Lưu thông tin adminitrative division";

export const ON_CHANGE_ADMIN_REGISTER = "Lưu thông tin Admin code";

export const REGISTER_CUSTOMER_ACCOUNT_PENDING = "Register new customer pending";

export const REGISTER_CUSTOMER_ACCOUNT_SUCCESS = "Regsiter customer account success";

export const REGISTER_CUSTOMER_ACCOUNT_ERROR = "Register new customer fail";

export const ON_CHANGE_EMAIL_LOGIN = "lưu thông tin email khi login";

export const ON_CHANGE_PASSWORD_LOGIN = "Lưu thông tin pasword khi login";

export const ON_LOGIN_CUSTOMER_PENDING = "Khi người dùng ấn nút Log in";

export const ON_LOGIN_CUSTOMER_SUCCESS = "Khi người dùng ấn nút login thành công";

export const ON_LOGIN_CUSTOMER_ERROR = "Khi người dúng ấn nút Login nhưng không thành công";

export const ON_REFRESH_CUSTOMER_ACCOUNT_PENDING = "Giữ thông tin người dùng khi tải lại trang";

export const ON_REFRESH_CUSTOMER_ACCOUNT_SUCCESS = "Giữ thông tin người dùng khi tải lại trang thành công";

export const ON_REFRESH_CUSTOMER_ACCOUNT_ERROR = "Giữ thông tin người dùng khi tải lại trang thất bại";

export const ON_LOGOUT_CUSTOMER_ACCOUNT_PENDING = "LOG OUT thông tin người dùng khi tải lại trang";

export const ON_LOGOUT_CUSTOMER_ACCOUNT_SUCCESS = "LOG OUT thông tin người dùng khi tải lại trang thành công";

export const ON_LOGOUT_CUSTOMER_ACCOUNT_ERROR = "LOG OUT thông tin người dùng khi tải lại trang không thành công";

export const ON_REGISTER_ADMIN_ACCOUNT_PENDING = "Khi admin tạo tài khoản";

export const ON_REGISTER_ADMIN_ACCOUNT_SUCCESS = "Khi người dùng tạo tài khoản thành công";

export const ON_REGISTER_ADMIN_ACCOUNT_ERROR = "Khi người dùng tạo tài khoản không thành công";

export const SET_LOADING_CUSTOMER = "loading customer";