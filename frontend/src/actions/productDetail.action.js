import { 
    ON_CLICK_MORE_DETAIL_PRODUCT ,
    ON_SEND_FIND_PRODUCT_BY_ID_ERROR,
    ON_SEND_FIND_PRODUCT_BY_ID_PENDING,
    ON_SEND_FIND_PRODUCT_BY_ID_SUCCESS,
    ON_CHANGE_INPUT_NUMBER_OF_QUANTITY,
    ON_CLICK_ADD_PRODUCT_TO_CART,
    SEND_DATA_FROM_LOCALSTORAGE_TO_STORE_CART,
    ON_CLICK_DECREASE_QUANTITY_IN_CART, 
    ON_CLICK_INCREASE_QUANTITY_IN_CART,
    ON_CLICK_DELETE_PRODUCT_IN_CART,
    ON_PROCEED_CHECKOUT_PENDING,
    ON_PROCEED_CHECKOUT_SUCCESS,
    ON_PROCEED_CHECKOUT_ERROR,
    ON_CLICK_SHIPPING_EMS,
    ON_CLICK_SHIPPING_UPS,
    ON_CLICK_SHIPPING_DHL,
    ON_CLICK_SHIPPING_FEDEX,
    ON_CLICK_SHIPPING_TNT,
    ON_CLICK_SHIPPING_NHATTIN,
    ON_CLICK_PROCEDD_PAYMENT,
    ON_CLICK_PAY,
    ON_CHANGE_ORDER_STATUS_SUCCESS,
    ON_CHANGE_ORDER_STATUS_ERROR,
    ON_SEND_ORDER_GOOGLE_ACCOUNT_PENDING,
    ON_SEND_ORDER_GOOGLE_ACCOUNT_SUCCESS,
    ON_SEND_ORDER_GOOGLE_ACCOUNT_ERROR
} from "../constant/productDetail.constant";


export const onSendOrderForGoogleAccount = (productCart, totalPay, uid) => {
    return async (dispatch) => {
        try{
            dispatch({
                type: ON_SEND_ORDER_GOOGLE_ACCOUNT_PENDING
            })
            // Pretent shiping cost is the same 
            //call api creat order base on customer id
            const conditionCreateOrder = {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8"
                },
                body: JSON.stringify({
                    note : "NoThing", 
                    cost: totalPay  ,
                    status: "Open",
                    uid: uid
                }) 
            }
            // api create order
            const CreateOrder = await fetch ("http://localhost:8000/orderGoogleAccount", conditionCreateOrder);

            const dataOrder = await CreateOrder.json();

            //  call api to post the product cart to order detail
            for(let i = 0;  i < productCart.length;  i ++){
                const conditionCreateOrderDetail = {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json; charset=utf-8"
                    },
                    body: JSON.stringify({
                        quantity: productCart[i].quantity,
                        product: productCart[i].id,
                        orderId: dataOrder.data._id
                    }) 
                }
                //api create order detail
                await fetch("http://localhost:8000/orderDetail", conditionCreateOrderDetail);
              
            }

            //send back order after update
            const findOrder = await fetch("http://localhost:8000/order/" + dataOrder.data._id, {method: "GET"});
            const Order = await findOrder.json();
            localStorage.setItem("order", JSON.stringify(Order.data));

            dispatch({
                type: ON_SEND_ORDER_GOOGLE_ACCOUNT_SUCCESS,
            })

        }catch(error){
            dispatch({
                type: ON_SEND_ORDER_GOOGLE_ACCOUNT_ERROR,
                error: error
            })
        }
    }
}
export const  onChangeOrderStatus = (orderId) =>{
    return async(dispatch) => {
        try{
            const changeOrderCondition = {
                method: "PUT",
                headers: {
                    "Content-type": "application/json; charset=utf-8"
                },
                body: JSON.stringify({
                    status: "Confirmed",
                    orderId: orderId
                })
            }
            const apiChangeOrderStatus = await fetch("http://localhost:8000/orderStatus", changeOrderCondition);

            await apiChangeOrderStatus.json();
                        
            dispatch({
                type: ON_CHANGE_ORDER_STATUS_SUCCESS,
                
            })
        }catch(error){
            dispatch({
                type: ON_CHANGE_ORDER_STATUS_ERROR,
                error: error
            })
        }
    }
}
export const onClickPay = () => {
    return{
        type: ON_CLICK_PAY
    }
}

export const onClickProceedPayment = () => {
    return{
        type: ON_CLICK_PROCEDD_PAYMENT
    }
}

export const onClickShippingEMS = () => {
    return{
        type: ON_CLICK_SHIPPING_EMS,
    }
} 

export const onClickShippingUps = () => {
    return{
        type: ON_CLICK_SHIPPING_UPS
    }
}

export const onClickShippingDHL = () => {
    return{
        type: ON_CLICK_SHIPPING_DHL
    }
}

export const onClickShippingFedex = () => {
    return{
        type: ON_CLICK_SHIPPING_FEDEX
    }
}

export const onClickShippingTnt = () => {
    return{
        type: ON_CLICK_SHIPPING_TNT
    }
}

export const onClickShippingNhatTin = () => {
    return{
        type: ON_CLICK_SHIPPING_NHATTIN
    }
}
export const onClickMoreDetailProduct = (id) => {
   return {
        type: ON_CLICK_MORE_DETAIL_PRODUCT,
        productId: id
    }
}

export const onChangeQuantity = (value, product) => {
    return{
        type: ON_CHANGE_INPUT_NUMBER_OF_QUANTITY,
        quantity: value,
        data:  product
    }
}



// Add Product to cart
export const onClickAddProductToCart = (product, quantity) => {
    return (dispatch, getState) => {
        try {
            // Retrieve cart data from local storage
            const serializedCart = localStorage.getItem('cart') ?? "[]";
            const loadedCart = JSON.parse(serializedCart) || [];
            // Check if the product is already in the cart
            const existingProductIndex = loadedCart.findIndex((item) => item.name === product.name);

            if (existingProductIndex !== -1) {
              // If the product exists in the cart, increment its quantity by one
              loadedCart[existingProductIndex].quantity += quantity;
            } else {
              // If the product doesn't exist in the cart, add it to the cart with a quantity of one
              loadedCart.push(product);
            }

            // Save updated cart data to local storage
            localStorage.setItem('cart', JSON.stringify(loadedCart));
            
            
            // Dispatch action to update cart state in Redux store
            dispatch({ type:  ON_CLICK_ADD_PRODUCT_TO_CART, payload: loadedCart });

          } catch (error) {
            console.error('Error adding item to cart', error);
          }
    }
}

// Find Product by Id
export const findProductByID = (id) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: ON_SEND_FIND_PRODUCT_BY_ID_PENDING
            })
            var requestOption = {
                method:"GET",
            }
            // Get product by ID

            const getProductByID = await fetch("http://localhost:8000/product/" + id, requestOption);

            const dataProduct = await getProductByID.json();
            
            //Get product byType
            const getProductByType = await fetch("http://localhost:8000/filterProductProductType?type=" + dataProduct.data.type, requestOption);

            const dataProductType = await getProductByType.json();
            dispatch({
                type: ON_SEND_FIND_PRODUCT_BY_ID_SUCCESS,
                data: dataProduct.data,
                dataProducType: dataProductType.data
            })
        }
        catch(error){
            return({
                type: ON_SEND_FIND_PRODUCT_BY_ID_ERROR,
                error: error
            })
        }
    }
}

export const handlerTakeDataFromLocalStorageCart = (value) =>{
    return{
        type: SEND_DATA_FROM_LOCALSTORAGE_TO_STORE_CART,
        data: value
    }
}

export const handlerIncreaseQuantityInCart = (productName) => {
    return{
        type: ON_CLICK_INCREASE_QUANTITY_IN_CART,
        data: productName,
    }
}

export const handlerDecreaseQuantityInCart = (productName) => {
    return{
        type: ON_CLICK_DECREASE_QUANTITY_IN_CART,
        data: productName
    }
}

export const handlerDeleteProductInCart = (productName) => {
    return {
        type: ON_CLICK_DELETE_PRODUCT_IN_CART,
        data: productName
    }
}

/*Concept Checkout: 
    1.  Create Order base on customer id
    2.  Then we create order detail to Order
*/
export const handleProceedCheckout = (productCart, customerData, totalPay, couponId) => {
    return async (dispatch) => {
        try{
            dispatch({
                type: ON_PROCEED_CHECKOUT_PENDING
            })
  
            //call api creat order base on customer id
            const conditionCreateOrder = {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8"
                },
                body: JSON.stringify({
                    note : "NoThing", 
                    cost: totalPay ,
                    customerId: customerData._id,
                    status: "Open",
                    couponUse: couponId
                }) 
            }
            // api create order
            const CreateOrder = await fetch ("http://localhost:8000/customer/order", conditionCreateOrder);

            const dataOrder = await CreateOrder.json();
            
            //  call api to post the product cart to order detail
            for(let i = 0;  i < productCart.length;  i ++){
                const conditionCreateOrderDetail = {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json; charset=utf-8"
                    },
                    body: JSON.stringify({
                        quantity: productCart[i].quantity,
                        product: productCart[i].id,
                        orderId: dataOrder.data._id
                    }) 
                }
                //api create order detail
                 await fetch("http://localhost:8000/orderDetail", conditionCreateOrderDetail);

            }

            //send back order after update
            const findOrder = await fetch("http://localhost:8000/order/" + dataOrder.data._id, {method: "GET"});
            const Order = await findOrder.json();
            localStorage.setItem("order", JSON.stringify(Order.data));

            dispatch({
                type: ON_PROCEED_CHECKOUT_SUCCESS,
            })

        }catch(error){
            console.log(error);
            dispatch({
                type: ON_PROCEED_CHECKOUT_ERROR,
                error: error
            })
        }
    }
}