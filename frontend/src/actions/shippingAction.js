import {
    ON_CHANGE_ADDRESS_SHIPPING_ADDRESS,
    ON_CHANGE_ZIPCODE_SHIPPING_ADDRESS,
    ON_CHANGE_ADMINISTRATIVE_DIVISION_SHIPPING_ADDRESS,
    ON_CHANGE_CITY_SHIPPING_ADDRESS,
    ON_CHANGE_COUNTRY_SHIPPING_ADDRESS,
    ON_CHANGE_EMAIL_SHIPPING_ADDRESS,
    ON_CHANGE_FULL_NAME_SHIPPING_ADDRESS,
    ON_CHANGE_PHONE_SHIPPING_ADDRESS,
    ON_SEND_SHIPPING_ADDRESS_PENDING,
    ON_SEND_SHIPPING_ADDRESS_SUCCESS,
    ON_SEND_SHIPPING_ADDRESS_ERROR,
    VALIDATE_SHIPPING_ADDRESS,
    UPDATE_PAYMENT_FOR_GOOGLE_ACCOUNT
} from "../constant/shipping.constant"

export const onChangeFullNameShippingAddress = (value, order) =>{
    return{
        type: ON_CHANGE_FULL_NAME_SHIPPING_ADDRESS,
        value: value,
        order: order
    }
} 
export const onChangeEmailShippingAddress = (value) =>{
    return{
        type: ON_CHANGE_EMAIL_SHIPPING_ADDRESS,
        value: value
    }
} 
export const onChangeAddressShippingAddress = (value) =>{
    return{
        type: ON_CHANGE_ADDRESS_SHIPPING_ADDRESS,
        value: value
    }
} 
export const onChangePhoneShippingAddress = (value) =>{
    return{
        type: ON_CHANGE_PHONE_SHIPPING_ADDRESS,
        value: value
    }
} 
export const onChangeCityShippingAddress = (value) =>{
    return{
        type: ON_CHANGE_CITY_SHIPPING_ADDRESS,
        value: value
    }
} 
export const onChangeZipcodeShippingAddress = (value) =>{
    return{
        type: ON_CHANGE_ZIPCODE_SHIPPING_ADDRESS,
        value: value
    }
} 
export const onChangeCountryShippingAddress = (value) =>{
    return{
        type: ON_CHANGE_COUNTRY_SHIPPING_ADDRESS,
        value: value
    }
} 
export const onChangeAdministrativeDivisionShippingAddress = (value) =>{
    return{
        type: ON_CHANGE_ADMINISTRATIVE_DIVISION_SHIPPING_ADDRESS,
        value: value
    }
} 

export const onSendShippingAddress = (shippingObject) => {
    return async (dispatch) => {
        try{
            
            dispatch({
                type: ON_SEND_SHIPPING_ADDRESS_PENDING
            }) 
            const condition = {
                method: "POST",
                headers: {
                    "Content-type": "application/json; charset=utf-8"
                },
                body: JSON.stringify(shippingObject)
            }
            const sendShippingApi = await fetch("http://localhost:8000/addInfoGoogleAccount", condition);
            const data = await sendShippingApi.json();
            
            dispatch({
                type: ON_SEND_SHIPPING_ADDRESS_SUCCESS,
                data: data
            })
            dispatch({
                type: UPDATE_PAYMENT_FOR_GOOGLE_ACCOUNT
            })
        }catch(error){
            dispatch({
                type: ON_SEND_SHIPPING_ADDRESS_ERROR,
                error: error
            })
        }   
    }
}

export const validateShipping = () =>{
    return {
        type: VALIDATE_SHIPPING_ADDRESS
    }
}