import { 
    OPEN_SIDE_BAR,
    ClOSE_SIDE_BAR
} from "../constant/constant";

export const openSideBar = () => {
    return{
        type: OPEN_SIDE_BAR
    }
}
export const closeSideBar = () => {
    return{
        type: ClOSE_SIDE_BAR
    }
}