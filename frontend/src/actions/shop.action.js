import {
    CHANGE_PRODUCT_NUMBER,
    ON_CHANGE_FILTER_NAME,
    ON_CHANGE_FILTER_MIN_PRICE,
    ON_CHANGE_FILTER_MAX_PRICE,
    ON_CHANGE_FILTER_TYPE_SOFA,
    ON_CHANGE_FILTER_TYPE_DESK,
    ON_CHANGE_FILTER_TYPE_CHAIR,
    ON_CHANGE_FILTER_TYPE_BOOKSHELF,
    ON_CHANGE_FILTER_TYPE_TVSTAND,
    ON_CHANGE_FILTER_TYPE_BED,
    ON_CHANGE_FILTER_TYPE_DECORITEM,
    ON_CHANGE_CURRENT_PAGE_NUMBER,
    ON_CHANGE_COUPON_CODE,
    ON_CHECK_COUPON_PENDING, 
    ON_CHECK_COUPON_SUCCESS,
    ON_CHECK_COUPON_ERROR
} from "../constant/shop.constant";

export const onChangeCoupon = (coupon) =>{
    return{
        type: ON_CHANGE_COUPON_CODE,
        value: coupon
    }
}

export const changeProductNumberInShop = (limit) => {
    return{
        type: CHANGE_PRODUCT_NUMBER,
        value: limit
    }
}



export const onChangeFilterName = (name) => {
    return{
        type: ON_CHANGE_FILTER_NAME,
        value: name
    }
}
export const onChangeFilterMaxPrice = (price) => {
    return{
        type: ON_CHANGE_FILTER_MAX_PRICE,
        value: price
    }
}

export const  onChangeFilterMinPrice = (price) => {
    return{
        type: ON_CHANGE_FILTER_MIN_PRICE,
        value: price
    }
}

export const onChangeFilterTypeSofa = (sofa) =>{
    return{
        type: ON_CHANGE_FILTER_TYPE_SOFA,
        value: sofa
    }
}

export const onChangeFilterTypeDesk = (Desk) => {
    return{
        type: ON_CHANGE_FILTER_TYPE_DESK,
        value: Desk
    }
}

export const  onChangeFilterTypeChair = (Chair) => {
    return{
        type: ON_CHANGE_FILTER_TYPE_CHAIR,
        value: Chair
    }
}

export const onChangeFilterTypeBookShelf = (Bookshelf) => {
    return{
        type: ON_CHANGE_FILTER_TYPE_BOOKSHELF,
        value: Bookshelf
    }
}

export const onChangeFilterTypeTvStand = (TvStand) => {
    return{
        type: ON_CHANGE_FILTER_TYPE_TVSTAND,
        value: TvStand
    }
}

export const onChangeFilterTypeBed = (Bed) => {
    return{
        type: ON_CHANGE_FILTER_TYPE_BED,
        value: Bed
    }
}

export const  onChangeFilterTypeDecorItem = (DecorItem) => {
    return{
        type: ON_CHANGE_FILTER_TYPE_DECORITEM,
        value: DecorItem
    }
}

export const onChangeCurrentPage = (pageNumber) => {
    return{
        type: ON_CHANGE_CURRENT_PAGE_NUMBER,
        value: pageNumber
    }
}

//check Coupon 
export const  checkCoupon = (voucher) => {
    return async (dispatch) => {
        try{
            dispatch({
                type: ON_CHECK_COUPON_PENDING
            })
            const checkCouponCondition = {
                method: "GET",
            }
            const checkCoupon = await  fetch ("http://localhost:8000/getVoucherbyCode/" + voucher, checkCouponCondition);

            const data = await checkCoupon.json();
            dispatch({
                type: ON_CHECK_COUPON_SUCCESS,
                data: data.data
            })
        }catch(error){
            dispatch({
                type: ON_CHECK_COUPON_ERROR,
                error: error
            })
        }
    }
}
