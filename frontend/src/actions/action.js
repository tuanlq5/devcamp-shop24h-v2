import {
  SEND_REQUEST_TAKE_PRODUCT_PENDING,
  SEND_REQUEST_TAKE_PRODUCT_START,
  SEND_REQUEST_TAKE_PRODUCT_END,
  SIGN_OUT_GOOGLE_ACCOUNT,
  SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS,
  ON_CHANGE_FULLNAME_REGISTER,
  ON_CHANGE_PHONE_REGISTER,
  ON_CHANGE_EMAIL_REGISTER,
  ON_CHANGE_PASSWORD_REGISTER,
  ON_CHANGE_ADDRESS_REGISTER,
  ON_CHANGE_CITY_REGISTER,
  ON_CHANGE_COUNTRY_REGISTER,
  ON_CHANGE_POLICY_REGISTER,
  ON_CHANGE_ADMINISTRATOR_REGISTER,
  ON_CHANGE_ADMIN_REGISTER,
  REGISTER_CUSTOMER_ACCOUNT_PENDING,
  REGISTER_CUSTOMER_ACCOUNT_SUCCESS,
  REGISTER_CUSTOMER_ACCOUNT_ERROR,
  ON_CHANGE_EMAIL_LOGIN,
  ON_CHANGE_PASSWORD_LOGIN,
  ON_LOGIN_CUSTOMER_PENDING,
  ON_LOGIN_CUSTOMER_SUCCESS,
  ON_LOGIN_CUSTOMER_ERROR,
  ON_REFRESH_CUSTOMER_ACCOUNT_PENDING,
  ON_REFRESH_CUSTOMER_ACCOUNT_SUCCESS,
  ON_REFRESH_CUSTOMER_ACCOUNT_ERROR,
  ON_LOGOUT_CUSTOMER_ACCOUNT_PENDING,
  ON_LOGOUT_CUSTOMER_ACCOUNT_SUCCESS,
  ON_LOGOUT_CUSTOMER_ACCOUNT_ERROR,
  SET_LOADING_CUSTOMER,
  ON_REGISTER_ADMIN_ACCOUNT_PENDING,
  ON_REGISTER_ADMIN_ACCOUNT_SUCCESS,
  ON_REGISTER_ADMIN_ACCOUNT_ERROR
} from "../constant/constant";

import { ON_CLICK_FILTER_PRODUCT } from "../constant/shop.constant";

export const setLoadingCustomer = (loading) => {
  return {
    type: SET_LOADING_CUSTOMER,
    payload: loading,
  };
};

//Lấy dữ liệu Product khi tải trang Shop
export const fetchDataProduct = (
  limit,
  currentPage,
  filterName,
  filterMinValue,
  filterMaxValue,
  filterProductType
) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: SEND_REQUEST_TAKE_PRODUCT_PENDING,
      });
      var requestOption = {
        method: "GET",
      };
      /* Product Type Api call*/
      const getProductType = await fetch(
        "http://localhost:8000/productType",
        requestOption
      );

      const dataProductType = await getProductType.json();
      /* Product Api call*/

      //chuyển productType Id sang query
      const filterTypeUrlArray = filterProductType.map((element, index) => {
        return `&type=${element}`;
      });

      let filterURL = "http://localhost:8000/getProductFilter";

      let query = "?";

      //Xét điều kiện nếu các trường filter được nhập
      if (filterName !== "") {
        query += `name=${filterName}&`;
      }

      //xét điều kiện nếu trường filter giá min mã được nhập
      if (filterMinValue !== 0 && filterMaxValue !== 0) {
        query += `minPrice=${filterMinValue}&maxPrice=${filterMaxValue}&`;
      }

      //Xét điều kiện nếu product type được nhập
      if (JSON.stringify(filterProductType) !== JSON.stringify([])) {
        query += `type=${filterProductType[0]}`;

        for (let i = 1; i < filterTypeUrlArray.length; i++) {
          query += `${filterTypeUrlArray[i]}`;
        }
      }

      let newURL = filterURL + query; //UrL filter product sau khi thêm query filter

      const getAllProductFilter = await fetch(newURL, requestOption); //request lấy dữ liệu theo filter

      const dataProductFilter = await getAllProductFilter.json(); //data product sau khi filter

      let numberOfPage = Math.ceil(dataProductFilter.data.length / limit); //Số trang trên panigation

      let skipPage = currentPage - 1; //số trang skip khi ấn vào panigation

      /* Panigation Api Call */

      let panigationURL = "http://localhost:8000/getProductWithpanigation";

      let queryPanigationURL = "?";

      //Xét điều kiện nếu các trường filter được nhập
      if (filterName !== "") {
        queryPanigationURL += `name=${filterName}&`;
      }

      //xét điều kiện nếu trường filter giá min mã được nhập
      if (filterMinValue !== 0 && filterMaxValue !== 0) {
        queryPanigationURL += `minPrice=${filterMinValue}&maxPrice=${filterMaxValue}&`;
      }

      //Xét điều kiện nếu product type được nhập
      if (JSON.stringify(filterProductType) !== JSON.stringify([])) {
        queryPanigationURL += `type=${filterProductType[0]}`;

        for (let i = 1; i < filterTypeUrlArray.length; i++) {
          queryPanigationURL += `${filterTypeUrlArray[i]}`;
        }
      }

      //Thêm query URL
      let newUrlPanigation =
        panigationURL +
        queryPanigationURL +
        "&skipPage=" +
        skipPage +
        "&limitProduct=" +
        limit;

      const responseURL = await fetch(newUrlPanigation, requestOption);

      const productPage = await responseURL.json();

      await dispatch({
        type: SEND_REQUEST_TAKE_PRODUCT_START,
        page: numberOfPage,
        data: productPage,
        productType: dataProductType,
      });
    } catch (error) {
      return {
        type: SEND_REQUEST_TAKE_PRODUCT_END,
        error: error,
      };
    }
  };
};

export const signInGoogleAccountSuccessful = (data) => {
  return {
    type: SEND_AUTHENTICATION_TO_GOOGLE_SUCCESS,
    payload: data,
  };
};

export const signOutGoogleAccount = () => {
  return {
    type: SIGN_OUT_GOOGLE_ACCOUNT,
  };
};

export const registerNewCustomer = (customer) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: REGISTER_CUSTOMER_ACCOUNT_PENDING,
      });
      const requestCondition = {
        method: "POST",
        headers: {
          "Content-type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(customer),
      };
      const sendNewCustomer = await fetch(
        "http://localhost:8000/customer",
        requestCondition
      );
      if(sendNewCustomer.status === 500){
        return dispatch({
          type: REGISTER_CUSTOMER_ACCOUNT_ERROR,
        });
      }
      return dispatch({
        type: REGISTER_CUSTOMER_ACCOUNT_SUCCESS,
      });

    } catch (error) {
      return dispatch({
        type: REGISTER_CUSTOMER_ACCOUNT_ERROR,
        error: error.message,
      });
    }
  };
};

export const onClickFilterProduct = () => {
  return {
    type: ON_CLICK_FILTER_PRODUCT,
  };
};

export const onChangeFullname = (value) => {
  return {
    type: ON_CHANGE_FULLNAME_REGISTER,
    payload: value,
  };
};

export const onChangeAddress = (value) => {
  return {
    type: ON_CHANGE_ADDRESS_REGISTER,
    payload: value,
  };
};

export const onChangePhone = (value) => {
  return {
    type: ON_CHANGE_PHONE_REGISTER,
    payload: value,
  };
};

export const onChangeEmail = (value) => {
  return {
    type: ON_CHANGE_EMAIL_REGISTER,
    payload: value,
  };
};

export const onChangePassword = (value) => {
  return {
    type: ON_CHANGE_PASSWORD_REGISTER,
    payload: value,
  };
};

export const onChangeCity = (value) => {
  return {
    type: ON_CHANGE_CITY_REGISTER,
    payload: value,
  };
};

export const onChangeCountry = (value) => {
  return {
    type: ON_CHANGE_COUNTRY_REGISTER,
    payload: value,
  };
};

export const onChangeAdministrativeDivision = (value) => {
  return{
    type: ON_CHANGE_ADMINISTRATOR_REGISTER,
    payload: value
  }
}

export const onChangeAdminCode = (value) => {
  return{
    type: ON_CHANGE_ADMIN_REGISTER,
    payload: value
  }
}

export const onChangePolicy = (value) => {
  return {
    type: ON_CHANGE_POLICY_REGISTER,
    payload: value,
  };
};

export const onChangeEmailLogin = (value) => {
  return {
    type: ON_CHANGE_EMAIL_LOGIN,
    payload: value,
  };
};

export const onChangePasswordLogIn = (value) => {
  return {
    type: ON_CHANGE_PASSWORD_LOGIN,
    payload: value,
  };
};

//Login User Account
export const onClickLogInCustomer = (value) => {
  return async (dispatch) => {
    try {
      await dispatch({
        type: ON_LOGIN_CUSTOMER_PENDING,
      });

      const requestCondition = {
        method: "POST",
        headers: {
          "Content-type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(value),
        mode: "cors",
        credentials: "include",
      };
      //lấy access token
      const sendLoginCustomerRequest = await fetch(
        "http://localhost:8000/customerLogin",
        requestCondition
      );

      const accessToken = await sendLoginCustomerRequest.json();
    
      const requestWithBearToken = {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=utf-8",
          "Authorization": `Bearer ${accessToken.accessToken}`,
        },
       
      };

      //lấy dữ liệu khi nhận được access token
      const takeTheDataFromDB = await fetch(
        "http://localhost:8000/findCustomerEmail/" + value.email,
        requestWithBearToken
      );

      const customerData = await takeTheDataFromDB.json();
      //refresh
      dispatch({
        type: ON_LOGIN_CUSTOMER_SUCCESS,
        data: customerData.data[0],
      });
    } catch (error) {
  
      dispatch({
        type: ON_LOGIN_CUSTOMER_ERROR,
        error: error,
      });
    }
  };
};

// Refresh Customer Account
export const refreshCustomerLogin = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: ON_REFRESH_CUSTOMER_ACCOUNT_PENDING,
      });
      dispatch(setLoadingCustomer(true));
      const requestOption = {
        method: "POST",
        credentials: "include",
      };
      //take the new access token with refresh token in the cookies
      const takeAccesstoken = await fetch(
        "http://localhost:8000/refresh",
        requestOption
      );

      const data = await takeAccesstoken.json();

      //new request option for the user profile
      const requestOptionCustomer = {
        method: "GET",
        headers: {
          "Content-type": "application/json; charset=utf-8",
          Authorization: `Bearer ${data.accessToken}`,
        },
      };
      //send request take user data
      const takeCustomerData = await fetch(
        "http://localhost:8000/findCustomerEmail/" + data.email,
        requestOptionCustomer
      );

      const customerData = await takeCustomerData.json();
      dispatch({
        type: ON_REFRESH_CUSTOMER_ACCOUNT_SUCCESS,
        data: customerData.data[0],
      });
    } catch (error) {
      dispatch({
        type: ON_REFRESH_CUSTOMER_ACCOUNT_ERROR,
        error: error,
      });
    } finally {
      setLoadingCustomer(false);
    }
  };
};

export const logOutCustomerAccount = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: ON_LOGOUT_CUSTOMER_ACCOUNT_PENDING,
      });
      const requestOption = {
        method: "GET",
        credentials: "include",
      };

      //call api lout out user
      await fetch("http://localhost:8000/customerLogout", requestOption);

      dispatch({
        type: ON_LOGOUT_CUSTOMER_ACCOUNT_SUCCESS,
        message: "Logout successful!",
      });
    } catch (error) {
      dispatch({
        type: ON_LOGOUT_CUSTOMER_ACCOUNT_ERROR,
        error: error,
      });
    }
  };
};

export const registerNewAdmin = (adminAccount)  => {
  return async(dispatch)  =>{
    try{
      await dispatch({
        type: ON_REGISTER_ADMIN_ACCOUNT_PENDING
      })

      const condition = {
        method: "POST",
        headers:{
          "Content-type": " application/json; charset=utf-8"
        },
        body: JSON.stringify(adminAccount)
      }

      const sendAdminAccount = await fetch("http://localhost:8000/AdminRegister", condition);

      const  data = await sendAdminAccount.json();

      if(!sendAdminAccount.ok){
        if (sendAdminAccount.status === 500) {
          return dispatch({
            type: ON_REGISTER_ADMIN_ACCOUNT_ERROR,
            error: sendAdminAccount.status
          })
        } else {
          return dispatch({
            type: ON_REGISTER_ADMIN_ACCOUNT_ERROR,
            error: sendAdminAccount.status
          })
        }
      }

    
      await dispatch({
            type: ON_REGISTER_ADMIN_ACCOUNT_SUCCESS,
            data: data
        })
    }catch(error){
      return dispatch({
        type: ON_REGISTER_ADMIN_ACCOUNT_ERROR,
        error: error.message
      })
    }
  }
}