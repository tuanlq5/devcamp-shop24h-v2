import { 
    LOAD_PRODUCT_LIST_PENDING,
    LOAD_PRODUCT_LIST_SUCCESS,
    LOAD_PRODUCT_LIST_ERROR,
    
} from "../../constant/admin/admin.constant";
import { 
    EDIT_PRODUCT_ERROR , 
    EDIT_PRODUCT_PENDING,
    EDIT_PRODUCT_SUCCESS,
    SET_BACK_EDIT_STATUS,
    DELETE_PRODUCT_ERROR,
    DELETE_PRODUCT_SUCCESS,
    DELETE_PRODUCT_PENDING,
    SET_BACK_DELETE_STATUS,
    
} from "../../constant/admin/admin.ProductConstant";


export const setBackEditProductStatus = () => {
    return{
        type: SET_BACK_EDIT_STATUS
    }
}

export const setBackDeleteProductStatus = () => {
    return{
        type: SET_BACK_DELETE_STATUS
    }
}

export const onStartDeleteProduct = (productId) => {
    return async(dispatch) =>{
        try{
            dispatch({
                type: DELETE_PRODUCT_PENDING
            })
            const refreshCondition = {
                method: "POST",
                credentials: "include"
            }

            const refreshUser = await fetch("http://localhost:8000/refresh", refreshCondition);

            const accessToken = await refreshUser.json();

            const condition = {
                method:"PUT",
                headers:{
                    "Content-type": "Application/json; charset=utf-8",
                    Authorization : `Bearer ${accessToken.accessToken}`
                }, 
            }

            const softDeleteProduct = await fetch("http://localhost:8000/softDeleteProduct/" + productId, condition);

            await softDeleteProduct.json();
            return dispatch({
                type: DELETE_PRODUCT_SUCCESS
            })
        }catch(error){
            return dispatch({
                type: DELETE_PRODUCT_ERROR,
                error: error
            })
        }
    }
}

//Edit product action
export const onStartEditProduct = (newProduct, productId, imageReplace, imageIndex, imageFile, productData) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: EDIT_PRODUCT_PENDING
            })

            // Sending new product picture to cloudinary 
            const formData = new FormData();
            
            let imageDataJson = null;
            // console.log(JSON.stringify(imageFile) )
            if(JSON.stringify(imageFile) !== JSON.stringify([]))
            {
                formData.append('image', imageFile[0]);

                const sendImageCondition = {
                    method: "POST",
                    body: formData,
                }
                // update new product image to cloudinary
                const sendImageToCloudinary = await fetch("http://localhost:8000/postImageToCloudinary ", sendImageCondition)
                
                imageDataJson = await sendImageToCloudinary.json();
            
                
                // Retrieve the image link
                const imageLink = imageDataJson.data;
                
                // update the new image link in product 
                
                productData.imageURL[imageIndex] = imageLink
                newProduct.imageURL = productData.imageURL;
      
                if(imageReplace !== null){
                    // Delete the previousimage in cloudinary 
                    // Create image id for api
                    const lastIndex = imageReplace.lastIndexOf('/');
                    const secondLastIndex = imageReplace.lastIndexOf('/', lastIndex - 1);
                    const endIndex = imageReplace.lastIndexOf('.');
                    const segment = imageReplace.substring(secondLastIndex + 1, endIndex);
                    const imageId = {
                        id: segment
                    }
                    
                    const deleteCloudinaryImageCondition = {
                        method:"POST",
                        headers:{
                            "Content-type": "Application/json; charset=utf-8",
                        },
                        body: JSON.stringify(imageId)
                    }
                    // Delete the old product image
                    const deleteCloudinaryImage =  await fetch("http://localhost:8000/deleteCloudinaryImage", deleteCloudinaryImageCondition);

                    await deleteCloudinaryImage.json();
                }
            }
            // send refreshtoken for access key
            const  refreshCondition = {
                method: "POST",
                credentials: "include"
            }
            const refreshUser = await fetch("http://localhost:8000/refresh", refreshCondition);

            const accessToken = await refreshUser.json();

            const condition = {
                method:"PUT",
                headers:{
                    "Content-type": "Application/json; charset=utf-8",
                    Authorization : `Bearer ${accessToken.accessToken}`
                },
                body: JSON.stringify(newProduct)
            }

            // edit the product
            const editProduct = await fetch("http://localhost:8000/product/" + productId , condition);
            
            await editProduct.json();

            return dispatch({
                type: EDIT_PRODUCT_SUCCESS
            })
        }catch(error){
            // console.log(error);
            dispatch({
                type: EDIT_PRODUCT_ERROR,
                error: error
            })
        }
    }
}

// Load All product Data
export const loadProductData = (limit , skip) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: LOAD_PRODUCT_LIST_PENDING
            })
            
            const conditon = {
                method:"GET"
            }
            // eslint-disable-next-line no-useless-concat
            const getProduct = await fetch("http://localhost:8000/getProductWithpanigation" + `?limitProduct=${limit}&skipPage=${skip}`, conditon);
            const data = await getProduct.json();

            dispatch({
                type: LOAD_PRODUCT_LIST_SUCCESS,
                data: data.data,
                length: data.length
            })
        }catch(error){
            dispatch({
                type: LOAD_PRODUCT_LIST_ERROR,
                error: error
            })
        }
    }
}

