import { 
    LOAD_ORDER_LIST_PENDING,
    LOAD_ORDER_LIST_SUCCESS,
    LOAD_ORDER_LIST_ERROR,
    ON_CHANGE_STATUS_ORDER,
    ON_EDIT_ORDER_STATUS_PENDING,
    ON_EDIT_ORDER_STATUS_SUCCESS,
    ON_EDIT_ORDER_STATUS_ERROR
} from "../../constant/admin/admin.constant";


export const loadOrderList = (limit, skip) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: LOAD_ORDER_LIST_PENDING
            })
            const requestOption = {
                method: "POST",
                credentials: "include"
            }
            //take the new access token with refresh token in the cookies 
            const takeAccesstoken = await fetch("http://localhost:8000/refresh" , requestOption);

            const accessToken = await takeAccesstoken.json();

            const condition = {
                method: "GET", 
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accessToken.accessToken
                },
                credentials: "include",
            };

            // eslint-disable-next-line no-useless-concat
            const loadOrder = await fetch("http://localhost:8000/order" + `?limit=${limit}&skip=${skip}`,condition );

            const data = await loadOrder.json();

            dispatch({
                type: LOAD_ORDER_LIST_SUCCESS,
                data: data.data,
                length: data.length
            })
        }catch(error){
            dispatch({
                type: LOAD_ORDER_LIST_ERROR,
                error: error
            })
        }
    }
}

export const onChangeStatusOrder = (status, orderObject) => {
    return{

        type: ON_CHANGE_STATUS_ORDER,
        status: status,
        orderObject: orderObject
    }
}

export const editOrderStatus = (orderObject) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: ON_EDIT_ORDER_STATUS_PENDING
            })
            
            const condition= {
                method: "PUT",
                headers:{
                    "Content-type":"application/json; charset=utf-8"
                },
                body: JSON.stringify({
                    status: orderObject.status,
                    orderId: orderObject._id    
                })
            };
            console.log(condition);
            await fetch("http://localhost:8000/orderStatus", condition);
            dispatch({
                type: ON_EDIT_ORDER_STATUS_SUCCESS
            })
        }catch(error){
            dispatch({
                type: ON_EDIT_ORDER_STATUS_ERROR
            })
        }
    }
}