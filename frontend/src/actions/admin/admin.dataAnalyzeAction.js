import { 
    MONTHLY_ORDER,
    MONTHLY_REVENUE,
    MONTHLY_NEW_CUSTOMER,
    SEND_REQUEST_ALL_ORDER_PENDING,
    SEND_REQUEST_ALL_ORDER_SUCCESS,
    SEND_REQUEST_ALL_ORDER_ERROR,
    SEND_REQUEST_DONUT_CHART_DATA_PENDING,
    SEND_REQUEST_DONUT_CHART_DATA_SUCCESS,
    SEND_REQUEST_DONUT_CHART_DATA_ERROR
 } from "../../constant/admin/admin.DataAnalyzeConstant";


export const newMonthlyRevenue = (orderData) => {
    return{
        type: MONTHLY_REVENUE,
        data: orderData
    }
}

export const newMonthlyOrder = (orderData) => {
    return{
        type: MONTHLY_ORDER,
        data: orderData
    }
}

export const newMonthlyCustomer = (orderData) => {
    return{
        type: MONTHLY_NEW_CUSTOMER,
        data: orderData
    }
}

// function define month by number
function getMonthName(monthNumber) {
    const date = new Date();
    date.setMonth(monthNumber - 1);
  
    return date.toLocaleString('en-US', {
      month: 'long',
    });
}

export const sendRequestAllOrder = () => {
    return async(dispatch) =>{
        try {
            dispatch({
                type: SEND_REQUEST_ALL_ORDER_PENDING
            })
            const requestOption = {
                method: "POST",
                credentials: "include"
                }
            //take the new access token with refresh token in the cookies 
            const takeAccesstoken = await fetch("http://localhost:8000/refresh" , requestOption);
        
            const accessToken = await takeAccesstoken.json();
        
            const condition = {
                method: "GET", 
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accessToken.accessToken
                },
                credentials: "include",
            };
            // Load all order
            const loadOrder =  await fetch("http://localhost:8000/order", condition);
            const data = await loadOrder.json();
            // confirmed order
            const data2 = [];
            for(let i = 0 ; i<  data.data.length; i++){
                if(data.data[i].status === "Confirmed"){
                    data2.push(data.data[i])
                }
            }
            
            // Load all user Activities
            const loadUserActivities = await fetch("http://localhost:8000/getAllUserActivities", condition);
            const userActivities = await loadUserActivities.json();
            
            // find the order data in June
            const dataInJune = [];
            const acceptedOrder = [];
            for(let i = 0 ;  i< data.data.length; i++){
                if(data.data[i].orderDate.split("-")[1] === "04"){
                    dataInJune.push(data.data[i])
                }
            }
            // find confirmed order
            for(let i = 0 ; i<  dataInJune.length; i++){
                if(dataInJune[i].status === "Confirmed"){
                    acceptedOrder.push(dataInJune[i])
                }
            }
            // calculate the monthly sale 
            const monthlySale = Math.round((acceptedOrder.reduce((partical, a) => partical + a.cost, 0))*100)/100 ;
            
            // calculate the comparison percentage of sale between last month and this month
            const dataInLastMonth = [];
            const confirmOrderInLastMonth = [];
            for(let i = 0 ;  i< data.data.length; i++){
                if(data.data[i].orderDate.split("-")[1] === "06"){
                dataInLastMonth.push(data.data[i])
                }
            }
            // find confirmed order
            for(let i = 0 ; i<  dataInLastMonth.length; i++){
                if(dataInLastMonth[i].status === "Confirmed"){
                    confirmOrderInLastMonth.push(dataInLastMonth[i])
                }
            }
            const lastMonthSale = Math.round((confirmOrderInLastMonth.reduce((partical,a) => partical + a.cost, 0))*100)/100;
            const compareLastMonthSalePercentage = Math.round(((monthlySale - lastMonthSale)/lastMonthSale * 100)*100)/100;

            //  calculate he comparison percentage of order between last month and this month
            const comparisonLastMonthOrderPercentage = Math.round(((acceptedOrder.length - confirmOrderInLastMonth.length)/confirmOrderInLastMonth.length * 100)*100)/100;
            
            // create list of add product
            const addProduct = [];
            for(let i = 0 ; i < userActivities.length; i++){
               for(let j = 0 ; j< userActivities[i].cartActivities.addProductToCart.length; j++){
                    addProduct.push(userActivities[i].cartActivities.addProductToCart[j])
               }
            }

            // Create data add product base on month
            const addProductSumary = [];
            for(let i = 0; i < addProduct.length; i++){
                addProductSumary.push(getMonthName(
                    addProduct[i]
                    .createdAt
                    .split("-")[1]
                ))
            }
            
            // format add product data so it can put on line chart
            // find the duplicate month for ratio 
            const addproductCount = {};

            addProductSumary.forEach(element => {
                addproductCount[element] = (addproductCount[element] || 0) + 1;
            });

            // the data fot chart which using Object.key and values to separate a new array
            const addProductMonth = Object.keys(addproductCount);
            const addProductDataInMonth = Object.values(addproductCount);

            // create data for combo table
            // create list of month 
            
            // create list of order
            const orderSuccessSumary = [];
            for(let i = 0; i < data2.length; i++){
                orderSuccessSumary.push(getMonthName(
                    data2[i]
                    .createdAt
                    .split("-")[1]
                ))
            }
            let orderSuccessData = []
            orderSuccessSumary.forEach(element => {
                orderSuccessData[element] = (orderSuccessData[element] || 0) + 1;
            });

            const orderSuccessTime = Object.keys(orderSuccessData);
            const orderSuccess = Object.values(orderSuccessData);
            // create list of refund or cancel order
            const data3 = []; //order cancel
            for(let i = 0 ; i<  data.data.length; i++){
                if(data.data[i].status === "Canceled"){
                    data3.push(data.data[i])
                }
            }

            const refundOrderSumary = []; //find the order refund base on month
            for(let i = 0; i < data3.length; i++){
                refundOrderSumary.push(getMonthName(
                    data3[i]
                    .createdAt
                    .split("-")[1]
                ))
            }
            let orderFailData = []
            // format the order to object contain 2 keys: month and number of order
            refundOrderSumary.forEach(element => {
                orderFailData[element] = (orderFailData[element] || 0) + 1;
            });
            const orderFail = Object.values(orderFailData);
            

            dispatch({
                type: SEND_REQUEST_ALL_ORDER_SUCCESS,
                data: data2,
                monthlySale: monthlySale,
                orderLength: acceptedOrder.length,
                compareToLastMonthSale: compareLastMonthSalePercentage,
                compareToLastMonthOrder:  comparisonLastMonthOrderPercentage,
                addProductMonth: addProductMonth,
                addProductDataInMonth: addProductDataInMonth,
                userActivities: userActivities,
                orderCancel: data3.length,
                orderTime: orderSuccessTime,
                acceptedOrder: orderSuccess,
                refundOrder: orderFail
            })
            } 
        catch (error) {
             dispatch({
                type: SEND_REQUEST_ALL_ORDER_ERROR,
                error: error
             })
        }
    }
}

export const sendRequestComboChartData = () => {
    return async(dispatch) => {
        try {
            dispatch({
                type: SEND_REQUEST_DONUT_CHART_DATA_PENDING
            })
            const requestOption = {
                method:"POST",
                credentials: "include"
            }
            const fetchBearToken = await fetch("http://localhost:8000/refresh", requestOption);

            const accesstoken = await fetchBearToken.json();

            const requestOptionForComboChart = {
                method: "GET",
                headers:{
                    "Content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accesstoken.accessToken
                },
                credentials: "include",
            }
            
            const requestDonutChartData = await fetch("http://localhost:8000/getCategoryRatioInYear", requestOptionForComboChart);

            const donutChartData = await requestDonutChartData.json();

            dispatch({
                type: SEND_REQUEST_DONUT_CHART_DATA_SUCCESS,
                donutChartData: donutChartData
            })
        } catch (error) {
            // console.log(error)
            dispatch({
                type: SEND_REQUEST_DONUT_CHART_DATA_ERROR,
                error: error
            })
        }
    }
}