import { 
    LOAD_CUSTOMER_LIST_PENDING,
    LOAD_CUSTOMER_LIST_SUCCESS,
    LOAD_CUSTOMER_LIST_ERROR  
} from "../../constant/admin/admin.constant";

import {
    ON_CHANGE_CUSTOMER_FULL_NAME, 
    UPDATE_CUSTOMER_PENDING,
    UPDATE_CUSTOMER_SUCCESS,
    UPDATE_CUSTOMER_ERROR,
    DELETE_CUSTOMER_ERROR,
    DELETE_CUSTOMER_SUCCESS,
    DELETE_CUSTOMER_PENDING
} from "../../constant/admin/admin.CustomerConstant";

export const onChangeCustomerFullName = (value) => {
    return{
        type: ON_CHANGE_CUSTOMER_FULL_NAME,
        data: value
    }
}

export const  deleteCustomerById = (customerId) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: DELETE_CUSTOMER_PENDING
            })

            const conditionRefresh ={
                method: "POST",
                credentials:"include"
            }

            const takeAccessToken = await fetch("http://localhost:8000/refresh", conditionRefresh);

            const accessToken =  await takeAccessToken.json();

            const condition = {
                method: "PUT",
                headers: {
                    "authorization": `Bearer ${accessToken.accessToken}`
                },
            }
            await fetch("http://localhost:8000/softDeleteCustomer/" + customerId, condition);

            return dispatch({
                type: DELETE_CUSTOMER_SUCCESS
            })
        }catch(error){
            return dispatch({
                type: DELETE_CUSTOMER_ERROR,
                error: error
            })
        }
    }
}

export const updateCustomerData = (newCustomerData ,customerId) => {
    return async(dispatch) =>{
      try{  
        await dispatch({
            type: UPDATE_CUSTOMER_PENDING
        })

        const conditionRefreshUser = {
            method: "POST",
            credentials: "include"
        }

        const sendRefreshToken = await fetch("http://localhost:8000/refresh", conditionRefreshUser);

        const accessToken = await sendRefreshToken.json()

        const condition = {
            method: "PUT",
            headers: {
                "Content-type": "application/json; charset=utf-8",
                "Authorization": `Bearer ${accessToken.accessToken}`
            },
            body: JSON.stringify(newCustomerData)
        }

        const editCustomer = await fetch("http://localhost:8000/customer/" + customerId, condition);
        
        await editCustomer.json();
        
        return dispatch({
            type: UPDATE_CUSTOMER_SUCCESS
        })
    }catch(error){
        return dispatch({
            type: UPDATE_CUSTOMER_ERROR,
            error: error
        })
    }
    }
}

export const loadCustomerData = (limit, skip) => {
    return async(dispatch)=> {
        try{
            dispatch({
                type: LOAD_CUSTOMER_LIST_PENDING
            })
           
            const requestOption = {
                method: "POST",
                credentials: "include"
            }
            //take the new access token with refresh token in the cookies 
            const takeAccesstoken = await fetch("http://localhost:8000/refresh" , requestOption);

            const accessToken = await takeAccesstoken.json();

            const condition ={
                method:"GET",
                headers: {
                    "Content-type": 'application/json; charset=utf-8',
                    "Authorization": `Bearer ${accessToken.accessToken}`
                }
            }
            const getCustomerData = await fetch(`http://localhost:8000/getCustomerWithPanigation?limit=${limit}&skip=${skip}`, condition);

            const data = await getCustomerData.json();

            dispatch({
                type: LOAD_CUSTOMER_LIST_SUCCESS,
                data: data.data,
                length: data.length,
            })
        }catch(error){
            dispatch({
                type: LOAD_CUSTOMER_LIST_ERROR,
                error: error
            })
        }
    }
}