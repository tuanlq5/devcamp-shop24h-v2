import { 
    LOAD_LIST_COUPON_PENDING ,
    LOAD_LIST_COUPON_SUCCESS, 
    LOAD_LIST_COUPON_ERROR,

}from "../../constant/admin/admin.constant";

import { 
    ON_DELETE_COUPON_PENDING,
    ON_DELETE_COUPON_SUCCESS,
    ON_DELETE_COUPON_ERROR,
    ON_EDIT_COUPON_PENDING,
    ON_EDIT_COUPON_SUCCESS,
    ON_EDIT_COUPON_ERROR,
    ON_UPDATE_EDIT_COUPON_STATUS,
    ON_UPDATE_DELETE_COUPON_STATUS 
} from "../../constant/admin/admin.CouponConstant";

export const updateEditCouponStatus = () =>{
    return{
        type: ON_UPDATE_EDIT_COUPON_STATUS
    }
}

export const updateDeleteCouponStatus = () => {
    return{
        type: ON_UPDATE_DELETE_COUPON_STATUS
    }
}

export const onEditCouponData = (couponData, couponId) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: ON_EDIT_COUPON_PENDING
            })

            const requestOption = {
                method: "POST",
                credentials: "include"
            }
            //take the new access token with refresh token in the cookies 
            const takeAccesstoken = await fetch("http://localhost:8000/refresh" , requestOption);

            const accessToken = await takeAccesstoken.json();

            const condition = {
                method: "PUT", 
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accessToken.accessToken
                },
                body: JSON.stringify(couponData)
            };
            await fetch("http://localhost:8000/voucher/" + couponId  , condition);

            dispatch({
                type: ON_EDIT_COUPON_SUCCESS,
            })
        }catch(error){
            dispatch({
                type: ON_EDIT_COUPON_ERROR,
                error: error
            })
        }
    }
}

export const onDeleteCouponData = (couponId) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: ON_DELETE_COUPON_PENDING
            })

            const requestOption = {
                method: "POST",
                credentials: "include"
            }
            //take the new access token with refresh token in the cookies 
            const takeAccesstoken = await fetch("http://localhost:8000/refresh" , requestOption);

            const accessToken = await takeAccesstoken.json();

            const condition = {
                method: "PUT", 
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accessToken.accessToken
                },
            };
            await fetch("http://localhost:8000/softDeleteVoucher/" + couponId  , condition);


            dispatch({
                type: ON_DELETE_COUPON_SUCCESS,
            })
        }catch(error){
            dispatch({
                type: ON_DELETE_COUPON_ERROR,
                error: error
            })
        }
    }
}
export const loadVoucherData = (limit, skip) => {
    return async(dispatch) => {
        try{
            dispatch({
                type: LOAD_LIST_COUPON_PENDING
            })

            const requestOption = {
                method: "POST",
                credentials: "include"
            }
            //take the new access token with refresh token in the cookies 
            const takeAccesstoken = await fetch("http://localhost:8000/refresh" , requestOption);

            const accessToken = await takeAccesstoken.json();

            const condition = {
                method: "GET", 
                headers: {
                    "Content-type": "application/json; charset=utf-8",
                    "Authorization": "Bearer " + accessToken.accessToken
                },
                credentials: "include",
            };
            const loadCoupon = await fetch(`http://localhost:8000/getVoucherWithPanigation?limit=${limit}&skip=${skip}`, condition);

            const data = await loadCoupon.json();

            dispatch({
                type: LOAD_LIST_COUPON_SUCCESS,
                data: data.data,
                length: data.length
            })
        }catch(error){
            dispatch({
                type: LOAD_LIST_COUPON_ERROR,
                error: error
            })
        }
    }
}