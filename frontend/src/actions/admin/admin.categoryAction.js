import { 
    SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_ERROR,
    SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_SUCCESS,
    SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_PENDING,
    ON_REQUEST_EDIT_CATEGORY_PENDING,
    ON_REQUEST_EDIT_CATEGORY_SUCCESS,
    ON_REQUEST_EDIT_CATEGORY_ERROR,
    ON_LOAD_ALL_CATEGORY_PENDING,
    ON_LOAD_ALL_CATEGORY_SUCCESS, 
    ON_LOAD_ALL_CATEGORY_ERROR,
    ON_DELETE_CATEGORY_PENDING,
    ON_DELETE_CATEGORY_SUCCESS,
    ON_DELETE_CATEGORY_ERROR
} from "../../constant/admin/admin.CategoryConstant"

export const loadCategoryAdmin = (skip, limit) => {
    return async(dispatch) => {
        try{
            await dispatch({
                type: SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_PENDING
            })
            // access token
            const  refreshCondition = {
                method: "POST",
                credentials: "include"
            }
            const refreshUser = await fetch("http://localhost:8000/refresh", refreshCondition);

            const accessToken = await refreshUser.json();

            const condition = {
                method: "GET",
                headers:{
                    "Content-type": "Application/json; charset=utf-8",
                    Authorization : `Bearer ${accessToken.accessToken}`
                },
            }
            const loadCategory =  await fetch(`http://localhost:8000/productTypeWithPanigation?skipPage=${skip}&limit=${limit}`, condition);

            const data = await loadCategory.json();
                
            return dispatch({
                type: SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_SUCCESS,
                data: data.productType,
                productTypeLength: data.length
            })
        }catch(error){
            console.log(error);
            return dispatch({
                type: SEND_REQUEST_TAKE_ALL_CATEGORY_LIST_ERROR,
                error: error
            })
        }
    }
}

export const requestEditCategory = (categoryObject, imageFile, newProductType) => {
    return async(dispatch) => {
        try {
            dispatch({
                type: ON_REQUEST_EDIT_CATEGORY_PENDING
            })
            // Sending new product picture to cloudinary 
            const formData = new FormData();
            if(JSON.stringify(imageFile) !== JSON.stringify([])){

                // console.log(imageFile);
                formData.append('image', imageFile[0]);

                const sendImageCondition = {
                    method: "POST",
                    body: formData
                }

                // update new category avatar to cloudinary
                const sendImageToCloudinary = await fetch("http://localhost:8000/postImageToCloudinary", sendImageCondition)
                
                const imageDataJson = await sendImageToCloudinary.json();
                
                // Retrieve the image link
                const imageLink = imageDataJson.data;
                newProductType.avatar = imageLink;


                // Delete the previous avatar of category in cloudinary
                // create image id for api
                const lastIndex = categoryObject.avatar.lastIndexOf('/');
                const secondLastIndex = categoryObject.avatar.lastIndexOf('/', lastIndex - 1 );
                const endIndex = categoryObject.avatar.lastIndexOf('.');
                const segment = categoryObject.avatar.substring(secondLastIndex + 1, endIndex);
                const imageId = {
                    id: segment
                }
                // console.log(imageId);
                const deleteCloudinaryImageCondition = {
                    method:"POST",
                    headers:{
                        "Content-type": "Application/json; charset=utf-8",
                    },
                    body: JSON.stringify(imageId)
                }
                // Delete the old product image
                const deleteCloudinaryImage =  await fetch("http://localhost:8000/deleteCloudinaryImage", deleteCloudinaryImageCondition);

                await deleteCloudinaryImage.json();
            }
            // send refreshtoken for access key
            const  refreshCondition = {
                method: "POST",
                credentials: "include"
            }
            const refreshUser = await fetch("http://localhost:8000/refresh", refreshCondition);

            const accessToken = await refreshUser.json();

            // console.log(newProductType);
            const condition = {
                method:"PUT",
                headers:{
                    "Content-type": "Application/json; charset=utf-8",
                    Authorization : `Bearer ${accessToken.accessToken}`
                },
                body: JSON.stringify(newProductType)
            }
          
            // edit the product type
            const editProductType = await fetch("http://localhost:8000/productType/" + categoryObject._id , condition);
            
            await editProductType.json();
            
            dispatch({
                type: ON_REQUEST_EDIT_CATEGORY_SUCCESS
            })
        } catch (error) {
            console.log(error);
            dispatch({
                type: ON_REQUEST_EDIT_CATEGORY_ERROR
            })
        }
    }
}

export const loadAllCategory = () => {
    return async(dispatch) => {
        try {
            dispatch({
                type: ON_LOAD_ALL_CATEGORY_PENDING
            })
           // access token
           const  refreshCondition = {
            method: "POST",
                credentials: "include"
            }
            const refreshUser = await fetch("http://localhost:8000/refresh", refreshCondition);

            const accessToken = await refreshUser.json();

            const condition = {
                method: "GET",
                headers:{
                    "Content-type": "Application/json; charset=utf-8",
                    Authorization : `Bearer ${accessToken.accessToken}`
                },
            }
            const loadCategory =  await fetch(`http://localhost:8000/productType`, condition);

            const data = await loadCategory.json();
            dispatch({
                type: ON_LOAD_ALL_CATEGORY_SUCCESS,
                data:  data.data
            })
        } catch (error) {
            dispatch({
                type: ON_LOAD_ALL_CATEGORY_ERROR
            })
        }
    }
}

export const requestDeleteCategory = (categoryId) =>{
    return async(dispatch) => {
        try{
            dispatch({
                type: ON_DELETE_CATEGORY_PENDING
            })
            const  refreshCondition = {
                method: "POST",
                credentials: "include"
            }
            const refreshUser = await fetch("http://localhost:8000/refresh", refreshCondition);

            const accessToken = await refreshUser.json();

            const condition = {
                method:"PUT",
                headers:{
                    "Content-type": "Application/json; charset=utf-8",
                    Authorization : `Bearer ${accessToken.accessToken}`
                },
                
            }

            // Delete the category
             await fetch("http://localhost:8000/softDeleteProductType/" + categoryId , condition);
            
            // await deleteCategory.json();

            dispatch({
                type: ON_DELETE_CATEGORY_SUCCESS
            })
        }catch(error){
            // console.log(error);
            dispatch({
                type: ON_DELETE_CATEGORY_ERROR
            })
        }
    }
}