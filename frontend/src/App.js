import DefaultLayout from "./layout/DefaultLayout";
import AdminLayout from "./layout/AdminLayout";
import ForbiddenPage from "./layout/403";
import "./styles/style.css";
import { Route, Routes} from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { refreshCustomerLogin } from "./actions/action";
import { CircularProgress } from "@mui/material";
function App() {
  const {customerData} = useSelector(state => state.productReducer);
  const [foundRoleAdmin, setFoundRoleAdmin] = useState(false);
  const [loadingUser, setLoadingUser] = useState(false);
  const dispatch = useDispatch();
  //check if refresh token is exist or not
  const refreshToken = document.cookie.split("=")[1];
  
  // Call api for nonUser cookies
  const nonUserActivities = async () => {
    try{
        const condition = {
          method: "POST",
          headers: {
            "Content-type": "application/json, charset=utf-8"
          },
          credentials: "include",
        }
        await fetch("http://localhost:8000/nonUserACtivities", condition);
    }catch(error){
        console.assert(error);
    }
  }

  // Call api to refresh user data
  useEffect(()=> {
    const checkRole = async()=>{
      try{
        setLoadingUser(true);
        if(refreshToken){
          await dispatch(refreshCustomerLogin());
        }
        setLoadingUser(false);
      }catch(error){
        //Some error
      }
    }
    checkRole()
    nonUserActivities();
  },[dispatch, refreshToken])

  // find the role of user
  useEffect(()=>{
    if(JSON.stringify(customerData) !== JSON.stringify({})){
      if(customerData.role.admin === 6666 && !foundRoleAdmin){
          setFoundRoleAdmin(true)
      }
    }
  },[customerData, foundRoleAdmin, loadingUser])

  // Loading User
  if(loadingUser){
    return(
      <>
        <div style={{
           display: "flex",
           justifyContent: "center",
           alignItems: "center",
           textAlign: "center",
           minHeight: "100vh",
        }}>
          <CircularProgress/>
        </div>
      </>
    )
  }
  return (  
      <>
        <Routes>
            {/* Shopping Home Page */}
            <Route path="/*"  element={<DefaultLayout/>}></Route>
            {/* Admin role only */}
            <Route path="/Admin/*" element={
              foundRoleAdmin
                ?
                (<AdminLayout/>)
                :
                (<ForbiddenPage/>)
            }></Route>
            {/* Forbidden Page */}
            <Route path="/403" element={<ForbiddenPage />}/>

        </Routes>
      </>
  );
}

export default App;
