import RouteList from "../../Routes";
import { useLocation} from "react-router-dom";
import { Breadcrumbs, Link } from "@mui/material";
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import { useSelector } from "react-redux";

export default  function ShopBreadcrump  () {
    const {productObject} = useSelector(state => state.productDetailReducer);
    
    //Lấy dữ liệu param id product
    let currentLocation = useLocation().pathname;

    //Nếu 1 product được chọn điều hướng sang product Detail
    if(JSON.stringify(productObject) !== JSON.stringify({})){
      currentLocation = "/Shop/:ProductDetail"
    }

    const getRouteName = (pathname, routes) => {
        //tìm path trùng theo url
        const currentRoute = RouteList.find((route) => route.path === pathname);
        //trả về tên route nếu tìm thấy
        return currentRoute ? currentRoute.name : false
    }; 
    
    const getBreadcrumbs = (location) => {
        const breadcrumbs = [];
      
        location.split('/').reduce((prev, curr, index, array) => {
          let currentPathname = `${prev}/${curr}`;
          let routeName = getRouteName(currentPathname, RouteList);
          //Nếu product detail được chọn, breadcrumb name bằng tên sản phẩm
          if(currentPathname === "/Shop/:ProductDetail"){
              routeName = productObject.name
          }
          //thêm gia trị vào breadcrumb
          routeName &&
            breadcrumbs.push({
              pathname: currentPathname,
              name: routeName,
              active: index + 1 === array.length ? true : false,
            })
          return currentPathname
        })
        return breadcrumbs
    }
    const breadcrumbs = getBreadcrumbs(currentLocation);
  
    return(
        <Breadcrumbs 
          separator={<NavigateNextIcon fontSize="small" style={{color: "black"}} />} 
          style=
          {
            {
              position:"relative",
              height: "100px",
              marginLeft:"50px",
            }
          }>
            <Link underline="hover" href={"/"} sx={{color: "black"}}>Home</Link>
            {breadcrumbs.map((breadcrumb, index) => {
            return (
            <Link underline="hover" sx={{color: "black"}}
                {...(breadcrumb.active ? { underline: 'none' } : { href: breadcrumb.pathname })}
                key={index}
            >
                {breadcrumb.name}
            </Link>
            )
        })}
        </Breadcrumbs>
    )
}