import { Route, Routes} from "react-router-dom";

export default function AppContent ({RouteList}) {
    return(
        <>
            <Routes>
                {RouteList.map((route, index) => {
                        return(
                            route.element && (
                                <Route
                                    key={index}
                                    path={route.path}
                                    exact ={route.exact}
                                    name={route.name}
                                    element = {<route.element/>}
                                />
                            )
                        )
                    })
                }
            </Routes>
        </>
    )
}