import CheckIcon from '@mui/icons-material/Check';
import { useSelector } from 'react-redux';

export default function ProgressCheckout (){
    const {payment, confirmOrder}  = useSelector(state => state.productDetailReducer);
    if(confirmOrder){
        return(
            <>
               <div className="progressCheckout-container">
                    <div className="progressCheckpoint-header">
                        <p>SHIPPING</p>
                        <p>PAYMENT</p>
                        <p>CONFIRMATION</p>
                    </div>
                    <div className="progressCheckpoint-bar">
                        <div className='progressCheckpoint-icon'>
                            <CheckIcon style={{ color: "white"}}></CheckIcon>
                        </div>
                        <div className='progressCheckpoint-line'>
                        </div>
                    
                        <div className='progressCheckpoint-icon'>
                            <CheckIcon style={{ color: "white"}}></CheckIcon>
                        </div>
                        <div className='progressCheckpoint-line'>

                        </div>
                        <div className='progressCheckpoint-icon'>
                            <CheckIcon style={{ color: "white"}}></CheckIcon>
                        </div>
                    </div>
                </div>
            </>
        )
    }
    return(
        <>
            {
                payment ?
                <div className="progressCheckout-container">
                    <div className="progressCheckpoint-header">
                        <p>SHIPPING</p>
                        <p>PAYMENT</p>
                        <p>CONFIRMATION</p>
                    </div>
                    <div className="progressCheckpoint-bar">
                        <div className='progressCheckpoint-icon'>
                            <CheckIcon style={{ color: "white"}}></CheckIcon>
                        </div>
                        <div className='progressCheckpoint-line'>
                        </div>
                        <div className='progressCheckpoint-icon'>
                            <CheckIcon style={{ color: "white"}}></CheckIcon>
                        </div>
                        <div className='progressCheckpoint-line'>
                        </div>
                        <div className='progressCheckpoint-icon-not-check'>   
                        </div>
                    </div>
                </div>
                :
                <div className="progressCheckout-container">
                    <div className="progressCheckpoint-header">
                        <p>SHIPPING</p>
                        <p>PAYMENT</p>
                        <p>CONFIRMATION</p>
                    </div>
                    <div className="progressCheckpoint-bar">
                        <div className='progressCheckpoint-icon'>
                            <CheckIcon style={{ color: "white"}}></CheckIcon>
                        </div>
                        <div className='progressCheckpoint-line'>
                        </div>                
                        <div className='progressCheckpoint-icon-not-check'>
                        </div>
                        <div className='progressCheckpoint-line-not-check-1'>
                        </div>
                        <div className='progressCheckpoint-icon-not-check'>   
                        </div>
                    </div>
                </div>    
            }  
        </>
    )
}