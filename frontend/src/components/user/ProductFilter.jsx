import { Checkbox } from "@mui/material";
import {useDispatch} from "react-redux";
import { 
    onChangeFilterName,
    onChangeFilterMinPrice,
    onChangeFilterMaxPrice,
    onChangeFilterTypeBed,
    onChangeFilterTypeSofa,
    onChangeFilterTypeChair,
    onChangeFilterTypeBookShelf,
    onChangeFilterTypeDecorItem,
    onChangeFilterTypeDesk,
    onChangeFilterTypeTvStand,
} from "../../actions/shop.action";
import {  useState } from "react";

import { onClickFilterProduct } from "../../actions/action";

export default function ProductFilter () {
    const [bedChecked, setBedChecked] = useState(false);
    const [sofaChecked, setSofaChecked] = useState(false);
    const [chairChecked, setChairChecked] = useState(false);
    const [bookShelfChecked, setBookShelfChecked] = useState(false);
    const [decorItemChecked, setDecorItemChecked] = useState(false);
    const [deskChecked, setDeskChecked] = useState(false);
    const [tvStandChecked, setTvStandChecked] = useState(false);

    const dispatch = useDispatch();

    //hàm thay đổi state khi nhập filter name
    const onChangeName = (event) => {
        dispatch(onChangeFilterName(event.target.value));
    }
    //hàm thay đổi state khi nhập filter min price
    const onChangeMinPrice = (event) => {
        dispatch(onChangeFilterMinPrice(event.target.value));
    }

    //hàm thay đổi state khi nhập filter mã price
    const  onChangeMaxPrice = (event) => {
        dispatch(onChangeFilterMaxPrice(event.target.value));
    }

    //hàm thay đổi state khi chọn filter sofa
    const handlerSofaChecked = (event) => {
        setSofaChecked(event.target.checked);
        if(!sofaChecked ) {
            dispatch(onChangeFilterTypeSofa(true));
        }
        if(sofaChecked ) {
            dispatch(onChangeFilterTypeSofa(false));
        }
    }
    
    //hàm thay đổi state khi chọn filter bed 
    const  handlerBedChecked = (event) => {
        setBedChecked(event.target.checked);
        if(!bedChecked) {
            dispatch(onChangeFilterTypeBed(true));
        }
        if(bedChecked) {
            dispatch(onChangeFilterTypeBed(false));
        }
    }

    //hàm thay đổi state khi chọn filter desk
    const handlerDeskChecked = (event) => {
        setDeskChecked(event.target.checked);
        if(!deskChecked) {
            dispatch(onChangeFilterTypeDesk(true));
        }
        if(deskChecked ) {
            dispatch(onChangeFilterTypeDesk(false));
        }
    }

    //hàm thay đổi state khi chọn filter chair
    const handlerChairChecked = (event) => {
        setChairChecked(event.target.checked);
        if(!chairChecked){
            dispatch(onChangeFilterTypeChair(true));
        }
        if(chairChecked ){
            dispatch(onChangeFilterTypeChair(false));
        }
    }
    //hàm thay đổi state khi chọn filter bookShelf
    const handlerBookShelfChecked = (event) => {
        setBookShelfChecked(event.target.checked);
        if(!bookShelfChecked){
            dispatch(onChangeFilterTypeBookShelf(true));
        }
        if(bookShelfChecked ){
            dispatch(onChangeFilterTypeBookShelf(false));
        }
    }

    //hàm thay đổi state khi chọn decor item
    const handlerItemChecked = (event) => {
        setDecorItemChecked(event.target.checked);
        if(!decorItemChecked){
            dispatch(onChangeFilterTypeDecorItem(true));
        }
        if(decorItemChecked ){
            dispatch(onChangeFilterTypeDecorItem(false));
        }
    }

    //hàm thay đổi state khi chọn tv stand
    const handlerTvStandChecked = (event) =>{
        setTvStandChecked(event.target.checked);
        if(!tvStandChecked){
            dispatch(onChangeFilterTypeTvStand(true));
        }
        if(tvStandChecked ){
            dispatch(onChangeFilterTypeTvStand(false));
        }
    }
    //Nút Filter được ấn
    const handlerFilterClick = () => {
        dispatch(onClickFilterProduct());
    }
    return(
        <>
            <div className="filter-product-name">
                <p>Filter product name</p>
                <input onChange={onChangeName} type="text" placeholder="product name..."/>
            </div>
            <div className="filter-product-price">
                <p>Filter price</p>
                <div className="filter-product-price-input">
                    <input onChange={onChangeMinPrice} type="number" placeholder="min"/>
                    <p>-</p>
                    <input  onChange={onChangeMaxPrice} type="number" placeholder="max"/>
                </div>
                
            </div>
            <div className="filter-product-type">
                <p>Filter product type</p>
                <p>
                    <Checkbox 
                        checked={sofaChecked}
                        onChange={handlerSofaChecked}
                        sx={{ 
                        color: "rgb(68, 63, 63)",
                        '&.Mui-checked': {
                            color:'rgb(97, 94, 68)',
                        },
                        }}>
                    </Checkbox> Sofa</p> 
                <p>
                    <Checkbox  
                        checked={deskChecked}
                        onChange={handlerDeskChecked}
                        sx={{ 
                            color: "rgb(68, 63, 63)",
                            '&.Mui-checked': {
                            color:'rgb(97, 94, 68)',
                            },
                        }}>
                    </Checkbox> Desk</p> 
                <p>
                    <Checkbox  
                        checked = {chairChecked}
                        onChange = {handlerChairChecked}
                        sx={{ 
                        color: "rgb(68, 63, 63)",
                        '&.Mui-checked': {
                            color:'rgb(97, 94, 68)',
                        },
                        }}>
                    </Checkbox> Chair</p> 
                <p>
                    <Checkbox  
                        checked = {bookShelfChecked}
                        onChange = {handlerBookShelfChecked}
                        sx={{ 
                        color: "rgb(68, 63, 63)",
                        '&.Mui-checked': {
                            color:'rgb(97, 94, 68)',
                        },
                        }}>
                    </Checkbox> Bookshelf</p> 
                <p>
                    <Checkbox 
                        checked = {tvStandChecked}
                        onChange={handlerTvStandChecked} 
                        sx={{ 
                        color: "rgb(68, 63, 63)",
                        '&.Mui-checked': {
                            color: "rgb(97, 94, 68)",
                        },
                        }}>
                    </Checkbox> Tv Stand</p>
                <p>
                    <Checkbox  
                        checked = {bedChecked}
                        onChange = {handlerBedChecked}
                        sx={{ 
                        color: "rgb(68, 63, 63)",
                        '&.Mui-checked': {
                            color: "rgb(97, 94, 68)",
                        },
                        }}>
                    </Checkbox> Bed</p> 
                <p>
                    <Checkbox 
                        checked = {decorItemChecked}
                        onChange =  {handlerItemChecked}
                        sx={{ 
                        color: "rgb(68, 63, 63)",
                        '&.Mui-checked': {
                            color: "rgb(97, 94, 68)",
                        },
                        }}>
                    </Checkbox> Decor Item</p>      
            </div>
            <div className="shop-product-filter-button-container">
                <button onClick={handlerFilterClick}>Filter</button>
            </div>
        </>
    )
}