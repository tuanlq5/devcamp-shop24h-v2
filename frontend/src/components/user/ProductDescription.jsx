import { useSelector } from "react-redux";

export default function ProductDescription(){
    const {productObject} = useSelector(state => state.productDetailReducer);

    return(
        <div className="product-detail-box">

            {JSON.stringify(productObject) === `{}` ? <></>:
                <div>
                    <p>Description</p>
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Doloribus, odit! Delectus harum atque laborum, non distinctio facilis accusamus, sed dolorem molestiae enim ut asperiores, doloremque repudiandae ipsum ea. Aspernatur, deserun.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cumLorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.</p>
                    <div >
                        <img src={productObject.imageURL[2]} alt="discription" style={{
                            width: "100%"
                        }}/>
                    </div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquid est reiciendis sed aspernatur temporibus voluptate! Debitis dolores adipisci sunt repudiandae eaque, quos dolorem itaque inventore minima reprehenderit nisi, deleniti consectetur!</p>
                    <div>
                    <img src={productObject.imageURL[3]} alt="discription" />
                    </div>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aliquid est reiciendis sed aspernatur temporibus voluptate! Debitis dolores adipisci sunt repudiandae eaque, quos dolorem itaque inventore minima reprehenderit nisi, deleniti consectetur!</p>
                </div>
            }
        </div>
    )
}