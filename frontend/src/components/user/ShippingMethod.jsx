import LocalShippingIcon from '@mui/icons-material/LocalShipping';
import { useDispatch, useSelector } from 'react-redux';
import { 
    onClickShippingEMS, 
    onClickShippingDHL, 
    onClickShippingFedex, 
    onClickShippingNhatTin, 
    onClickShippingUps, 
    onClickShippingTnt } from '../../actions/productDetail.action';
export default function ShippingMethod(){
    const dispatch = useDispatch();
    const {payment}  = useSelector(state => state.productDetailReducer);
    const handleEMS = () => {
        dispatch(onClickShippingEMS());
    }
    const handleUps = () => {
        dispatch(onClickShippingUps());
    }
    const handleDHL = () => {
        dispatch(onClickShippingDHL());
    }
    const handleFedEx = () => {
        dispatch(onClickShippingFedex());
    }
    const handleTNT = () => {
        dispatch(onClickShippingTnt());
    }
    const handleNhatTin = () => {
        dispatch(onClickShippingNhatTin());
    }
    return(
        <>
        {
        payment ? <></>
        :
            <div className="shipingMethod-container">
                <p>Select a Shipping Method</p>
                <p><LocalShippingIcon></LocalShippingIcon> Choose a shipping method</p>
                <div className="shipMethod-card">
                    <button onClick={handleEMS}></button> {/* EMS */ }
                    <button onClick={handleUps}></button> {/* UPS */ }
                    <button onClick={handleDHL}></button> {/* DHL */ }
                    <button onClick={handleFedEx}></button> {/* FedEx */ }
                    <button onClick={handleTNT}></button> {/* TNT */ }
                    <button onClick={handleNhatTin}></button> {/* NhatTin */ }
                </div>
            </div>
        }
            
        </>
    )
}