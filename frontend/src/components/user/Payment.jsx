import {TextField} from "@mui/material";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { onClickPay } from "../../actions/productDetail.action";
import { onChangeOrderStatus } from "../../actions/productDetail.action";
export default function Payment (){
    const dispatch = useDispatch();
    const {payment, confirmOrder}  = useSelector(state => state.productDetailReducer);
    const order = JSON.parse(localStorage.getItem("order"));
    const handleVisa = () => {

    }

    const  handleMasterCard = () => {

    }


    const handleJCB = () => {

    }
    //hàm xử lý khi người dùng ấn nút trả
    const handlePay = () => {
        dispatch(onClickPay());
        //xoá thông tin mặt hàng trên local storage
        localStorage.removeItem("cart");
        //xoá thông tin order trên local storage
        localStorage.removeItem("order");
        //xoá voucher trên local storage
        localStorage.removeItem("voucher");
        //thay đổi order status
        dispatch(onChangeOrderStatus(order._id));
    }

    if(confirmOrder){
        return(
            <>
                <div className="payment-done-container">
                    <p>Great! Your Order is On the Way ✔</p>
                    <div className="payment-done-picture"></div>
                </div>
            </>
        )
    }
    return(
        <>
            {payment ? 
                <div className="payment-container">
                    <p>Choose Your Paying Method</p>
                    <div className="payment-box">
                        <div className="payment-method-button">
                            <button onClick= {handleVisa}></button>   {/* Visa */}
                            <button onClick={handleMasterCard}></button>   {/* Master card */}
                            <a href="https://www.paypal.com/signin">
                                  {/* Paypal */}  
                            </a>
                            <button onClick={handleJCB}></button>   {/* JCB */}
                        </div>
                        <div className="payment-method-info">
                            <TextField sx={{
                                width: "48%"
                            }}  label="Card Holder" variant="outlined" />
                            <TextField sx={{
                                width: "48%"
                            }} label="Card Number" variant="outlined" />
                            <TextField sx={{
                                width: "48%"
                            }} label="Expire Date" variant="outlined" />
                            <TextField sx={{
                                width: "48%"
                            }} label="CVC" variant="outlined" />
                        </div>
                        <button className="pay-button" onClick={handlePay}>Pay</button>
                    </div>
                </div>
            : 
            <></>
            }
        </>
    )
}