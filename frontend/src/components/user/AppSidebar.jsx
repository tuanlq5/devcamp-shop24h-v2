import { useSelector, useDispatch} from "react-redux";
import { closeSideBar } from "../../actions/sidebar.action";
export default function SideBar (){
    const dispatch = useDispatch();
    const {openSideBar} = useSelector(state => state.sideBarReducer);

    //hàm đóng sidebar
    const handlerCloseSideBar = () => {
        dispatch(closeSideBar());
    }
    return(
        // <!-- side bar-->
        <>
            <div 
            style={
                openSideBar ? {width :"200px"} : {width :"0px"}
            } 
            id="mySidebar" className="sidebar">
                <button  className="closebtn" onClick={handlerCloseSideBar}>×</button>
                <a href="/">Home</a>
                <a href="/Shop">Shop</a>
                <a href="/#"> Sale</a>
                <a href="/#">Contacts</a>
                <a href="/#">Q&A</a>
            </div>
        </>
    )
}