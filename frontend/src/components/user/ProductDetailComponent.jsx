import ImageGallery from "react-image-gallery";
import Rating from '@mui/material/Rating';
import { useSelector, useDispatch } from 'react-redux';
import { onChangeQuantity, onClickAddProductToCart } from "../../actions/productDetail.action";



export default function ProductDetailComponent (){
    const dispatch = useDispatch();
    const {productObject, quantity, productAddtoCart} = useSelector(state => state.productDetailReducer);
    const {customerData} = useSelector (state => state.productReducer);
    let images = [];
    if( JSON.stringify(productObject) === `{}`){
      images = [
        {
          original: 'https://picsum.photos/id/1018/1000/600/',
          thumbnail: 'https://picsum.photos/id/1018/250/150/',
          originalHeight: "100%"
        },
      ];
    }else{
       images = [
        {
          original: productObject.imageURL[0],
          thumbnail: productObject.imageURL[0],
          originalHeight: "100%"
        },
        {
          original: productObject.imageURL[1],
          thumbnail: productObject.imageURL[1],
          originalHeight: "100%"
        },
        {
          original: productObject.imageURL[2],
          thumbnail: productObject.imageURL[2],
          originalHeight: "100%"
        },
        {
          original: productObject.imageURL[3],
          thumbnail: productObject.imageURL[3],
          originalHeight: "100%"
        },
      ];
    }
    
    // hàm giảm quantity
    const handlerIncreaseQuantity = async (productData, value) => {
      try {
          const product ={
              product: productData.id,
              quantity: value
          }
          
          if(JSON.stringify(customerData) !== JSON.stringify({})){
              const condition = {
                  method: "POST",
                  headers: {
                      "Content-type": "application/json; charset=utf-8",
                  },
                  body: JSON.stringify(product),
              }
              await fetch("http://localhost:8000/addProductCart/" + customerData._id, condition);
          }

          const condition = {
              method: "POST",
              headers: {
                  "Content-type": "application/json; charset=utf-8",
              },
              body: JSON.stringify(product),
              credentials: "include",
          }
          await fetch("http://localhost:8000/addProductCartNonUser", condition);

      } catch (error) {
          console.assert(error)
      }
  }

    //hàm xử lý khi người dung nhập quantity
    const onChangeAddQuantity = (event) => {
      dispatch(onChangeQuantity(event.target.value, productObject));
    }

    
    //hàm xử lý khi người dùng ấn nút add product to cart
    const onClickAddProductToCarts = () => {
      //Kiểm tra xem quantity được nhập hay không
      if(quantity === ""){ //string
        alert("Quantity has to be a number!");
        return false;
      }
      if(quantity <= 0){ //lớn hơn 0
        alert("Quantity has to be larger than 0!");
        return false;
      }

      //Thêm thông tin khi ấn add to cart
      dispatch(onClickAddProductToCart(productAddtoCart, quantity));
      handlerIncreaseQuantity(productAddtoCart,quantity);
    }
    return(
        <>
            <div className="product-picture-box">
                    <ImageGallery  items={images}  thumbnailPosition='left' />
            </div>
            <div className="product-info-box">
                <p>{productObject.name}</p>
                <p>{productObject.buyPrice} $</p>
                <Rating name="read-only" value={4} readOnly />
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Repudiandae molestias, tempore nostrum eos consequuntur magnam voluptatum praesentium accusantium ab ipsam! Cumque reprehenderit maiores modi assumenda repellat ab iste ad cum.</p>
                <div>
                    <p>Add Quantity</p>
                    <input type="number"  onChange={onChangeAddQuantity}/>
                </div>
                <button onClick={onClickAddProductToCarts}>ADD TO CART</button>
            </div>
        </>
    )
}