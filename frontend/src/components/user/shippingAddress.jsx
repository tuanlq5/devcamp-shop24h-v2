import { TextField } from "@mui/material"
import React, { useEffect, useState} from 'react'
import {FormControl, InputLabel, Select, MenuItem, OutlinedInput } from "@mui/material";
import LockIcon from '@mui/icons-material/Lock';
import { useSelector, useDispatch } from "react-redux";
import { 
    onChangeCityShippingAddress, 
    onChangeFullNameShippingAddress,
    onChangeEmailShippingAddress,
    onChangePhoneShippingAddress,
    onChangeZipcodeShippingAddress,
    onChangeAddressShippingAddress,
    onChangeCountryShippingAddress,
    onChangeAdministrativeDivisionShippingAddress
} from "../../actions/shippingAction";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    },
  },
};
export default function ShippingAddress () {
    const dispatch = useDispatch();

    const {customerData} = useSelector(state => state.productReducer);

    const [countryData, setCountryData] = useState([]); 

    const {payment}  = useSelector(state => state.productDetailReducer);

    const { 
        validateFullName,
        validateEmail,
        validatePhone,
        validateZipcode,
        validateAddress,
        validateCity,
        validateCountry,
        validateAdministrativeDivision} = useSelector(state => state.shippingReducer);

    const [administrativDivisionData, setAdministrativeDivisionData] = useState([]);

    const [country, setCountry] = useState("");

    const [administrativeDivision, setAdministrativeDivision] = useState("");

    const onChangeFullName = (event) => {
        const order = JSON.parse(localStorage.getItem("order"));
        dispatch(onChangeFullNameShippingAddress(event.target.value, order._id));
    }

    const onChangeEmail = (event) => {
        dispatch(onChangeEmailShippingAddress(event.target.value))
    }

    const onChangeAddress = (event) => {
        dispatch(onChangeAddressShippingAddress(event.target.value));
    }

    const onChangePhone = (event) => {
        dispatch(onChangePhoneShippingAddress(event.target.value));
    }
    
    const onChangeCity = (event) => {
        dispatch(onChangeCityShippingAddress(event.target.value));
    }

    const onChangeZipCode = (event) => {
        dispatch(onChangeZipcodeShippingAddress(event.target.value));
    }

    const handleChangeCountry = (event) => {
        setCountry( event.target.value);
        dispatch(onChangeCountryShippingAddress(event.target.value));
    }

    const handleChangeAdministrativeDivision = (event) => {      
        setAdministrativeDivision(event.target.value);
        dispatch(onChangeAdministrativeDivisionShippingAddress(event.target.value));
    }

    const handleRequestCountryData = async (err, data ) => {
        try{
            const requestCountryData = await fetch ("http://localhost:8000/country", {method:"GET"});
            const data = await requestCountryData.json();
            setCountryData(data.data);
        }catch(err){
            console.log(err);
        }
    }

    useEffect(() => {
        handleRequestCountryData();
    },[])

    useEffect(()=> {
    
        //change Adminstrative division selection when changing country 
        if(country === "None"){
            setAdministrativeDivision("");
            setAdministrativeDivisionData([]);
        }
        for  (let index = 0; index < countryData.length; index++){
            if(countryData[index].code === country){
                setAdministrativeDivisionData(countryData[index].administrativeDivision);
            }
        }
        if(JSON.stringify(customerData) !== JSON.stringify({}) && JSON.stringify(countryData) !== JSON.stringify([])){
            setCountry(customerData.country);
        }
        if(JSON.stringify(customerData) !== JSON.stringify({}) && JSON.stringify(administrativDivisionData) !== JSON.stringify([])){
            setAdministrativeDivision(customerData.administrativeDivision);
        }
  
    },[country, customerData, administrativDivisionData, countryData])
    return(
        <>
            {
            payment ? <></>
            :
            <div className="ShippingAddress-container" >
                <p>Shipping Address</p>
                <div className="shipping-address-box">
                    {   
                        validateFullName ? 

                        <TextField label="Full name is not valid" error  style={{
                            width:"46%" 
                        }} id="outlined-size-normal" onChange={onChangeFullName} value={customerData.fullName}  />
                        :
                        <TextField label="Full name" style={{
                            width:"46%"
                        }} id="outlined-size-normal" onChange={onChangeFullName} value={customerData.fullName}  />
                    }
                    

                    {
                        validateEmail ? 
                        <TextField label="Email is not valid" error style={{
                            width:"46%"
                        }} id="outlined-size-normal" onChange={onChangeEmail} value={customerData.email}/>
                        :
                        <TextField label="Email"  style={{
                            width:"46%"
                        }} id="outlined-size-normal" onChange={onChangeEmail} value={customerData.email}/>
                    }


                    {
                        validateAddress ?
                        <TextField label="Address in not valid" error style={{
                            width:"46%"
                        }} id="outlined-size-normal" onChange={onChangeAddress} value={customerData.address} />
                        :
                        <TextField label="Address" style={{
                            width:"46%"
                        }} id="outlined-size-normal" onChange={onChangeAddress} value={customerData.address} />
                    }

                    {
                        validatePhone ? 
                        <TextField label="Phone is not valid" error style={{
                            width:"46%"
                        }} id="outlined-size-normal" onChange={onChangePhone} value={customerData.phone} />
                        :
                        <TextField label="Phone" style={{
                            width:"46%"
                        }} id="outlined-size-normal" onChange={onChangePhone} value={customerData.phone} />
                    }
                    
                    {
                        validateCity ? 
                        <TextField label="City is not valid " error style={{
                            width:"46%"
                        }}  id="outlined-size-normal" onChange={onChangeCity} value={customerData.city} />
                        :
                        <TextField label="City" style={{
                            width:"46%"
                        }}  id="outlined-size-normal" onChange={onChangeCity} value={customerData.city} />
                    }

                    {
                        validateZipcode ? 
                        <TextField label="Zipcode is not valid" error style={{
                            width:"46%"
                        }} id="outlined-size-normal"  onChange={onChangeZipCode}/>
                        :
                        <TextField label="Zipcode" style={{
                            width:"46%"
                        }} id="outlined-size-normal"  onChange={onChangeZipCode}/>
                    }

                    {
                        validateCountry ?
                        <FormControl error sx={{  minWidth: "96%" }} size="small">
                            <InputLabel  sx={{ translate: "-5px" }}>Country is not valid</InputLabel>
                            <Select
                                value={country}
                                label="Country is not valid"
                                input={<OutlinedInput label="Country is not valid" />}
                                onChange={handleChangeCountry}
                                MenuProps={MenuProps}
                            >
                                {countryData.map((element,index) => 
                                    <MenuItem value={element.code} key={index}>
                                        <em>{element.name}</em>
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                        :
                        <FormControl sx={{  minWidth: "96%" }} size="small">
                            <InputLabel sx={{ translate: "-5px" }}>Country</InputLabel>
                            <Select
                                value={country}
                                label="Country"
                                input={<OutlinedInput label="Country" />}
                                onChange={handleChangeCountry}
                                MenuProps={MenuProps}
                            >
                                {countryData.map((element,index) => 
                                    <MenuItem value={element.code} key={index}>
                                        <em>{element.name}</em>
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    }
                    

                    {
                        validateAdministrativeDivision ?
                        <FormControl error sx={{  minWidth: "96%" }} size="small">
                            <InputLabel sx={{ translate: "-5px" }}>Adminitrative Division is not valid</InputLabel>
                            <Select
                                value={administrativeDivision}
                                label="Adminitrative Division is not valid"
                                input={<OutlinedInput label="Adminitrative Division is not valid" />}
                                onChange={handleChangeAdministrativeDivision}
                                MenuProps={MenuProps}
                            >
                                {administrativDivisionData.map((element,index) => 
                                    <MenuItem value={element} key={index}>
                                        <em>{element}</em>
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                        :
                        <FormControl sx={{  minWidth: "96%" }} size="small">
                            <InputLabel id="demo-select-small" sx={{ translate: "-5px" }}>Adminitrative Division</InputLabel>
                            <Select
                                value={administrativeDivision}
                                label="Adminitrative Division"
                                input={<OutlinedInput label="Adminitrative Division" />}
                                onChange={handleChangeAdministrativeDivision}
                                MenuProps={MenuProps}
                            >
                                {administrativDivisionData.map((element,index) => 
                                    <MenuItem value={element} key={index}>
                                        <em>{element}</em>
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    }
                   
                </div>
                <p><LockIcon></LockIcon> Your information is secure</p>
            </div>
        }
            
        </>
    )
}