import {  useEffect, useState} from 'react';
import { useSelector } from 'react-redux';
import auth from '../../firebase';
import { onAuthStateChanged, signOut } from 'firebase/auth';
import { signInGoogleAccountSuccessful } from '../../actions/action';
import { useDispatch } from 'react-redux';
import { signOutGoogleAccount } from '../../actions/action';
import { openSideBar } from '../../actions/sidebar.action';
import { refreshCustomerLogin, logOutCustomerAccount } from '../../actions/action';
import React from 'react';
import { useNavigate } from 'react-router-dom';

function Navigator () {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { customerData, googleAccount} = useSelector((data) => data.productReducer); //thông tin google account

    //check if refresh token is exist or not
    const refreshToken = document.cookie.split("=")[1];
    
    let quantity = 0;
    if( JSON.parse(localStorage.getItem("cart")) !== null){
        quantity = JSON.parse(localStorage.getItem("cart")).length;
    }
    const [openUserDropDown, setUserDropDown] = useState(false);

    const handleLogOut = () => {
        //Hàm logout Google acount
        signOut(auth)
        .then(() => {
            dispatch(signOutGoogleAccount())
        }
        ).catch((error) =>{
            console.log(error)
        }
        )
        //logout user account
        dispatch(logOutCustomerAccount());
    }
    //hàm mở side bar
    const handlerOpenSideBar = () => {
        dispatch(openSideBar());
    }
    //hàm mở drop down user
    const handleOpenUserDropDown = () => {
        if(openUserDropDown === false){
            setUserDropDown(true)
        }else{
            setUserDropDown(false)
        }
    }

    //chuyển sang trang Admin
    const handleAdminPage = () => {
        navigate("/Admin")
    }
      
    useEffect(() => {
        //dữ thông tin google acount khi quay lại trang 
        onAuthStateChanged (auth,  (result) => {
          if(result){
            dispatch(signInGoogleAccountSuccessful(result));
          }else{
            dispatch(signInGoogleAccountSuccessful({}));
          }
        })
       
        //refresh customer login
        if(refreshToken){
            dispatch(refreshCustomerLogin());
        }
      }, [dispatch, refreshToken]);


    if(JSON.stringify(googleAccount) !== JSON.stringify({})){
        return(
             <>
        {/* <!-- Navigate bar --> */}
        <section id="navigate">
            <div className="container-navigation">

                <div className="top-navigate-bar">

                    <div className="top-navigate-bar-box1">

                        <p>Free Shipping - 20km from the store</p>

                    </div>
                    <div className="top-navigate-bar-box2">
                        <p>VND/USD</p>
                            <img src={googleAccount.photoURL} alt="avatar"/>
                        <div>
                            <button className='username-button' onClick={handleOpenUserDropDown}>{googleAccount.displayName}</button>
                            <div style={
                                openUserDropDown ? {translate:"0px",opacity: 0.9 } : { opacity: 0 ,translate: "0 -200px"}
                            } className="user-dropdown-content">
                                <button>My profile</button>
                                <button>Edit account</button>
                                <button onClick={handleLogOut}>Sign out</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bottom-navigate-bar">
                    <button className="button-navigator" onClick={handlerOpenSideBar} >☰</button>
                    <a className='logo-brand' href="/#">
                        <img src={require("../../assets/images/logos/logo.png")} alt="logo"/>
                    </a>
                        <div>
                            <a href="/">Home</a>
                            <a href="/Shop">Shop</a>
                            <a href="/#"> Sale</a>
                            <a href="/#">Contacts</a>
                            <a href="/#">Q&A</a>
                        </div>
                </div>
                <div className="bottom-navigate-bar-box1">
                    <a href="/ProductCart">
                        <img src={require("../../assets/images/icon/Bag-removebg-preview.png")} alt="bag"/>
                        <p>{quantity}</p>
                    </a>
                </div>
            </div>

            <div className='navigation-fillbox'>Box that fill the navbar gap</div>        
        
        </section>
        </>
        )
    }

    //If customer login successful reload the navbar
    if(JSON.stringify(customerData)  !== JSON.stringify({})){
        return(
            <>
            {/* <!-- Navigate bar --> */}
            <section id="navigate">
                <div className="container-navigation">
    
                    <div className="top-navigate-bar">
    
                        <div className="top-navigate-bar-box1">
    
                            <p>Free Shipping - 20km from the store</p>
    
                        </div>
                        {/* //Load user when login successful */}
                       
                        <div className="top-navigate-bar-box2">
                            <p>VND/USD</p>
                            <div>
                                <button className='username-button' onClick={handleOpenUserDropDown}>{customerData.fullName}</button>
                                <div style={
                                    openUserDropDown ? {translate:"0px",opacity: 0.9 } : { opacity: 0 ,translate: "0 -200px"}
                                } className="user-dropdown-content">
                                    <button>My profile</button>
                                    <button>Edit account</button>
                                    {!customerData.role?.admin ? <></> : <button onClick={handleAdminPage}>Admin</button>}
                                    <button onClick={handleLogOut}>Sign out</button>
                                </div>
                            </div>
                        </div>
                            
                    </div>
                    <div className="bottom-navigate-bar">
    
                        <button className="button-navigator" onClick={handlerOpenSideBar} >☰</button>
                        <a className='logo-brand' href="/#">
                            <img src={require("../../assets/images/logos/logo.png")} alt="logo"/>
                        </a>
                            <div>
                                <a href="/">Home</a>
                                <a href="/Shop">Shop</a>
                                <a href="/#"> Sale</a>
                                <a href="/#">Contacts</a>
                                <a href="/#">Q&A</a>
                            </div>
                    </div>
                    <div className="bottom-navigate-bar-box1">
                        <a href="/ProductCart">
                            <img src={require("../../assets/images/icon/Bag-removebg-preview.png")} alt="bag"/>
                            <p>{quantity}</p>
                        </a>
                    </div>
                </div>
    
                <div className='navigation-fillbox'>Box that fill the navbar gap</div>        
            
            </section>
            </>
        )
    }

    return(
        <>
            {/* <!-- Navigate bar --> */}
            <section id="navigate">
                <div className="container-navigation">
    
                    <div className="top-navigate-bar">
    
                        <div className="top-navigate-bar-box1">
    
                            <p>Free Shipping - 20km from the store</p>
    
                        </div>

                        <div className="top-navigate-bar-box2">
                            <a href="/Login">Login</a> / <a href="/Register">Register</a>
                        </div> 
                           
                    </div>
                    <div className="bottom-navigate-bar">
    
                        <button className="button-navigator" onClick={handlerOpenSideBar} >☰</button>
                        <a className='logo-brand' href="/#">
                            <img src={require("../../assets/images/logos/logo.png")} alt="logo"/>
                        </a>
                            <div>
                                <a href="/">Home</a>
                                <a href="/Shop">Shop</a>
                                <a href="/#"> Sale</a>
                                <a href="/#">Contacts</a>
                                <a href="/#">Q&A</a>
                            </div>
                    </div>
                    <div className="bottom-navigate-bar-box1">
                        <a href="/ProductCart">
                            <img src={require("../../assets/images/icon/Bag-removebg-preview.png")} alt="bag"/>
                            <p>{quantity}</p>
                        </a>
                    </div>
                </div>
    
                <div className='navigation-fillbox'>Box that fill the navbar gap</div>        
            
            </section>
            </>
    )
}

export default React.memo(Navigator);