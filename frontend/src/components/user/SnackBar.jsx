import {Snackbar} from '@mui/material';
import MuiAlert from '@mui/material/Alert';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

export default function SnackbarComponent (){
    const [openLogin, setOpenUserLogin] = useState(false);
    const [openCoupon, setCouponSnackbar] = useState(false);
    const {customerData} = useSelector(state => state.productReducer);
    const {percentDiscount} = useSelector (state => state.productDetailReducer);

    const handleCloseLogin = () => {
        setOpenUserLogin(false);
    }
    const handleCloseCoupon = () => {
        setCouponSnackbar(false)
    }
    useEffect(()=> {
        if(JSON.stringify(customerData) !== JSON.stringify({})){
            setOpenUserLogin(true)
        }
    }, [customerData])
    return(
        <>
            {/* Snackbar when user Login  */}
            <Snackbar open = {openLogin} autoHideDuration = {3000} onClose = {handleCloseLogin}>
                <MuiAlert  elevation={6}  variant="filled" severity='success' sx={{width:"100%", backgroundColor:"rgb(104 90 53)"}}>
                    Login successful!
                </MuiAlert>
            </Snackbar>
            {/* Snackbar when coupon is exist */}
            <Snackbar open = {openCoupon} autoHideDuration = {3000} onClose = {handleCloseCoupon}>
                <MuiAlert  elevation={6}  variant="filled" severity='success' sx={{width:"100%", backgroundColor:"rgb(104 90 53)"}}>
                    You Got {percentDiscount}% off from coupon !
                </MuiAlert>
            </Snackbar>
        </>
    )
}