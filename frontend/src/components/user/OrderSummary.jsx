import { useSelector } from "react-redux";


// Create our number formatter.
const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0,
    //These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
    //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
  });

export default function OrderSummary () {
    const {shippingCost, confirmOrder } = useSelector(state => state.productDetailReducer);
    const orderId = JSON.parse(localStorage.getItem("order"));
    const orderProduct =  JSON.parse(localStorage.getItem("cart"));
    const voucher = JSON.parse(localStorage.getItem("voucher"));
    //truncate the product name 
    function truncate(str, n){
        return (str.length > n) ? str.slice(0, n-1) + '...' : str;
    };
    // totalPay
    let totalPay = 0;

    if(orderId !== null){
        totalPay = orderId.cost
    }
    
    return(
        <>
            {
                confirmOrder ? 
                <></>
                :
                <div className="orderSummary-conatiner">
                    {orderId !== null ? 
                        <>
                        <div className="orderSummary-code" style={{textAlign: "center"}}>
                            <p>Order Summary: {orderId._id}</p>
                        </div>
                        <div className="orderSummary-box-1">
                            <p>Date</p>
                            <p>{orderId.orderDate.slice(0,10)}</p>
                        </div>
                        <div className="orderSummary-box-2">
                            <p>Time</p>
                            <p>{orderId.orderDate.slice(11,16)} {orderId.orderDate.slice(11,13) > 12 ? "PM" : "AM" }</p>
                        </div>
                        </>
                    :   
                        <></>
                    }
                   
                    <div className="orderSummary-box-3">
                        <p>------------------------------------------------------------------------------</p>
                    </div>
                    <div className="orderSummary-box-4">
                        <p>Products</p>
                        <p>Quantity</p>
                    </div>
                    {   orderProduct !== null ?
                    
                        orderProduct.map((element, index) =>
                            
                            <div className="orderSummary-box-5" key={index}>
                                <p>{truncate(element.name,30)}</p>
                                <p>{element.quantity}</p>
                            </div>
                        )
                        :
                        <></>   
                    }
                    <div className="orderSummary-box-6">
                        <p>------------------------------------------------------------------------------</p>
                    </div>
                    {
                        voucher !== null ?
                        voucher.discountPercent === 0 ? <></> :
                        <div className="orderSummary-box-7">
                            <p>{voucher.code}</p>
                            <p>COUPON APPLIED: {voucher.discountPercent}%</p>
                        </div>
                        : 
                        <></>
                    }

                    <div className="orderSummary-box-8">
                        <p>Subtotal</p>
                        <p>{formatter.format(totalPay)}</p>
                    </div>
                    <div className="orderSummary-box-9">
                        <p>Shiping Price</p>
                        <p>{formatter.format(shippingCost)}</p>
                    </div>
                    <div className="orderSummary-box-10">
                        <p>------------------------------------------------------------------------------</p>
                    </div>
                    <div className="orderSummary-box-11">
                        <p>Total</p>
                        <p>USD {formatter.format(totalPay + shippingCost)}</p>
                    </div>
                </div>
            }
            
        </>
    )
}