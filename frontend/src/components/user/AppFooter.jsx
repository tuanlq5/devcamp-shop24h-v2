import logo from "../../assets/images/logos/logo.png";

export default function Footer () {
   
    return(  
        // <!-- Footer -->
        <section id="footer">
            <div className="footer-container">
                <div className="footer-box-1">
                    <div  className="footer-column-1">
                        <p>Support</p>
                        <p>About Us</p>
                        <p>Career</p>
                        <p>Blogs</p>
                    </div>
                    <div  className="footer-column-2">
                        <p>Term & Policy </p>
                        <p>General Rule</p>
                        <p>Warranty Policy</p>
                        <p>Refund Policy</p>
                    </div>
                    <div  className="footer-column-3">
                        <p>Contact Us</p>
                        <p>Address 1: Lorem, ipsum dolor sit amet consectetur adipisicing elit.is</p>
                        <p>Phone: 0999999999</p>
                        <p>
                            <i className="fa-brands fa-facebook fa-2xl"></i>
                            &ensp;<i className="fa-brands fa-instagram fa-2xl"></i>
                            &ensp;<i className="fa-brands fa-twitter fa-2xl"></i>
                            &ensp;<i className="fa-brands fa-pinterest fa-2xl"></i>
                        </p>
                    </div>
                    <div  className="footer-column-4">
                        <img src={logo} alt="logo"/>
                    </div>
                </div>
                <div className="footer-box-2">
                    <p>@2023 Design by Le Quang Tuan</p>
                </div>
            </div>
        </section>
    )
}