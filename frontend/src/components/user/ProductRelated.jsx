import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { solid } from '@fortawesome/fontawesome-svg-core/import.macro';
import { useSelector } from "react-redux";

export default function ProductRelate () {
    const {productFindByType} = useSelector(state => state.productDetailReducer);
    
    // Hàm rút ngắn câu (truncate a string to String...)
    function truncate(str, n){
        return (str.length > n) ? str.slice(0, n-1) + '...' : str;
      };
    return(
        <div className='product-relate-box'>
            <p>Relate Product</p>
            <div className="product-relate">
                {   
                // Map product Card
                productFindByType.map((elements, index) => {
                    return(
                        <div className='shop-product-card' key={index}>
                            <img src={elements.imageURL[0]} alt={elements.name} />
                            <p> { truncate(elements.name, 35) }</p>
                            <p>Price: {elements.buyPrice} $</p>
                            <a href={"/Shop/" + elements._id} >More Detail <FontAwesomeIcon icon={solid("arrow-right")} /></a>
                        </div>
                        )
                    })
                }
            </div>
        </div>
    )
}