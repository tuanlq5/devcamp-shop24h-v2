import { useSelector, useDispatch } from "react-redux";
import { FormControl, Select, MenuItem  } from "@mui/material";
import { changeProductNumberInShop } from "../../actions/shop.action";
export default function SelectNumberPage (){
    const dispatch = useDispatch();
    const {limit} = useSelector((reduxData) => reduxData.productReducer);
    const handleChange = (event) => {
        dispatch(changeProductNumberInShop(event.target.value));
    }
    return(
        <>
            <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                <Select
                value={limit}
                onChange={handleChange}
                label="Rows"
                >
                <MenuItem value={0}>
                    <em>None</em>
                </MenuItem>
                <MenuItem value={9}>9</MenuItem>
                <MenuItem value={15}>15</MenuItem>
                <MenuItem value={20}>20</MenuItem>
                </Select>
            </FormControl>
        </>
    )
}