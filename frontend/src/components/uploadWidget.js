import { useEffect, useRef } from "react"
const UploadWidget = () => {
    const cloudinaryRef = useRef();
    const widgetRef = useRef();
    useEffect(() => {
        cloudinaryRef.current =  window.cloudinary;
        cloudinaryRef.current.createUploadWidget({
            cloudName: 'dkrcnwc5m',
            uploadpreset: "e6qum7pn"
        }, function (error, result) {
            console.log(result)
        })
    }, [])
}
export default  UploadWidget