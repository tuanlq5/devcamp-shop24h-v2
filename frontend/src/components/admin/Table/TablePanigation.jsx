import Table from "@mui/material/Table"
import Paper from '@mui/material/Paper';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { styled } from "@mui/material";
import tableCellClasses from "@mui/material/TableCell";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
}));
export default function TablePaginationComponent (
    {   
        columns,
        currentPage,
        rowPerPage,
        rowPerPageOption,
        handleChangePage,
        handleChangeRowsPerPage,
        TableRowComponent,
        length,
        headerColor,
        Height
    }
    ){
    
    return(
        <>
            <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                <TableContainer sx={{ height: Height}}>
                    <Table sx={{ minWidth: 500 }} aria-label="custom pagination table">
                    <TableHead sx={{backgroundColor: headerColor}}>
                        <StyledTableRow >
                        {columns.map((column, index) => (
                            
                            <StyledTableCell
                            key={index}
                            align={column.align}
                            style={{ minWidth: column.minWidth, color:"white", height: column.maxHeight}}
                            >
                            {column.label}
                            </StyledTableCell>
                        ))}
                        </StyledTableRow>
                    </TableHead>
                    {/* Table Content */}
                    <TableRowComponent/>

                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={rowPerPageOption}
                    component="div"
                    count={length}
                    rowsPerPage={rowPerPage}
                    page={currentPage}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleChangeRowsPerPage}
                />
            </Paper>
        </>
    )
}