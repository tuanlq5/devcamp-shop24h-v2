import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const ComboChart = ({label, orderSuccess, refund}) => {
    const chartRef = useRef(null);

    useEffect(() => {
        let chartInstance = null;

        const createChart = () => {
        const ctx = chartRef.current.getContext('2d');

        if (chartInstance) {
            chartInstance.destroy();
        }

        const data = {
                labels: label,
                datasets: [
                    // {
                    // label: 'Earning',
                    // data: [23, 32, 35, 28, 27, 33, 40, 36, 27, 23, 20, 25],
                    // fill: false,
                    // backgroundColor:"#EB7BF9",
                    // tension: 0.1,
                    // },
                    {
                    label: 'Refund',
                    data: refund,
                    fill: false,
                    backgroundColor:"#A1A8F7",
                    },
                    {
                    label: 'Order',
                    data: orderSuccess,
                    backgroundColor: 'rgb(226, 215, 167)',
                    },
                ],
            };
        chartInstance = new Chart(ctx, {
            type: 'bar',
            data: data,
            options: {
                plugins:{
                    tooltip:{
                        callbacks:{
                            label:(item)=> `  ${item.formattedValue}%`
                        }
                    }
                },
                responsive: true,
                scales: {
                    y: {
                        beginAtZero: true,
                        scaleLabel: {
                            display: true,
                            labelString: '%',
                        },
                    },
                },
                barPercentage: 0.6, // Adjust the bar width
                
            },
        });
        };

        createChart();

        return () => {
        if (chartInstance) {
            chartInstance.destroy();
        }
        };
    }, [refund, label, orderSuccess]);

    return (
        <div className="combo-chart-container" style={{position: "relative", width:"95%"}}>
            <canvas ref={chartRef}></canvas>
        </div>
    );
};

export default ComboChart;