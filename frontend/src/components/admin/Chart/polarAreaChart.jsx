import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const PolarAreaChart = () => {
  const chartRef = useRef(null);

  useEffect(() => {
    let chartInstance = null;

    const fetchData = async () => {
      // Retrieve data for the polar area chart from the backend API
      // Example:
      // const chartData = await fetch('/api/polar-chart-data');

      // Example data for the polar area chart
      const labels = ['Label 1', 'Label 2', 'Label 3', 'Label 4', 'Label 5'];
      const data = [10, 20, 15, 25, 30];

      const ctx = chartRef.current.getContext('2d');

      // Destroy any existing chart instance using the canvas
      if (chartInstance) {
        chartInstance.destroy();
      }

      // Create a new chart instance
      chartInstance = new Chart(ctx, {
        type: 'polarArea',
        data: {
          labels: labels,
          datasets: [
            {
              label: 'Data',
              data: data,
              backgroundColor: [
                'rgba(255, 99, 132, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(75, 192, 192, 0.5)',
                'rgba(153, 102, 255, 0.5)',
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
              ],
              borderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,
          scales: {
            r: {
              beginAtZero: true,
              suggestedMax: 40,
            },
          },
        },
      });
    };

    fetchData();

    // Clean up the chart when the component unmounts
    return () => {
      if (chartInstance) {
        chartInstance.destroy();
      }
    };
  }, []);

  return <canvas ref={chartRef}></canvas>;
};

export default PolarAreaChart;