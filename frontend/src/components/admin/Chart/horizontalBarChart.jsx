import React, { useState, useEffect, useRef } from 'react';
import Chart from 'chart.js/auto';

function HorizontalBarChart() {
  const chartRef = useRef(null);
  const chartInstanceRef = useRef(null); // Use a separate ref for chart instance

  const [chartData] = useState({
    labels: ['VN', 'CHN', 'CAN', 'SGP', 'MYS'],
    values: [45, 20, 60, 30, 40],
  });

  useEffect(() => {
    const ctx = chartRef.current.getContext('2d');

    // Destroy any existing chart instance
    if (chartInstanceRef.current) {
      chartInstanceRef.current.destroy();
    }

    // Create the chart
    chartInstanceRef.current = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: chartData.labels,
        datasets: [
          {
            label: 'MOST SALE',
            data: chartData.values,
            backgroundColor: 'rgba(75, 192, 192, 0.6)',
            barThickness: 10
        },
        ],
      },
      options: {
        indexAxis: 'y',
        scales: {
          x: {
            beginAtZero: true,
          },
        },
        plugins: {
            legend: {
                display: false, // Show the legend
                labels: {
                  font: {
                    size: 12, // Adjust the font size
                  },
                  boxWidth: 10, // Adjust the legend box width
                },
              },
          },
      },
    });

    // Cleanup function
    return () => {
      // Destroy the chart instance when the component is unmounted
      if (chartInstanceRef.current) {
        chartInstanceRef.current.destroy();
      }
    };
  }, [chartData]);

  return <canvas ref={chartRef} />;
}

export default HorizontalBarChart;