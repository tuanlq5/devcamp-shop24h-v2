import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const LineChart = ({width, legendDisplay, xDisplay, yDisplay, marginTop, lineColor, label, data}) => {
    const chartRef = useRef(null);

    useEffect(() => {
      let chartInstance = null;
  
      const fetchData = async () => {
        // Retrieve data for the line chart from the backend API
        // Example:
        // const chartData = await fetch('/api/line-chart-data');
  
        // Example data for the line chart
        // const labels = ['January', 'February', 'March', 'April', 'May', 'June'];
        // const data = [10, 20, 15, 25, 30, 20];
  
        const ctx = chartRef.current.getContext('2d');
  
        // Destroy any existing chart instance using the canvas
        if (chartInstance) {
          chartInstance.destroy();
        }
        
        // Create a new chart instance
        chartInstance = new Chart(ctx, {
          type: 'line',
          data: {
            labels: label,
            datasets: [
              {
                label: 'Cart ACtivities',
                data: data,
                borderColor: lineColor,
                backgroundColor: lineColor,
                tension: 0.4,
              },
            ],
          },
          options: {
            plugins:{
              legend:{
                display: legendDisplay
              },
              tooltip:{
                  callbacks:{
                      label:(item)=> `  ${item.formattedValue}%`
                  }
              }
            },
            responsive: true,
            scales: {
              x: {
                display: xDisplay,
                title: {
                  display: true,
                  text: 'Month',
                },
              },
              y: {
                display: yDisplay,
                title: {
                  display: true,
                  text: 'Add Product %',
                },
              },
            },
          },
        });
      };
  
      fetchData();
  
      // Clean up the chart when the component unmounts
      return () => {
        if (chartInstance) {
          chartInstance.destroy();
        }
      };
    }, [legendDisplay, xDisplay, yDisplay, lineColor, data, label]);
  
    return (
      <div className="line-chart-container" style={{position: "relative", width:width, marginTop: marginTop}}>
        <canvas id="lineChart" ref={chartRef} ></canvas>
      </div>
    );
};

export default LineChart;
