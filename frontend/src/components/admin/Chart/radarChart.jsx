import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const RadarChart = () => {
  const chartRef = useRef(null);

  useEffect(() => {
    let chartInstance = null;

    const fetchData = async () => {
      // Retrieve data for the radar chart from the backend API
      // Example:
      // const chartData = await fetch('/api/radar-chart-data');

      // Example data for the radar chart
      const labels = ['Label 1', 'Label 2', 'Label 3', 'Label 4', 'Label 5'];
      const data = [10, 20, 15, 25, 30];

      const ctx = chartRef.current.getContext('2d');

      // Destroy any existing chart instance using the canvas
      if (chartInstance) {
        chartInstance.destroy();
      }

      // Create a new chart instance
      chartInstance = new Chart(ctx, {
        type: 'radar',
        data: {
          labels: labels,
          datasets: [
            {
              label: 'Data',
              data: data,
              backgroundColor: 'rgba(54, 162, 235, 0.5)',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1,
              pointBackgroundColor: 'rgba(54, 162, 235, 1)',
              pointBorderColor: '#fff',
              pointBorderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,
          scales: {
            r: {
              beginAtZero: true,
              suggestedMax: 40,
              grid: {
                color: 'rgba(0, 0, 0, 0.1)',
              },
              angleLines: {
                color: 'rgba(0, 0, 0, 0.1)',
              },
              pointLabels: {
                font: {
                  size: 12,
                },
              },
            },
          },
        },
      });
    };

    fetchData();

    // Clean up the chart when the component unmounts
    return () => {
      if (chartInstance) {
        chartInstance.destroy();
      }
    };
  }, []);

  return <canvas ref={chartRef}></canvas>;
};

export default RadarChart;
