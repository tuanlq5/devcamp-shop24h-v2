import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const BarChart = () => {
  const chartRef = useRef(null);

  useEffect(() => {
    let chartInstance = null;

    const createChart = () => {
      const ctx = chartRef.current.getContext('2d');

      if (chartInstance) {
        chartInstance.destroy();
      }

      chartInstance = new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ['Label 1', 'Label 2', 'Label 3', 'Label 4', 'Label 5', 'Label 4', 'Label 5'],
          datasets: [
            {
              label: 'Custom Shape Bar',
              data: [10, 20, 75, 25, 30, 54, 60],
              backgroundColor: 'rgb(226, 215, 167)',
              borderColor: 'rgb(222, 205, 91)',
              borderWidth: 2,
              barBorderRadius: 4, // Set the border radius value
            },
          ],
        },
        options: {
          responsive: true,
          scales: {
            y: {
              beginAtZero: true,
            },
          },
          barPercentage: 0.4, // Adjust the bar width

        },
      });
    };

    createChart();

    return () => {
      if (chartInstance) {
        chartInstance.destroy();
      }
    };
  }, []);

  return (
    <div class="bar-chart-container" style={{position: "relative", height:"90%", marginTop:"20px"}}>
      <canvas ref={chartRef}></canvas>
    </div>
  );
};

export default BarChart;