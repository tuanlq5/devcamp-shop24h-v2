import React, {  useState } from 'react';
import {
  ComposableMap,
  Geographies,
  Geography,
  
 
} from 'react-simple-maps';
import { scaleLinear } from "d3-scale";
import countryData from "./countryData.json";

const colorScale = scaleLinear()
  .domain([1, 50])
  .range(["#ffedea", "#ff5233"]);


function WorldMapChart({setNewCountry, country}) {
  //2 data: 1 for Chonopleth, 1 for marker
  // const [dataMaker] = useState([
  //   { countryCode: 'USA', latitude: 37.0902, longitude: -95.7129, value: 50 },
  //   { countryCode: 'CAN', latitude: 56.1304, longitude: -106.3468, value: 30 },
  //   { countryCode: 'MEX', latitude: 23.6345, longitude: -102.5528, value: 25 },
  //   { countryCode: 'AUS', latitude: -25.2744, longitude: 133.7751, value: 15 },
  //   // Add more countries and their corresponding values
  // ]);
  const [data] = useState(countryData);
 
  return (
    <div id="world-map-chart" style={{width:"100%", height:"245px"}}>
      <ComposableMap
        projection="geoMercator"
        projectionConfig={{
          rotate: [-10, 0, 0],
          center:[2,30],
          scale: 150,
        }}
        width={300}
        height={1000}
        style={{
          width: '100%',
          height: '300px',
        }}
      >

      
        {data.length > 0 && (
        <Geographies geography={require("./world.json")} stroke="#D6D6DA" strokeWidth={0.5}>
          {({ geographies }) =>
            geographies.map((geo) => {
              const countryCode = geo.id;
              const countryData = data.find((d) => d.countryCode === countryCode);
              return (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  fill={countryData ? colorScale(countryData.value)  : '#EAEAEC'}
                  onMouseEnter={()=>{
                    setNewCountry(`${geo.properties.name}`)
                  
                  }}
                  onMouseLeave={()=>{
                    setNewCountry("")
                  }}
                  style={{
                    outline: "none",
                    hover: {
                      fill: '#F60',
                      cursor: 'pointer',
                    },
                  }}
                />
              );
            })
          }
        </Geographies>
        )}
        {/* {dataMaker.map((marker, index) => (
          <Marker key={index} coordinates={[marker.longitude, marker.latitude]}>
            <circle r={6} fill="#9DF06A" />
            <text
              textAnchor="middle"
              y={marker.latitude > 0 ? -15 : 15}
              style={{ fontSize: '30px', fontWeight:"600"}}
              fill="#9DF06A"
            >
              {marker.countryCode}
            </text>
          </Marker>
        ))} */}
      </ComposableMap>
    </div>
  );
};

export default WorldMapChart;