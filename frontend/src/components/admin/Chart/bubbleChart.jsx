import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const BubbleChart = () => {
  const chartRef = useRef(null);

  useEffect(() => {
    let chartInstance = null;

    const fetchData = async () => {
      // Retrieve data for the bubble chart from the backend API
      // Example:
      // const chartData = await fetch('/api/bubble-chart-data');

      // Example data for the bubble chart
      const data = [
        { x: 10, y: 20, r: 5 },
        { x: 15, y: 10, r: 8 },
        { x: 5, y: 25, r: 12 },
        { x: 8, y: 15, r: 10 },
      ];

      const ctx = chartRef.current.getContext('2d');

      // Destroy any existing chart instance using the canvas
      if (chartInstance) {
        chartInstance.destroy();
      }

      // Create a new chart instance
      chartInstance = new Chart(ctx, {
        type: 'bubble',
        data: {
          datasets: [
            {
              label: 'Bubble Dataset',
              data: data,
              backgroundColor: 'rgba(75, 192, 192, 0.5)',
              borderColor: 'rgba(75, 192, 192, 1)',
            },
          ],
        },
        options: {
          responsive: true,
          scales: {
            x: {
              title: {
                display: true,
                text: 'X Axis',
              },
              beginAtZero: true,
            },
            y: {
              title: {
                display: true,
                text: 'Y Axis',
              },
              beginAtZero: true,
            },
          },
        },
      });
    };

    fetchData();

    // Clean up the chart when the component unmounts
    return () => {
      if (chartInstance) {
        chartInstance.destroy();
      }
    };
  }, []);

  return <canvas ref={chartRef}></canvas>;
};

export default BubbleChart;