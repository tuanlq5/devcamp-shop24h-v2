import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const ScatterChart = () => {
  const chartRef = useRef(null);

  useEffect(() => {
    let chartInstance = null;

    const fetchData = async () => {
      // Retrieve data for the scatter chart from the backend API
      // Example:
      // const chartData = await fetch('/api/scatter-chart-data');

      // Example data for the scatter chart
      const data = [
        { x: 10, y: 20 },
        { x: 15, y: 25 },
        { x: 20, y: 30 },
        { x: 25, y: 35 },
        { x: 30, y: 40 },
      ];

      const ctx = chartRef.current.getContext('2d');

      // Destroy any existing chart instance using the canvas
      if (chartInstance) {
        chartInstance.destroy();
      }

      // Create a new chart instance
      chartInstance = new Chart(ctx, {
        type: 'scatter',
        data: {
          datasets: [
            {
              data: data,
              backgroundColor: 'rgba(54, 162, 235, 0.5)',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,
          scales: {
            x: {
              type: 'linear',
              position: 'bottom',
            },
            y: {
              type: 'linear',
              position: 'left',
            },
          },
        },
      });
    };

    fetchData();

    // Clean up the chart when the component unmounts
    return () => {
      if (chartInstance) {
        chartInstance.destroy();
      }
    };
  }, []);

  return <canvas ref={chartRef}></canvas>;
};

export default ScatterChart;