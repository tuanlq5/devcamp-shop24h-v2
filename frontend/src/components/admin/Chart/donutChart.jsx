import React, { useEffect, useRef } from 'react';
import  Chart  from 'chart.js/auto';

const DonutChart = (data) => {
  const chartRef = useRef(null);

  useEffect(() => {
    let chartInstance = null;
    const fetchData = async () => {
      // Retrieve data for the donut chart from the backend API
      // Example:
      // const chartData = await fetch('/api/donut-chart-data');

      // Example data for the donut chart
      // const labels = ['Category 1', 'Category 2', 'Category 3', 'Category 4','Category 5','Category 6','Category 7'];
      // const data = [30, 20, 10, 40 , 30 ,50 , 34];

      const ctx = chartRef.current.getContext('2d');

      // Destroy any existing chart instance using the canvas
      if (chartInstance) {
        chartInstance.destroy();
      }

      // Create a new chart instance
      chartInstance = new Chart(ctx, {
        type: 'doughnut',
        data: {
          labels: data.data[0],
          datasets: [
            {
              label: 'Sales',
              data: data.data[1],
              backgroundColor: [
                'rgba(255, 99, 132, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(255, 206, 86, 0.8)',
                'rgba(75, 192, 192, 0.8)',
                '#A1A8F7',
                "#DBA1F7",
                "#F7E4A1"
              ],
              borderColor: 'rgba(255, 255, 255, 1)',
              borderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,
          aspectRatio: 1,
          plugins:{
            legend:{
              position:"right",
              //onHover:{},
              labels:{
                boxWidth:  10,
                usePointStyle: true,
                pointStyle:"rectRounded"
              }
            }
          },  
        },
      });
    };

    fetchData();

    // Clean up the chart when the component unmounts
    return () => {
      if (chartInstance) {
        chartInstance.destroy();
      }
    };
  }, [data]);

  return (
    <div className='donut-chart-container' style={{
      position: "relative", 
      height:"52%", 
      width:"100%",  
      display:'flex',
      alignItems:"center",
      justifyContent:"center"
      }}>
      <canvas ref={chartRef} ></canvas>
    </div>
  );
};

export default DonutChart;