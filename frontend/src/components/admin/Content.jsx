import { Route, Routes} from "react-router-dom";

export default function AdminContent ({AdminRouteList}) {
    return(
        <>
            <div className="admin-content-container">
                <Routes>
                    {AdminRouteList.map((route, index) => {
                        return(
                            route.element && (
                                <Route
                                    key={index}
                                    path={route.path}
                                    exact ={route.exact}
                                    name={route.name}
                                    element = {<route.element/>}
                                />
                            )
                        )
                    })
                    }
                </Routes>
            </div>
        </>
    )
}