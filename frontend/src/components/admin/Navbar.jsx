import { useSelector, useDispatch} from 'react-redux';
import { useState, useEffect} from 'react';
import { onAuthStateChanged, signOut } from 'firebase/auth';
import auth from '../../firebase';
import { signOutGoogleAccount } from '../../actions/action';
import { refreshCustomerLogin, logOutCustomerAccount } from '../../actions/action';
import { signInGoogleAccountSuccessful } from '../../actions/action';
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import { useNavigate } from 'react-router-dom';

export default function NavbarAdmin({setSideBar}){
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { customerData, googleAccount} = useSelector((data) => data.productReducer); //thông tin google account, tai khoản
    const [openUserDropDown, setUserDropDown] = useState(false);

    //check if refresh token is exist or not
    const refreshToken = document.cookie.split("=")[1];
    //hàm mở drop down user
    const handleOpenUserDropDown = () => {
        if(openUserDropDown === false){
            setUserDropDown(true)
        }else{
            setUserDropDown(false)
        }
    }
    const handleLogOut = async() => {
        try{
            //Hàm logout Google acount
            await signOut(auth)
            .then(() => {
                dispatch(signOutGoogleAccount())
            }
            ).catch((error) =>{
                console.log(error)
            }
            )
            //logout user account
            await dispatch(logOutCustomerAccount());
            navigate("/");
        }catch(error){
            // some error
        }  
    }

    useEffect(() => {
        //dữ thông tin google acount khi quay lại trang 
        onAuthStateChanged (auth,  (result) => {
          if(result){
            dispatch(signInGoogleAccountSuccessful(result));
          }else{
            dispatch(signInGoogleAccountSuccessful({}));
          }
        })
       
        if(refreshToken){
            dispatch(refreshCustomerLogin());
        }
    }, [dispatch, refreshToken]);

    const googleAccountDisplay  = (
        <>
            <div className="navigate-admin-account-box">
                <img src={googleAccount.photoURL} alt="avatar"/>
                <div>
                    <button className='admin-button' onClick={handleOpenUserDropDown}>{googleAccount.displayName}</button>
                    <ul style={
                        openUserDropDown ? {maxHeight:"500px" } : {maxHeight:"0px" }
                    } className="user-dropdown-content">
                        <li><button>My profile</button></li>
                        <li><button>Edit account</button></li>
                        <li><button onClick={handleLogOut}>Sign out</button></li>
                    </ul>
                </div>
            </div>
        </>
    )

    const normalAccountDisplay = (
        <>
            <div className="navigate-admin-account-box">
                <div>
                    <button className='admin-button' onClick={handleOpenUserDropDown}><AccountBoxIcon/>&ensp;{customerData.fullName}</button>
                    <ul style={
                        openUserDropDown ? {maxHeight:"500px" } : {maxHeight:"0px" }
                    } className="user-dropdown-content">
                        <li><button>My profile</button></li>
                        <li><button>Edit account</button></li>
                        <li><button onClick={handleLogOut}>Sign out</button></li>
                    </ul>
                </div>
            </div>
        </>
    )

    const userNotLogin = (
        <>
            <div className="top-navigate-bar-box2"style={{color: "black"}}>
                <a href="/Login" style={{color: "black"}}>Login</a> / <a href="/Admin/AdminRegister" style={{color: "black"}}>Register</a>
            </div> 
        </>
    )
    //if admin already login to google account
        return(
            <>
                <Box sx={{ flexGrow: 1 }}>
                    <AppBar position="static" sx={{
                        backgroundColor:"rgb(214, 204, 163)",
                        color: "black",
                        height:"85px",
                        boxSizing:"border-box",
                        flexDirection: "row",
                    }}>
                        <Toolbar sx={{
                            width: "100%",
                        
                        }}>
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ 
                                mr: 2 , 
                            }}
                            onClick={setSideBar}
                        >
                            <MenuIcon />
                        </IconButton>
                        <div className='logo-brand-admin'>
                            <a href="/"><img src={require("../../assets/images/logos/logo.png")} alt="logo" /></a>
                        </div>
                            {/* If user login to google account */}
                            {   JSON.stringify(googleAccount) !== JSON.stringify({}) ? googleAccountDisplay : <></>}
                            {/* If user login to normal account */}
                            {   JSON.stringify(customerData) !== JSON.stringify({}) ? normalAccountDisplay : <></>}
                            {/* If user not login */}
                            {   JSON.stringify(customerData) === JSON.stringify({}) &&
                                JSON.stringify(googleAccount) === JSON.stringify({})
                                ? userNotLogin : <></>}
                        </Toolbar>
                    </AppBar>
                </Box>
            </>
        )
}