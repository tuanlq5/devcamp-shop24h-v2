import * as React from 'react';
import Box from '@mui/material/Box';
import { Drawer } from '@mui/material';
import { useState } from 'react';
import ShoppingBagIcon from '@mui/icons-material/ShoppingBag';
import InventoryIcon from '@mui/icons-material/Inventory';
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import DiscountIcon from '@mui/icons-material/Discount';
import ViewListIcon from '@mui/icons-material/ViewList';
import InsertChartIcon from '@mui/icons-material/InsertChart';
import KeyboardDoubleArrowUpIcon from '@mui/icons-material/KeyboardDoubleArrowUp';


export default function SideBar ({openSidebar, setSideBar}){
    const [order, setOrder] = useState(false);
    const [product, setProduct] = useState(false);
    const [customer, setCustomer] = useState(false);
    const [coupon, setCoupon]  = useState(false);
    const [category, setCategory] = useState(false);
    const [analize, setAnalize]  = useState(false);
    
    const showListOrderItem = () => {
       order ? setOrder(false) : setOrder(true);
    }

    const showProductListItem  = () => {
        product ? setProduct(false)  : setProduct(true);
    }
    
    const showCustomerListItem= () => {
        customer ? setCustomer(false) : setCustomer(true);
    }
    
    const showCouponListItem = () => {
        coupon ? setCoupon(false) : setCoupon(true);
    }

    const showCategoriesListItem = () => {
        category ? setCategory(false) : setCategory(true);
    }

    const showDataAnalyzeListItem = () => {
        analize ? setAnalize(false) : setAnalize(true);
    }

    return(
        <>
            <Drawer 
                open={openSidebar}
                onClose={setSideBar}
                >
                <Box
                    sx={{ width: 250, backgroundColor: "black", marginTop: "50px"}}
                    role="presentation"
                    >  
                    <div className='sidebar-item-box'>
                        <div>
                            <p><ShoppingBagIcon/>Order</p>
                            <button onClick={showListOrderItem}
                                style={
                                    order ? { transform: "rotate(180deg)"} : { transform: "rotate(0deg)"}
                                }
                            ><KeyboardDoubleArrowUpIcon/></button>
                        </div>
                        <ul style={
                            order ? {maxHeight:"200px"} : {maxHeight: "0px"}
                        }>
                           <li><a href='/Admin/OrderList'>Order List</a></li>     
                        </ul>
                    </div>

                    <div className='sidebar-item-box'>
                        <div>
                            <p><InventoryIcon/>Product</p>
                            <button onClick={showProductListItem} 
                                style={
                                    product ? { transform: "rotate(180deg)"} : { transform: "rotate(0deg)"}
                                }
                            ><KeyboardDoubleArrowUpIcon/></button>
                        </div>
                        <ul style={
                            product ? {maxHeight:"200px"} : {maxHeight: "0px"}
                        }>
                           <li><a href='/Admin/ProductList'>Product List</a></li>
                           <li><a href='/Admin/Addproduct'>Add Product</a></li>    
                        </ul>
                    </div>

                    <div className='sidebar-item-box'>
                        <div>
                            <p><AccessibilityIcon/>Customer</p>
                            <button onClick={showCustomerListItem}
                                style={
                                    customer ? { transform: "rotate(180deg)"} : { transform: "rotate(0deg)"}
                                }
                            ><KeyboardDoubleArrowUpIcon/></button>
                        </div>
                        <ul style={
                            customer ? {maxHeight:"200px"} : {maxHeight: "0px"}
                        }>
                           <li><a href='/Admin/CustomerList'>Customer List</a></li>
                               
                        </ul>
                    </div>

                    <div className='sidebar-item-box'>
                        <div>
                            <p><DiscountIcon/>Coupon</p>
                            <button onClick={showCouponListItem}
                                style={
                                   coupon ? { transform: "rotate(180deg)"} : { transform: "rotate(0deg)"}
                                }
                            ><KeyboardDoubleArrowUpIcon/></button>
                        </div>
                        <ul style={
                            coupon ? {maxHeight:"200px"} : {maxHeight: "0px"}
                        }>
                           <li><a href='/Admin/CouponList'>Coupon List</a></li>
                           <li><a href='/Admin/AddCoupon'>New Coupon</a></li>     
                        </ul>
                    </div>

                    <div className='sidebar-item-box'>
                        <div>
                            <p><ViewListIcon/>Categories</p>
                            <button onClick={showCategoriesListItem}
                                style={
                                    category ? { transform: "rotate(180deg)"} : { transform: "rotate(0deg)"}
                                }
                            ><KeyboardDoubleArrowUpIcon/></button>
                        </div>
                        <ul style={
                            category ? {maxHeight:"200px"} : {maxHeight: "0px"}
                        }>
                           <li><a href='/Admin/CategoryList'>Categories List</a></li>
                           <li><a href='/Admin/NewCategory'>New Categories</a></li>     
                        </ul>
                    </div>

                    <div className='sidebar-item-box'>
                        <div>
                            <p><InsertChartIcon/> Data Analyze</p>
                            <button onClick={showDataAnalyzeListItem}
                                style={
                                    analize ? { transform: "rotate(180deg)"} : { transform: "rotate(0deg)"}
                                }
                            ><KeyboardDoubleArrowUpIcon/></button>
                        </div>
                        <ul style={
                            analize ? {maxHeight:"200px"} : {maxHeight: "0px"}
                        }>
                           <li><a href='/Admin'>Overview</a></li>
                        </ul>
                    </div>
                </Box>
            </Drawer>
        </>
    )
}