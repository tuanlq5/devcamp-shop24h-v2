import AdminRouteList from "../../adminRoutes";
import { useLocation} from "react-router-dom";
import { Breadcrumbs, Link } from "@mui/material";
import NavigateNextIcon from '@mui/icons-material/NavigateNext';


export default  function AdminBreadcrump  () {
    let currentLocation = useLocation().pathname.slice(6);
    if (currentLocation === ""){
      currentLocation = "/"
    }
    const getRouteName = (pathname, routes) => {
        //tìm path trùng theo url
        const currentRoute = AdminRouteList.find((route) => route.path === pathname);
        //trả về tên route nếu tìm thấy
        return currentRoute ? currentRoute.name : false
    }; 
    
    const getBreadcrumbs = (location) => {
        const breadcrumbs = [];
      
        location.split('/').reduce((prev, curr, index, array) => {
          let currentPathname = `${prev}/${curr}`;
          let routeName = getRouteName(currentPathname, AdminRouteList);
          //thêm gia trị vào breadcrumb
          routeName &&
            breadcrumbs.push({
              pathname: currentPathname,
              name: routeName,
              active: index + 1 === array.length ? true : false,
            })
          return currentPathname
        })
        return breadcrumbs
    }
    const breadcrumbs = getBreadcrumbs(currentLocation);
  
    return(
        <Breadcrumbs 
          separator={<NavigateNextIcon fontSize="small" style={{color: "black"}} />} 
          style=
          {
            {
              position:"relative",
              height: "100px",
              marginLeft:"150px",
              marginTop:"40px"
            }
          }>
            <Link underline="hover" href={"/Admin"} sx={{color: "black"}}>Home</Link>
            {breadcrumbs.map((breadcrumb, index) => {
            return (
            <Link underline="hover" sx={{color: "black"}}
                {...(breadcrumb.active ? { underline: 'none' } : { href: breadcrumb.pathname })}
                key={index}
            >
                {breadcrumb.name}
            </Link>
            )
        })}
        </Breadcrumbs>
    )
}