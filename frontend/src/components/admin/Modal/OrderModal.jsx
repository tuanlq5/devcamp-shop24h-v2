import { Modal, Box, Typography } from "@mui/material"
import { editOrderStatus } from "../../../actions/admin/admin.orderAction";
import { useDispatch } from "react-redux";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: "300px",
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
  };
export default function OrderModal({ openEditModal, handleCloseEditModal, deleteOrderModal, handleCloseDeleteOrderModal, orderObject}){
    const dispatch = useDispatch();
    const handleChangeOrderStatus = () => {
        dispatch(editOrderStatus(orderObject));
        handleCloseEditModal();
    }
    
    const handleDeleteOrder = () => {
        console.log(orderObject)
    }
    return(
        <>
            <Modal
                open={openEditModal}
                onClose={handleCloseEditModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h5" component="h2">
                    Edit Order Status
                </Typography>
                <Typography id="modal-modal-description" sx={{ margin: "20px 0 !important"}}>
                    Do you want to change this order status?
                </Typography>
                <button onClick={handleChangeOrderStatus} style={{
                    backgroundColor:"bisque",
                    border:"0px",
                    width:"20%",
                    height:"40px",
                    fontSize: "16px"
                }}>Yes</button>
                <button 
                style={{
                    backgroundColor:"black",
                    border:"0px",
                    width:"40%",
                    height:"40px",
                    marginLeft:"15px",
                    fontSize: "16px",
                    color:"white"
                }} onClick={handleCloseEditModal}>No, Go Back</button>
                </Box>
            </Modal>

            <Modal
                open={deleteOrderModal}
                onClose={handleCloseDeleteOrderModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h5" component="h2">
                    Delete Order
                </Typography>
                <Typography id="modal-modal-description" sx={{ margin: "20px 0 !important"}}>
                    Do you actually want to delete this order?
                </Typography>
                <button  style={{
                    backgroundColor:"bisque",
                    border:"0px",
                    width:"20%",
                    height:"40px",
                    fontSize: "16px"
                }} onClick={handleDeleteOrder}>Yes</button>
                <button 
                style={{
                    backgroundColor:"black",
                    border:"0px",
                    width:"40%",
                    height:"40px",
                    marginLeft:"15px",
                    fontSize: "16px",
                    color:"white"
                }} onClick={handleCloseDeleteOrderModal}>No, Go Back</button>
                </Box>
            </Modal>
        </>
    )
}