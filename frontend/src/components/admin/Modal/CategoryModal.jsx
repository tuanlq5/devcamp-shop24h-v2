import { Modal, Typography, Box, Grid, TextField} from '@mui/material';
import { useState , useEffect } from 'react';
import {useDropzone} from "react-dropzone";
import { useDispatch, useSelector } from 'react-redux';
import { requestEditCategory, requestDeleteCategory} from '../../../actions/admin/admin.categoryAction';
import CircularProgress from '@mui/material/CircularProgress';
import SnackBar from '../snackbar/snackBar';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "6px"
};
const img = {
    height:"20%",
    width: "40%"
 }

export default function CategoryModal (
    {  
        editCategory, 
        handleCloseEditCategory, 
        handleCloseDeleteCategory, 
        deleteCategory,
        productTypeData,
        acceptedFiles,
        setAcceptedFiles
    }){
    const [name, setName] = useState(productTypeData.name ? productTypeData.name : "Sofa" );
    const [description, setDiscription] = useState(productTypeData.description ? productTypeData.description : "");
    const [open, setOpen] = useState(false);
    const [snackbar, setSnackbar] = useState("");
    let newProductType = {
        description: description,
        name: name,
        avatar:""
    };
    const {editCategoryPending, editCategoryStatus,  deleteCategoryStatus} = useSelector(state  => state.adminCategoryReducer);
    const dispatch = useDispatch();
    // dropzone properties 
    const {
        getRootProps, 
        getInputProps
    } = useDropzone({
        accept:{
            'image/png': [],
            'image/jpg': [],
            'image/jpeg': []
        },
        onDrop:  (files) => {
            setAcceptedFiles(
              files.map((file) => Object.assign(file, {
                preview: URL.createObjectURL(file),
              }))
            );
          },
    })

    // avoid memory leak
    useEffect(() => {
        return () => acceptedFiles.forEach(file => URL.revokeObjectURL(file.preview))
        
    },[acceptedFiles])   

    useEffect(() => {
        if(JSON.stringify(productTypeData) !== JSON.stringify({})){
            setName(productTypeData.name);
            setDiscription(productTypeData.description);
        }
    },[productTypeData])
    
    // Accepted drop file
    let acceptFileItems = acceptedFiles.map(file => (
        // <li key={file.path}>
        //     {file.path} - {file.size} bytes
        // </li>
        
            <img 
                src={file.preview} 
                alt="accepted"
                key={file.path}
                style={img}
                onLoad={() => {
                    URL.revokeObjectURL(file.preview)
                }}
            />
    ))
    const handleName = (event) =>{
        setName(event.target.value);
        newProductType.name = event.target.value;
        // console.log(name);
    }
    
    const handleDiscription = (event) => {
        setDiscription(event.target.value);
        newProductType.description = event.target.value;
    }
    // function edit cate gory
    const handleEditCategory = () => {
        dispatch(requestEditCategory(productTypeData, acceptedFiles, newProductType));   
    }
    // function close snackbar
    const handleCloseEditSnackBar = () => {
        setOpen(false);
    }
    // function delete category 
    const  handleDeleteCategory = () => {
        dispatch(requestDeleteCategory(productTypeData._id));
    }
    useEffect(() =>{
        if(editCategoryStatus){
            handleCloseEditCategory();
            setOpen(true);
            setSnackbar("Edit category successful!")
        }
        if(deleteCategoryStatus){
            handleCloseDeleteCategory();
            setOpen(true);
            setSnackbar("Delete category successful!")
        }
    }, [editCategoryStatus, deleteCategoryStatus, handleCloseEditCategory, handleCloseDeleteCategory]);
    return(
        <>  
        <SnackBar open={open} handleClose={handleCloseEditSnackBar} message={snackbar} severity={"success"}></SnackBar>
        {/* Edit Modal */}
            <Modal
                open={editCategory}
                onClose={handleCloseEditCategory}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    {
                    editCategoryPending ? 
                    <CircularProgress sx={{color:"rgb(191, 180, 142)", marginLeft: "50%"}}/>
                    :
                    <>
                    <Typography id="modal-modal-title" variant="h5" component="h2">
                        Edit Category: {productTypeData._id}
                    </Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={12} mt ={2} >
                            <TextField value={name} onChange={handleName} label="name" variant="outlined" sx={{ width: "100%" }}/>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12} mt ={2} >
                            <TextField value={description} onChange={handleDiscription} label="description" variant="outlined" sx={{ width: "100%" }}/>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2}>
                        <Grid item xs={12} mt ={2} mb ={3}>
                            {
                                JSON.stringify(acceptedFiles) === JSON.stringify([])
                                ?
                                <div {...getRootProps({className:"box-drop-update-picture"})}>
                                    <input {...getInputProps()} />
                                    <p>Drag your picture here, or click to select files</p>
                                    <em>(Only jpeg, jpg ,png images will be accepted)</em>
                                </div>
                                :
                                <aside className="droped-file-box">
                                    <div>
                                        <h4>Accepted Image</h4>
                                        <ul>{acceptFileItems}</ul>
                                    </div>
                                </aside>
                            }
                        </Grid>
                    </Grid>

                    <button onClick={handleEditCategory} style={{
                        backgroundColor:"bisque",
                        border:"0px",
                        width:"30%",
                        height:"40px",
                        fontSize: "16px"
                    }}>Edit Category</button>
                    <button 
                    style={{
                        backgroundColor:"black",
                        border:"0px",
                        width:"40%",
                        height:"40px",
                        marginLeft:"15px",
                        fontSize: "16px",
                        color:"white"
                    }} onClick={handleCloseEditCategory}>Go Back</button>
                    </>
                    
                    }
                    
                </Box>
            </Modal>
            {/* delete modal */}
           {/* Delete Modal */}
           <Modal
                keepMounted
                open={deleteCategory}
                onClose={handleCloseDeleteCategory}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >   
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h4" component="h2">
                    Edit category
                </Typography>
                    <Grid container spacing={2}> 
                        <Grid item xs={12} mt={2}>
                            <Typography>Are you sure want to delete this category?</Typography>
                        </Grid>
                        <Grid item xs={12} mt={2} sx={{alignItems: "flex-end"}}>
                            <button onClick={handleDeleteCategory} className="modal-edit-button">Delete the Category</button>
                            <button onClick={handleCloseDeleteCategory} className="modal-delete-button">Go Back</button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </>
    )
}