import { Modal, Typography, Box, Grid, TextField, FormControl, InputLabel, Select, OutlinedInput, MenuItem } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { onChangeAdministrativeDivisionShippingAddress , onChangeCountryShippingAddress } from '../../../actions/shippingAction';
import { updateCustomerData, deleteCustomerById} from '../../../actions/admin/admin.customerAction';
import SnackBar from '../snackbar/snackBar';
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "6px"
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
    },
  },
};
function CustomerModal ({customerInfo, openEditModal ,handleCloseEditModal, deleteCustomerModal, handleCloseDeleteCustomerModal})  {
    const dispatch = useDispatch();

    const {editCustomerStatus, deleteCustomerStatus} = useSelector(state => state.adminCustomerReducer);

    const [administrativDivisionData, setAdministrativeDivisionData] = useState([]);

    const [countryData, setCountryData] = useState([]); 

    const [fullname, setFullName] = useState("");

    const [email, setEmail] = useState("");

    const [phone, setPhone] = useState("");

    const [address, setAddress] = useState("");

    const [city, setCity] = useState("");

    const [country, setCountry] = useState("");

    const [administrativeDivision, setAdministrativeDivision] = useState("");

    const [snackBar, setSnackBar] = useState("");

    const customerData =
        {
            fullName: fullname,
            phone:  phone,
            email: email,
            address: address,
            city: city,
            administrativeDivision: administrativeDivision,
            country: country,
        }
    
    const [open, setOpen] = useState(false);    
   
    const [openDelete, setOpenDelete] = useState(false);

    //close snack bar
    const handleCloseEditSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setOpen(false);
    };
    //close delete modal
    const handleCloseDeleteSnackBar = (event, reason) => {
        if(reason === "clickaway"){
            return;
        }
        setOpenDelete(false);
    }
    //When admin change the product name
    const handleName = (event) => {
        setFullName(event.target.value)
        customerData.fullName = event.target.value;
        console.log(customerData);
    }

    const handleEmail = (event) => {
        setEmail(event.target.value)
        customerData.email = event.target.value;
    }

    const handlePhone = (event) => {
        setPhone(event.target.value)
        customerData.phone = event.target.value;
    }

    const handleAddress = (event) => {
        setAddress(event.target.value)
        customerData.address = event.target.value;
    }

    const handleCity = (event) => {
        setCity(event.target.value);
        customerData.city = event.target.value;
    }
    const handleRequestCountryData = async (err, data ) => {
        try{
            const requestCountryData = await fetch ("http://localhost:8000/country", {method:"GET"});
            const data = await requestCountryData.json();
            setCountryData(data.data);
        }catch(err){
            console.log(err);
        }
    }


    const handleChangeCountry = (event) => {
        setCountry( event.target.value);
        dispatch(onChangeCountryShippingAddress(event.target.value));
        customerData.country = event.target.value;
    }

    const handleChangeAdministrativeDivision = (event) => {
        setAdministrativeDivision(event.target.value);
        dispatch(onChangeAdministrativeDivisionShippingAddress(event.target.value));
        customerData.administrativeDivision = event.target.value;
    }


    const handleEditCustomer = () => {
        dispatch(updateCustomerData(customerData, customerInfo._id));
    }

    const handleDeleteCustomer = () => {
        dispatch(deleteCustomerById(customerInfo._id));
    }
    useEffect(()=> {
        handleRequestCountryData();
        //change Adminstrative division selection when changing country 
        if(country === "None"){
            setAdministrativeDivision("");
            setAdministrativeDivisionData([]);
        }
        for  (let index = 0; index < countryData.length; index++){
            if(countryData[index].code === country){
                setAdministrativeDivisionData(countryData[index].administrativeDivision);
            }
        }
    },[country, countryData])

    useEffect(()=> {

        if(JSON.stringify(customerInfo) !== JSON.stringify({}) && customerInfo !== undefined){
            setFullName(customerInfo.fullName);
            setEmail(customerInfo.email);
            setPhone(customerInfo.phone);
            setAddress(customerInfo.address);
            setCountry(customerInfo.country);
            setTimeout(() => {
                setAdministrativeDivision(customerInfo.administrativeDivision);
            }, 2000); 
            setCity(customerInfo.city);
        }
    },[customerInfo])

    useEffect (()=>{
        if(editCustomerStatus){
            handleCloseEditModal()
            setOpen(true)
            setSnackBar("Edit Customer Successful!")
        }

        if(deleteCustomerStatus){
            handleCloseDeleteCustomerModal() 
            setOpenDelete(true)
            setSnackBar("Delete Customer Successful") 
        }
        
        
    },[editCustomerStatus, deleteCustomerStatus, handleCloseEditModal, handleCloseDeleteCustomerModal])

    
    return(
        <>  
            <SnackBar open={open} handleClose={handleCloseEditSnackBar} message={snackBar} severity={"success"}></SnackBar>
            <SnackBar open={openDelete} handleClose={handleCloseDeleteSnackBar} message={snackBar} severity={"success"}></SnackBar>
            <Modal
                open={openEditModal}
                onClose={handleCloseEditModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h5" component="h2">
                    Edit Customer 
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12} mt ={2}>
                        <TextField value={fullname} onChange={handleName} label="fullname" variant="outlined" sx={{ width: "100%" }}/>
                    </Grid>
                    <Grid item xs={12} mt ={2}>
                        <TextField value={email} onChange={handleEmail} label="email" variant="outlined" sx={{ width: "100%" }}/>
                    </Grid>
                    <Grid item xs={12} mt ={2}>
                        <TextField value={phone} onChange={handlePhone} label="phone" variant="outlined" sx={{ width: "100%" }}/>
                    </Grid>
                    <Grid item xs={12} mt ={2}>
                        <TextField value={address} onChange={handleAddress} label="address" variant="outlined" sx={{ width: "100%" }}/>
                    </Grid>
                    <Grid item xs={12} mt={2}>
                        <FormControl sx={{  minWidth: "100%" }} size="small">
                            <InputLabel sx={{ translate: "-5px" }}>Country</InputLabel>
                            <Select
                                value={country}
                                label="Country"
                                input={<OutlinedInput label="Country" />}
                                onChange={handleChangeCountry}
                                MenuProps={MenuProps}
                            >
                                {countryData.map((element,index) => 
                                    <MenuItem value={element.code} key={index}>
                                        <em>{element.name}</em>
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} mt={2} >
                        <FormControl sx={{  minWidth: "100%" }} size="small">
                            <InputLabel id="demo-select-small" sx={{ translate: "-5px" }}>Adminitrative Division</InputLabel>
                            <Select
                                value={administrativeDivision}
                                label="Adminitrative Division"
                                input={<OutlinedInput label="Adminitrative Division" />}
                                onChange={handleChangeAdministrativeDivision}
                                MenuProps={MenuProps}
                            >
                                {administrativDivisionData.map((element,index) => 
                                    <MenuItem value={element} key={index}>
                                        <em>{element}</em>
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} mt ={2} mb={4}>
                        <TextField value={city} onChange={handleCity} label="city" variant="outlined" sx={{ width: "100%" }}/>
                    </Grid>
                </Grid>
                <button onClick={handleEditCustomer} style={{
                    backgroundColor:"bisque",
                    border:"0px",
                    width:"30%",
                    height:"40px",
                    fontSize: "16px"
                }}>Edit Customer Order</button>
                <button 
                style={{
                    backgroundColor:"black",
                    border:"0px",
                    width:"40%",
                    height:"40px",
                    marginLeft:"15px",
                    fontSize: "16px",
                    color:"white"
                }} onClick={handleCloseEditModal}>Go Back</button>
                </Box>
            </Modal>
            
            <Modal
                open={deleteCustomerModal}
                onClose={handleCloseDeleteCustomerModal}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h5" component="h2">
                    Delete Customer
                </Typography>
                <Typography id="modal-modal-description" sx={{ margin: "20px 0 !important"}}>
                    Do you actually want to delete this Customer?
                </Typography>
                <button  style={{
                    backgroundColor:"bisque",
                    border:"0px",
                    width:"20%",
                    height:"40px",
                    fontSize: "16px"
                }} onClick={handleDeleteCustomer}>Yes</button>
                <button 
                style={{
                    backgroundColor:"black",
                    border:"0px",
                    width:"40%",
                    height:"40px",
                    marginLeft:"15px",
                    fontSize: "16px",
                    color:"white"
                }} onClick={handleCloseDeleteCustomerModal}>No, Go Back</button>
                </Box>
            </Modal>
        </>
    )
}

export default CustomerModal;