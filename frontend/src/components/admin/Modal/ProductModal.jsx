import { Box, Typography , Modal, TextField, Grid, FormControl, InputLabel, Select, MenuItem} from "@mui/material";
import { useEffect, useState} from "react";
import React from "react";
import {  useDispatch, useSelector } from "react-redux";
import { onStartEditProduct, onStartDeleteProduct } from "../../../actions/admin/admin.productAction";
import SnackBar from "../snackbar/snackBar";
import {useDropzone} from "react-dropzone";
import CircularProgress from '@mui/material/CircularProgress';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1200,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "6px",
   
};


const img = {
    height:"20%",
    width: "40%"
 }

const ModalEditProduct =  ({acceptedFiles, setAcceptedFiles, categoryList ,productInfo, openEditModal, handleCloseEdit, openDeleteModal, handleCloseDelete}) => {
    const dispatch = useDispatch();
    const [category, setCategory] = useState(productInfo.type && productInfo.type._id ? productInfo.type.name : '641acdba59a3d1acc1ca554c');
    const [name, setName ] = useState(productInfo.name ? productInfo.name : '');
    const [price, setPrice ] = useState(productInfo.buyPrice ? productInfo.buyPrice : 0);
    const [promotionPrice, setPromotionPrice] = useState(productInfo.promotionPrice ? productInfo.promotionPrice : 0);
    const [quantity, setQuantity] = useState(productInfo.amount ? productInfo.amount : 0);
    const [description, setDescription] = useState(productInfo.description ? productInfo.description : "");
    const {editProductStatus, deleteProductStatus, pendingEditProduct } = useSelector(state => state.adminProductReducer);
    const [open, setOpen] = useState(false);
    const [snackBar, setSnackBar]  = useState("");
    const [selectedImage, setSelectedimage] = useState(-1);
    const [editImage, setEditImage] = useState("");
    const [checkDropImage, setCheckDropImage] = useState("");
    let newProduct = {
        name: name,
        description: description,
        type: category,
        buyPrice: price,
        promotionPrice: promotionPrice,
        amount: quantity
    }
    // dropzone properties 
    const {
        getRootProps, 
        getInputProps
    } = useDropzone({
        accept:{
            'image/png': [],
            'image/jpg': [],
            'image/jpeg': []
        },
        onDrop:  (files) => {
            setAcceptedFiles(
                files.map((file) => Object.assign(file, {
                    preview: URL.createObjectURL(file),
                }))
            );
        },
    })

    //accepted file in dropzone 
    let acceptFileItems = acceptedFiles.map(file => (
        // <li key={file.path}>
        //     {file.path} - {file.size} bytes
        // </li>
        
            <img 
                src={file.preview} 
                alt="accepted"
                key={file.path}
                style={img}
                onLoad={() => {
                    URL.revokeObjectURL(file.preview)
                }}
            />
            ))

    // rejected file in dropzone 
    // const rejectedFileItems = fileRejections.map(({file, errors}) => (
    //     <li key={file.path}>
    //         {file.path}
    //         <ul>
    //             {errors.map( e => (
    //                 <li key={e.code}> {e.message}</li>
    //             ))}
    //         </ul>
    //     </li>
    // ))
    // avoid memory leak
    useEffect(() => {
        return () => acceptedFiles.forEach(file => URL.revokeObjectURL(file.preview))
        
    },[acceptedFiles])    
    //close snack bar
    const handleCloseEditSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setOpen(false);
    };
    

    //When admin change the product name
    const handleName = (event) => {
        setName(event.target.value);
        newProduct.name = event.target.value
    }

    //When admin change the category
    const handleChangeCategory = (event) => {
        setCategory(event.target.value);
        newProduct.type = event.target.value
    }

    //When admin Change the Price
    const handlePrice = (event) => {
        setPrice(event.target.value)
        newProduct.buyPrice = event.target.value
    }

    //When admin change the promotion Price
    const handlePromotionPrice = (event) => {
        setPromotionPrice(event.target.value);
        newProduct.promotionPrice = event.target.value
    }

    //When admin change the quantity
    const handleQuantity = (event) => {
        setQuantity(event.target.value)
        newProduct.amount  = event.target.value
    }

    //When admin change the description
    const handleDescription  = (event) => {
        setDescription(event.target.value)
        newProduct.description = event.target.value
    }
 
    //When admin click edit product
    const handleEditProduct = () => {
        if(selectedImage === -1 && acceptedFiles.length > 0 ){
            setCheckDropImage("You haven't choose edit image!")
            return
        }   
        if(selectedImage !== -1 &&  acceptedFiles.length === 0 ){
            setCheckDropImage("You haven't drop new image!")
            return
        }
        setCheckDropImage("");
        dispatch(onStartEditProduct(newProduct, productInfo._id, editImage, selectedImage, acceptedFiles, productInfo)); 
        setSelectedimage(-1);
        acceptFileItems = [];
    }
    // Pick image handler function
    const handlePickImage = (image, index) => {
        setSelectedimage(index);
        setEditImage(image);
        if(selectedImage === index){
            setSelectedimage(-1)
        }
    }
    // when admin click delete product
    const handleDeleteProduct = () => {
        dispatch(onStartDeleteProduct(productInfo._id));
    }

    useEffect(()=> {
        if(JSON.stringify(productInfo) !== JSON.stringify({})){
            setCategory(productInfo.type._id);
            setName(productInfo.name);
            setDescription(productInfo.description);
            setPrice(productInfo.buyPrice);
            setPromotionPrice(productInfo.promotionPrice);
            setQuantity(productInfo.amount);
        }
    },[productInfo])


    useEffect (()=>{
        
        if(editProductStatus){
            setOpen(true)
            setSnackBar("Edit Product Successful!")
        }

        if(deleteProductStatus){
            setOpen(true)
            setSnackBar("Delete Product Successful!")
        }

    },[editProductStatus, deleteProductStatus])
    useEffect (()=>{
        
        if(editProductStatus){
            handleCloseEdit()
        }

        if(deleteProductStatus){
            handleCloseDelete()

        }

    },[editProductStatus, deleteProductStatus, handleCloseEdit, handleCloseDelete])
    

    // incase product in not load yet
    if(JSON.stringify(productInfo) === JSON.stringify({})){
        return(
            <></>
        )
    } 
    return(
        <React.Fragment>  
            <SnackBar open={open} handleClose={handleCloseEditSnackBar} message={snackBar} severity={"success"}></SnackBar>
            
            {/* Edit Modal */}
            <Modal
                open={openEditModal}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >   
                <Box sx={style}>
                    {
                        pendingEditProduct ? <CircularProgress sx={{color:"rgb(191, 180, 142)", marginLeft: "50%"}}/> :
                        <>
                            <Typography id="modal-modal-title" variant="h4" component="h2">
                                Edit Product 
                            </Typography>
                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <Grid item xs={12} mt = {5}>
                                        <TextField value = {name} onChange={handleName} label="Name" variant="outlined" sx={{ width: "100%" }}/>
                                    </Grid>
                                    <Grid item xs={12} mt={2}>
                                        <FormControl fullWidth>
                                            <InputLabel id="demo-simple-select-label">Catergory</InputLabel>
                                            <Select 
                                                value={category}
                                                label="Category"
                                                onChange={handleChangeCategory}
                                            >
                                                {categoryList.map((element, index)=>{
                                                    return(
                                                        <MenuItem value={element._id} key={index}>{element.name}</MenuItem>
                                                    )
                                                })}
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} mt={2}>
                                        <TextField value={price} onChange={handlePrice} label="Price" variant="outlined" sx={{ width: "100%" }} />
                                    </Grid>
                                    <Grid item xs={12} mt={2}>
                                        <TextField value={promotionPrice} onChange={handlePromotionPrice}  label="Promotion Price" variant="outlined" sx={{ width: "100%" }}/>
                                    </Grid>
                                    <Grid item xs={12} mt={2}>
                                        <TextField value={quantity} onChange={handleQuantity} label="Quantiy" variant="outlined" sx={{ width: "100%" }}/>
                                    </Grid>
                                    <Grid item xs={12} mt={2}>
                                        <textarea value={description} onChange={handleDescription} placeholder="Description" 
                                        style={{ 
                                            padding:"10px",
                                            width: "97.4%" , 
                                            border: "1px solid rgb(191, 180, 142)",
                                            borderRadius: "5px",
                                            minHeight: "60px"
                                            }}> 
                                        </textarea>
                                    </Grid>
                                </Grid>
                                <Grid item xs={6} mt={2}>
                                    <div className="box-update-picture">
                                        <div className="box-select-picture">
                                            {productInfo.imageURL.map((image, index)=>
                                                <img src={image} alt={index} key={index} onClick={() => handlePickImage(image, index)} style={{
                                                    width:"120px",
                                                    height:"140px",
                                                    borderRadius: "5px"
                                                }}
                                                className={selectedImage === index ? 'selected-image' : ""}
                                                ></img>
                                            )} 
                                        </div>
                                        <div {...getRootProps({className:"box-drop-update-picture"})}>
                                            <input {...getInputProps()} />
                                            <p>Drag your picture here, or click to select files</p>
                                            <em>(Only jpeg, jpg ,png images will be accepted)</em>
                                        </div>
                                        {checkDropImage !== "" ? <p style={{color:"red"}}>{checkDropImage}</p> : <></>}
                                        <aside className="droped-file-box">
                                            <div>
                                                <h4>Accepted files</h4>
                                                <ul>{acceptFileItems}</ul>
                                            </div>
                                            
                                        </aside>
                                    </div>    
                                </Grid>
                                <Grid item xs={12} mt={2} sx={{alignItems: "flex-end"}}>
                                    <button onClick={handleEditProduct} className="modal-edit-button">Edit Product</button>
                                    <button onClick={handleCloseEdit} className="modal-delete-button">Go Back</button>
                                </Grid>
                            </Grid>
                        </>
                    }
                </Box>
            </Modal>

            {/* Delete Modal */}
            <Modal
                keepMounted
                open={openDeleteModal}
                onClose={handleCloseDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >   
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h4" component="h2">
                    Edit Product
                </Typography>
                    <Grid container spacing={2}> 
                        <Grid item xs={12} mt={2}>
                            <Typography>Are you sure want to delete this product?</Typography>
                        </Grid>
                        <Grid item xs={12} mt={2} sx={{alignItems: "flex-end"}}>
                            <button onClick={handleDeleteProduct} className="modal-edit-button">Delete the Product</button>
                            <button onClick={handleCloseDelete} className="modal-delete-button">Go Back</button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </React.Fragment>
    )
}

export default ModalEditProduct