import { Box, Typography , Modal, TextField, Grid} from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { onEditCouponData, onDeleteCouponData } from "../../../actions/admin/admin.couponAction";
import SnackBar from "../snackbar/snackBar";
const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
    borderRadius: "6px"
};

export default function CouponModal ({couponData, openEditModal, handleCloseEdit, deleteCoupon, handlecloseDelete}) {
    const dispatch = useDispatch();
    const [couponCode, setCouponCode] = useState(123156);
    const [discount, setDiscount] = useState(0);
    const {editCouponStatus, deleteCouponStatus} = useSelector(state => state.adminCouponReducer);
    const [open, setOpen] = useState(false);   
    const [snackBar, setSnackBar] = useState("");
    //close snack bar
    const handleCloseEditSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
        setOpen(false);
    };


    const newDiscountCode = {   
        voucherCode: "123465",
        discountPercent: "12"
    }   
    const handleOnChangeVoucherCode = (event) => {
        setCouponCode(event.target.value);
        newDiscountCode.voucherCode = event.target.value;
    }

    const handleOnChangeDiscount = (event) => {
        setDiscount(event.target.value);
        newDiscountCode.discountPercent = event.target.value;
    }

    const handleEditProduct = () => {
        dispatch(onEditCouponData(newDiscountCode, couponData._id));
    }

    const handleDeleteCoupon = () => {
        dispatch(onDeleteCouponData(couponData._id));
    }
    useEffect(()=> {
        if(JSON.stringify(couponData) !== JSON.stringify({}) ){
            setCouponCode(couponData.voucherCode)
            setDiscount(couponData.discountPercent)
        }
    },[couponData])

    useEffect(()=> {
        if(editCouponStatus){
            handleCloseEdit()
            setOpen(true)
            setSnackBar("Edit Coupon Successful!")
        }
        if(deleteCouponStatus){
            handlecloseDelete()
            setOpen(true)
            setSnackBar("Delete Coupon Successful!")
    }
    },[editCouponStatus, deleteCouponStatus, handleCloseEdit, handlecloseDelete])
    return(
        <>
            <SnackBar open={open} handleClose={handleCloseEditSnackBar} message={snackBar} severity={"success"}></SnackBar>
            {/* Edit Modal */}
            <Modal
                open={openEditModal}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >   
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h4" component="h2">
                    Edit Product 
                </Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={12} mt={2}>
                            <TextField value={couponCode}  onChange={handleOnChangeVoucherCode}  label="Code" variant="outlined" sx={{ width: "100%" }}/>
                        </Grid>
                        <Grid item xs={12} mt={2}>
                            <TextField value={discount} onChange={handleOnChangeDiscount} label="Discount" variant="outlined" sx={{ width: "100%" }}/>
                        </Grid>
                        <Grid item xs={12} mt={2} sx={{alignItems: "flex-end"}}>
                            <button onClick={handleEditProduct} className="modal-edit-button">Edit Product</button>
                            <button onClick={handleCloseEdit}  className="modal-delete-button">Go Back</button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            {/* Delete Modal */}
            <Modal
                open={deleteCoupon}
                onClose={handlecloseDelete}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                <Typography id="modal-modal-title" variant="h5" component="h2">
                    Delete Coupon
                </Typography>
                <Typography id="modal-modal-description" sx={{ margin: "20px 0 !important"}}>
                    Do you actually want to Delete this Coupon?
                </Typography>
                <button  style={{
                    backgroundColor:"bisque",
                    border:"0px",
                    width:"20%",
                    height:"40px",
                    fontSize: "16px"
                }} onClick={handleDeleteCoupon}>Yes</button>
                <button 
                style={{
                    backgroundColor:"black",
                    border:"0px",
                    width:"40%",
                    height:"40px",
                    marginLeft:"15px",
                    fontSize: "16px",
                    color:"white"
                }} onClick={handlecloseDelete}>No, Go Back</button>
                </Box>
            </Modal>
        </>
    )
}