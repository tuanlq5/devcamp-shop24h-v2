import React from "react"
 

const orderList = React.lazy(() => import("./views/Admin/order/OrderList"));
const productList = React.lazy(() => import("./views/Admin/product/ProductList"));
const addProduct = React.lazy(() => import("./views/Admin/product/NewProduct"));
const customerList = React.lazy(() => import("./views/Admin/customer/CustomerList"));
const couponList = React.lazy(() => import("./views/Admin/coupon/CouponList"));
const newCoupon = React.lazy(() => import("./views/Admin/coupon/NewCoupon"));
const categoryList = React.lazy(() => import("./views/Admin/category/CategoryList"));
const newCategory = React.lazy(() => import("./views/Admin/category/NewCategory"));
const overview = React.lazy(() => import("./views/Admin/dataAnalyze/OverviewData"));

const AdminRouteList = [
    {path:"/", name:"Overview", element: overview },
    {path:"/OrderList", name:"Order List", element: orderList },
    {path:"/ProductList", name:"Product List", element: productList},
    {path:"/AddProduct", name: "Add Product", element: addProduct},
    {path:"/CustomerList", name: "Customer List", element: customerList},
    {path:"/CouponList", name: "Coupon List", element: couponList},
    {path:"/AddCoupon", name: "Add Coupon", element: newCoupon},
    {path:"/CategoryList", name:"Catergories List", element: categoryList},
    {path:"/NewCategory", name:"Add Category", element: newCategory},
]

export default AdminRouteList