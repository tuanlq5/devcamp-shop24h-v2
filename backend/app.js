// khai báo thư viện express 
const { response, request } = require("express");
const express = require ("express");
var cors = require('cors');


// khai báo mongoose 
var mongoose = require('mongoose');
mongoose.set('strictQuery', true);
//khởi tạo app express
const app = express();

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Authorization,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    if (req.method === 'OPTIONS') {
      res.sendStatus(200);
    } else {
      next();
    }
});

//khai báo cổng chạy app
const port= 8000;

//khai báo middleware
const verifyJWT = require('./app/middleware/verifyJWT');

//model
const refreshTokenModel = require("./app/models/refreshToken");
const customerModel = require("./app/models/Customer");
const orderModel = require("./app/models/order");
const orderDetailModel = require("./app/models/orderDetail");
const voucherModel = require("./app/models/voucher");
const productModel = require("./app/models/product");
const productTypeModel = require("./app/models/productType");
const googleAccountModel = require("./app/models/GoogleaAccountInfo");
const CountryModel = require("./app/models/country");
const nonUserACtivities = require("./app/models/nonUserActivities");
const userActivities = require("./app/models/userActivities");
const addProduct = require("./app/models/addProduct");
const removeProduct = require("./app/models/removeProduct");
const successPurchase = require("./app/models/successPurchase");
const couponUse = require("./app/models/couponUse");
const productView = require("./app/models/productView");
const loginData = require("./app/models/loginData");
const logoutData = require("./app/models/logoutData");

//khai báo router 
const productTypeRouter = require("./app/routes/productTypeRouter");
const productRouter = require("./app/routes/productRouter");
const customerRouter = require("./app/routes/customerRouter");
const orderRouter = require("./app/routes/orderRouter")
const orderDetailRouter = require("./app/routes/orderDetailRouter");
const voucherRouter = require("./app/routes/voucherRoutes");
const authRouter = require("./app/routes/authRouter");
const refreshRouter = require("./app/routes/refreshRouter");
const logOutRouter = require("./app/routes/logoutRouter");
const googleAccountRouter = require("./app/routes/googleFireBaseRouter");
const countryRouter = require("./app/routes/countryRouter");
const adminRegisterRouter = require("./app/routes/adminRegisterRouter");
const productCartRouter = require("./app/routes/productCart");
const userActivitiesRouter = require("./app/routes/userActivitiesRouter");
const nonUserACtivitiesRouter = require("./app/routes/nonUserActivities");
const productViewRouter = require("./app/routes/productViewRouter");
const successPurchaseRouter = require("./app//routes/successPurchase");
const couponUseRouter = require("./app/routes/couponUseRouter");
const cookieParser = require("cookie-parser");

//cấu hình request đọc 
app.use(express.json());

//middleware for cookies
app.use(cookieParser());

app.post("/post", (req, res) => {
    console.log("Connected to React");
    res.redirect("/");
  });
//kết nối với mongoDB
mongoose.connect('mongodb://127.0.0.1:27017/CRUD_shop24h', function(error){
    if(error) throw error;
    console.log('Successfully connected');
});
// App sử dụng router
app.use(cors())
app.use("/", adminRegisterRouter);
app.use("/", productTypeRouter);
app.use("/", productRouter);
app.use("/", orderRouter);
app.use("/", orderDetailRouter);
app.use("/", voucherRouter);
app.use("/", authRouter);
app.use("/", refreshRouter);
app.use("/", logOutRouter);
app.use("/", googleAccountRouter);
app.use("/", countryRouter);
app.use("/", customerRouter);
app.use("/", productCartRouter);
app.use("/", userActivitiesRouter);
app.use("/", nonUserACtivitiesRouter)
app.use("/", productViewRouter);
app.use("/", successPurchaseRouter);
app.use("/", couponUseRouter);
// chạy app trên cổng
app.listen(port,()=>{
    console.log("App listening on port :" , port)
})