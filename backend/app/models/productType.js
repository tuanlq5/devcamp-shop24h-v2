//ipmort thư viện mongoose
const mongoose = require ("mongoose");
const moment = require('moment-timezone');
// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const productTypeSchema = new Schema({
    name:{
        type: String,
        require: true,
        unique:true
    },
    avatar:{
        type: String
    },
    description:{
        type: String,
    },
    _delete:{
        type: Boolean,
        default: false
    },
    percentageOrder:{
        type: String
    },
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps:true
});
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Product_Type", productTypeSchema);