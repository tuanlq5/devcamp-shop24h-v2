//ipmort thư viện mongoose
const mongoose = require("mongoose");
const productType = require("./productType");
const moment = require("moment-timezone");
// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const customerSchema = new Schema(
  {
    fullName: {
      type: String,
      require: true,
    },
    phone: {
      type: String,
      require: true,
    },
    email: {
      type: String,
      require: true,
      unique: true,
    },
    password: {
      type: String,
      require: true,
    },
    address: {
      type: String,
      default: "",
    },
    city: {
      type: String,
      default: "",
    },
    country: {
      type: String,
      default: "",
    },
    administrativeDivision: {
      type: String,
      default: "",
    },
    refreshToken: {
      type: mongoose.Types.ObjectId,
      ref: "refreshToken",
    },
    orders: [
      {
        type: mongoose.Types.ObjectId,
        ref: "Order",
      },
    ],
    zipCode: {
      type: String,
    },
    role: {
      type: Object,
      default: {
        customer: 2222,
      },
    },
    _Delete:{
      type: Boolean,
      default: false
    },
    createdAt: {
      type: String,
      default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format(),
    },
    updatedAt: {
      type: String,
      default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format(),
    },
  },
  {
    timestamps: true,
  }
);
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Customer", customerSchema);
