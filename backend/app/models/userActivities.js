//import thư viện mongoose 
const mongoose = require("mongoose");
const moment  = require ('moment-timezone');
const Schema = mongoose.Schema;

//Khởi tạo Schema
const userActivitiesSchema = new Schema({
    userID: {
        type: mongoose.Types.ObjectId,
        ref: "Customer"
    },
    cartActivities: {
        addProductToCart:[{
            type: mongoose.Types.ObjectId,
            ref: "addProduct"
        }],
        removeProductCart: [{
            type: mongoose.Types.ObjectId,
            ref: "removeProduct"
        }]
    },
    couponUse:{
        type: mongoose.Types.ObjectId,
        ref: "couponUse"
    },
    productView:[{
        type: mongoose.Types.ObjectId,
        ref: "productView"
    }],
    successPurchase: {
        type: mongoose.Types.ObjectId,
        ref: "successPurchase"
    },
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps: true
})

module.exports =  mongoose.model("userActivities", userActivitiesSchema)