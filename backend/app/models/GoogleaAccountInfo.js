const mongoose = require('mongoose');
const moment = require('moment-timezone');

const Schema = mongoose.Schema;


const googleAccount = new Schema({
    fullName:{
        type: String,
        require: true,
    },
    phone:{
        type: Number,
        require: true,
    },
    email:{
        type: String,
        require: true,
    },
    address:{
        type: String,
        default:"",
        require:true
    },
    city:{
        type: String,
        default:"",
        require:true
    },
    country:{
        type: String,
        default:"",
        require:true
    },
    administrativeDivision:{
        type: String,
        default: "",
        require:true
    } ,
    zipcode:{
        type: String
    },
    order:{
        type: mongoose.Types.ObjectId,
        ref: "Order"
    }
    ,
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("googleAccount",googleAccount)