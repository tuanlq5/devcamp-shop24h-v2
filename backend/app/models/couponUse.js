const mongoose = require("mongoose");
const moment = require("moment-timezone");
const Schema = mongoose.Schema;

const couponUse = new Schema({
    order: {
        type: mongoose.Types.ObjectId,
        ref: "Order"
    },
    coupon:{
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps: true
});

module.exports = mongoose.model("couponUse", couponUse);