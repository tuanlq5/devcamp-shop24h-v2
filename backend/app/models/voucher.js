//import thư viện mongoose
const mongoose = require("mongoose");
const moment = require('moment-timezone');
const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    voucherCode:{
        type: String,
        require: true,
        unique: true
    },
    discountPercent: {
        type: Number,
        require: true,
    },
    _Delete:{
        type: Boolean,
        default: false
    }
    ,    
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
}, {
    timestamps: true,
    versionKey: false
});

module.exports = mongoose.model("Voucher", voucherSchema);