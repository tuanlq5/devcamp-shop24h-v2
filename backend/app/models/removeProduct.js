const mongoose = require("mongoose");
const moment = require("moment-timezone");
const Schema = mongoose.Schema;

const removeProduct = new Schema({
    product:{
        type: mongoose.Types.ObjectId,
        ref:"Product"
    },
    quantity: {
        type: Number,
        require: true
    },
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps: true
});

module.exports = mongoose.model("removeProduct", removeProduct);