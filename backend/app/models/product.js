//ipmort thư viện mongoose
const mongoose = require ("mongoose");
const productType = require("./productType");
const moment = require('moment-timezone');
// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const productSchema = new Schema({
    name:{
        type: String,
        require: true,
        unique:true
    },
    description:{
        type: String,
    },
    type:{
        type: mongoose.Types.ObjectId,
        ref: "Product_Type",
        require: true
    },
    imageURL:[{
        type: String,
        require: true,
    }],
    buyPrice:{
        type: Number,
        require: true
    },
    promotionPrice:{
        type: Number,
        require: true
    },
    amount:{
        type: Number,
        default: 0
    },
    _Delete:{
        type: Boolean,
        default: false
    }
    ,
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps:true
});
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Product", productSchema);