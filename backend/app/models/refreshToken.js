//ipmort thư viện mongoose
const mongoose = require ("mongoose");
const moment = require('moment-timezone');
// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const refreshTokenSchema = new Schema({
   token:{
    type: String,
    require: true,
   },
   customer:{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Customer",
   },
   createdAt:{
    type: String,
    default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps:true
});
//biên dịch course model từ courseSchema
module.exports = mongoose.model("refreshToken", refreshTokenSchema);