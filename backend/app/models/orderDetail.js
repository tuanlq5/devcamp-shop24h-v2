//import thư viện mongoose
const { default: mongoose } = require("mongoose");
const product = require("./product");
const moment = require('moment-timezone');

// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo instance courseSchema từ class Schema
const orderDetailSchema = new Schema({
    product:{
        type: mongoose.Types.ObjectId,
        ref: "Product"
    },
    quantity:{
            type: Number,
            default: 0
    },
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
 },{
     timestamps:true
 });
//biên dịch course model từ courseSchema
module.exports = mongoose.model("OrderDetail", orderDetailSchema);