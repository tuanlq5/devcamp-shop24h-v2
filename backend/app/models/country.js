const mongoose  = require("mongoose");
const moment = require('moment-timezone');

const Schema = mongoose.Schema;

const Country = new Schema({
    name: {
        type: String
    },
    code: {
        type: String
    },
    administrativeDivision:{
        type: Array,
        default: []
    }
})

module.exports = mongoose.model("Country", Country);