const mongoose = require("mongoose");
const moment = require("moment-timezone");
const Schema = mongoose.Schema;

const successPurchase = new Schema({
    order: {
        type: mongoose.Types.ObjectId,
        ref: "Order"
    },
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps: true
});

module.exports = mongoose.model("successPurchase", successPurchase);