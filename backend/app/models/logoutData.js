const mongoose = require("mongoose");
const moment = require("moment-timezone");
const Schema = mongoose.Schema;

const logoutData = new Schema({
    customerAccount: {
        type: mongoose.Types.ObjectId,
        ref: "Customer"
    },
    googleAccount:{
        type: mongoose.Types.ObjectId,
        ref: "googleAccount"
    },
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
},{
    timestamps: true
});

module.exports = mongoose.model("logoutData", logoutData);