//import thư viện mongoose
const { default: mongoose } = require("mongoose");

// class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

const moment = require('moment-timezone');


//khởi tạo instance courseSchema từ class Schema
const orderSchema = new Schema({
    orderDate:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    shippedDate:{
        type: Date,
    },
    note:{
        type: String,
    },
    customer:{
        type: mongoose.Types.ObjectId,
        ref: "Customer"
    }
    ,
    orderDetail:[{
        type: mongoose.Types.ObjectId,
        ref: "OrderDetail"
    }],
    cost: {
        type: Number,
        default: 0 
    },
    status: {
        type: String,
        require: true
    },
    uid:{
        type: String,
    },
    couponUse:{
        type: mongoose.Types.ObjectId,
        ref: "Voucher"
    },
    _Delete:{
        type: Boolean,
        default: false
    }
    ,
    createdAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    },
    updatedAt:{
        type: String,
        default: moment.tz(Date.now(), "Asia/Ho_Chi_Minh").format()
    }
 },{
     timestamps:true
 });
//biên dịch course model từ courseSchema
module.exports = mongoose.model("Order", orderSchema);