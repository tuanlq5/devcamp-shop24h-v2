// The Concept of Authorize is giving the authority to specific role to access a specific routes
/** Specific user will have many role
 *  each role have different access authority
 *  the more role they got, the more access they can have
 *  this fundtion will check wether or not which user allowed can access the router base on the parameter 
 *  using include and find
 *  method include will check wether the param(alowed role) is exist in customer role return  array of true and false
 *  find will check the array wether or not have the value true so it can given a boolean result 
 */

const verifyRole = (...allowedRoles) => {
  return (req, res, next) => {

    if (!req?.roles) return res.sendStatus(401);
    const rolesArray = [...allowedRoles];
    const result = req.roles.map((role) =>
      rolesArray.includes(role)).find((value) =>  value === true);
    ;
    if (!result) return res.sendStatus(401);
    next();
  };
};

module.exports = {
  verifyRole,
};
