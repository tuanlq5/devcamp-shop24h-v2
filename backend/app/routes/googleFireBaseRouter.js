const express = require("express");
const  router = express.Router();

const googleFireBaseController = require("../controller/googleFireBasecontroller");

router.post("/LoginGoogleAccount", googleFireBaseController.authetocationGoogleAccount);

router.post("/addInfoGoogleAccount", googleFireBaseController.sendCustomerInfoWithGoogleAccount);

module.exports = router;