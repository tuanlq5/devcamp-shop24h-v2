const express = require("express");
const cors = require("cors");
const counponUseController = require("../controller/couponUse");
const corsCondition = require("../config/Cross-origin").corsCondition;
const router = express.Router();


router.get("/getCouponUse", cors(corsCondition), counponUseController.createCouponUse);

module.exports = router;


