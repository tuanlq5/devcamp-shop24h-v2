// khai báo thư viện express js
const express = require("express");
const cors = require("cors");
//khai báo router app
const router = express.Router();

//verify  middleware JWT
const JWT = require("../middleware/verifyJWT").verifyJWT;

//middleware verify role
const verifyRole = require("../middleware/verifyRole");

//import product controller
const customerController = require("../controller/customerController");

//import Role list
const ROLE_LIST = require("../config/role_list").ROLE_LIST;


// Allow requests from any origin with credentials
const corsCondition = {
  origin: "http://localhost:3000",
  credentials: true,
};

//import verifyJWT
const verifyJWT = require("../middleware/verifyJWT").verifyJWT; 


router.post("/customer", cors(corsCondition), cors(corsCondition), customerController.createCustomer);

router.get( "/customer", cors(corsCondition),verifyJWT , verifyRole.verifyRole(ROLE_LIST.admin), customerController.getAllCustomer);

router.get("/customer/:customerId", cors(corsCondition), customerController.getCustomerByID);

router.get("/findCustomerEmail/:email", cors(corsCondition),verifyJWT , customerController.getCustomerByEmail);

router.put("/customer/:customerId", cors(corsCondition), verifyJWT, customerController.updateCustomerById);

router.delete("/:customerId", cors(corsCondition), customerController.deleteCustomerById);

router.get("/getCustomerWithPanigation", cors(corsCondition), verifyJWT, verifyRole.verifyRole(ROLE_LIST.admin), customerController.getCustomerWithPanigation);

router.put("/softDeleteCustomer/:customerId",  cors(corsCondition), verifyJWT, verifyRole.verifyRole(ROLE_LIST.admin), customerController.softDeleteCustomerById);


module.exports = router;
