const express = require("express");
const cors = require("cors");

//khai báo router app
const router  = express.Router();

//import verifyJWT
const verifyJWT = require("../middleware/verifyJWT").verifyJWT; 

//import verivyRole
const verifyRole = require("../middleware/verifyRole").verifyRole;

//import role list 
const roleList = require("../config/role_list").ROLE_LIST;

//import controller
const voucherController = require("../controller/voucherController");

//import cors Condition
const corsCondition =  require("../config/Cross-origin").corsCondition;



router.get("/voucher", cors(corsCondition), verifyJWT ,verifyRole(roleList.admin), voucherController.getAllVoucher);

router.post("/voucher", cors(corsCondition), verifyJWT ,verifyRole(roleList.admin), voucherController.createVoucher);

router.get("/getVoucherbyCode/:voucherCode", cors(corsCondition),  voucherController.getVoucherByVoucherCode);

router.get("/getVoucherWithPanigation", cors(corsCondition), verifyJWT ,verifyRole(roleList.admin), voucherController.getVoucherWithPanigation);

router.put("/voucher/:voucherId", cors(corsCondition), verifyJWT, verifyRole(roleList.admin), voucherController.updateVoucherCode);

router.put("/softDeleteVoucher/:voucherId", cors(corsCondition), verifyJWT, verifyRole(roleList.admin), voucherController.softDeleteVoucher);

module.exports = router;
