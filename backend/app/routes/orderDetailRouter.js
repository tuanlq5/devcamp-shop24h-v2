// khai báo thư viện express js
const { response } = require("express");
const express = require("express");
const cors = require("cors");
//khai báo router app
const router = express.Router();
//import product controller
const orderDetailController = require("../controller/orderDetailController");

router.post("/orderDetail", cors(), orderDetailController.createOrderDetail);

router.get("/orderDetail", orderDetailController.getAllOrderDetail);

router.get("/orderDetail/:orderDetailId", orderDetailController.getOrderDetailByID);

router.get("/order/:orderId/orderDetail", orderDetailController.getAllOrderDetailOfOrder);

router.put("/orderDetail/:orderDetailId", orderDetailController.updateOrderDetailByID);

router.delete("/order/:orderId/orderDetail/:orderDetailId", orderDetailController.deleteOrderByID);

module.exports = router;
