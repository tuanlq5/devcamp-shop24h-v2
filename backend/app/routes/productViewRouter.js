const express = require("express");
const cors = require("cors");
const corsCondition = require("../config/Cross-origin").corsCondition;
const router = express.Router();
const productViewController = require("../controller/productView");

router.post("/createProductView/:userId",cors(corsCondition), productViewController.createProductView);

module.exports = router;