const express = require("express");
const cors = require("cors");
const corsCondition = require("../config/Cross-origin").corsCondition;
const nonUserActivities = require("../controller/nonUserActivities");
const router = express.Router();

router.post("/nonUserACtivities", cors(corsCondition), nonUserActivities.createNonUserActivities);

module.exports = router;