// khai báo thư viện express js
const { response } = require("express");
const express = require("express");
const cors = require("cors");

//khai báo router app
const router = express.Router();

//import product controller
const productController = require("../controller/productController");

// origin optio
const origin  = require("../config/Cross-origin").corsCondition;

//import middleware
const productMiddleware = require("../middleware/productMiddleware");

//import verivyRole
const verifyRole = require("../middleware/verifyRole").verifyRole;

// upload image middle ware
const uploadCloud = require("../middleware/uploadImage");

//import role list 
const roleList = require("../config/role_list").ROLE_LIST;


//import verifyJWT
const verifyJWT = require("../middleware/verifyJWT").verifyJWT; 

router.post("/product", cors(origin), verifyJWT, verifyRole(roleList.admin), productController.createProduct);

router.get("/filterProductName", cors(origin), productController.getProductWithFilterName);

router.get("/filterProductPrice", cors(origin), productController.getProductWithFilterPrice);

router.get("/filterProductProductType", cors(origin), productController.getProductWithFilterProductType);

router.get("/getProductWithpanigation", cors(origin), productController.getProductInPanigation);

router.get("/getProductFilter", cors(origin), productController.getProductEveryFilterOption);

router.get("/product", cors(origin), productMiddleware.GetProductMiddleware , productController.getProductWithLimit);

router.get("/product/:productId", cors(origin), productController.getProductByID);

router.put("/product/:productId", cors(origin), verifyJWT, verifyRole(roleList.admin), productController.updateProductById);

router.delete("/product/:productId", cors(origin), verifyJWT, verifyRole(roleList.admin), productController.deleteProductById);

router.put("/softDeleteProduct/:productId", cors(origin), verifyJWT, verifyRole(roleList.admin),  productController.softDeleteProduct);

router.post("/postImageToCloudinary", uploadCloud.single("image"), productController.uploadImage);

router.post("/deleteCloudinaryImage", cors(origin), productController.deleteCloudinaryProductImage);

module.exports = router;