const express = require("express");
const cors = require("cors");
const router = express.Router();

const corsCondition = {
    origin: "http://localhost:3000",
    credentials: true
} 
//import controller
const logoutController = require("../controller/logOutController");

router.get("/customerLogout", cors(corsCondition) , logoutController.handleLogoutCustomer);

module.exports = router;