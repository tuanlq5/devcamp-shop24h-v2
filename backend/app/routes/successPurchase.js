const express = require("express");
const router = express.Router();
const cors = require("cors");
const corsCondition = require("../config/Cross-origin").corsCondition;
const successPurchase = require("../controller/successPurchase");
const { models } = require("mongoose");

router.get("/getSuccessPurchase", cors(corsCondition), successPurchase.takeListSuccessPurchase);

router.post("/createExistSuccessPurchase", cors(corsCondition),  successPurchase.creatAllExistSuccessPurchase);

module.exports = router;