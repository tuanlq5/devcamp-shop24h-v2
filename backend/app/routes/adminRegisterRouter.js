const express = require("express");
const router = express.Router();
const cors = require("cors");

const crossOrigin = require("../config/Cross-origin").corsCondition;

const AdminRegisterController = require("../controller/AdminRegister");

router.post("/AdminRegister", cors(crossOrigin), AdminRegisterController.createAdminAccount);

module.exports = router