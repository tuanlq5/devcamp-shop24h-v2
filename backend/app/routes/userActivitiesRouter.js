const express =  require("express");
const router = express.Router();
const cors = require("cors");
const corsCondition = require("../config/Cross-origin").corsCondition;
const userActivities = require("../controller/userActivities");
const verifyRole = require("../middleware/verifyRole").verifyRole;
const verifyJWT = require("../middleware/verifyJWT").verifyJWT;
const roleList = require("../config/role_list").ROLE_LIST;

router.post("/addUserActivities", cors(corsCondition), userActivities.addActivitiesToAllAccount);

router.get("/getAllUserActivities", cors(corsCondition), verifyJWT, verifyRole(roleList.admin), userActivities.getAllUserAcitvities);


module.exports = router;
