// khai báo thư viện express js
const express = require("express");
const cors = require("cors");

//khai báo router app
const router = express.Router();

// Import product middleware
const productTypeMiddleware = require("../middleware/productTypeMiddleware");

//import product controller
const productTypeController = require("../controller/productTypeController")

const verifyRole = require("../middleware/verifyRole").verifyRole;

const verifyJWT = require("../middleware/verifyJWT").verifyJWT;

const role_list = require("../config/role_list").ROLE_LIST;

const corsCondition = require("../config/Cross-origin").corsCondition;

router.post("/productType",  cors(corsCondition) , productTypeController.createProductType);

router.get("/productType", cors(corsCondition),verifyJWT, verifyRole(role_list.admin), productTypeController.getAllProductType);

router.get("/productType/:productTypeId", cors(corsCondition), productTypeController.getProductTypeByID );

router.put("/productType/:productTypeId", cors(corsCondition), verifyJWT, verifyRole(role_list.admin), productTypeController.updateProductTypeById);

router.delete("/productType/:productTypeId", cors(corsCondition), verifyJWT, verifyRole(role_list.admin), productTypeController.deleteProductTypeById);

router.get("/getCategoryRatioInYear", cors(corsCondition), verifyJWT, verifyRole(role_list.admin), productTypeController.createCategoryPercentageRatioInYear);

router.get("/productTypeWithPanigation", cors(corsCondition), verifyJWT, verifyRole(role_list.admin), productTypeController.getProductTypeWithPanigation);

router.put("/softDeleteProductType/:productTypeId", cors(corsCondition), verifyJWT, verifyRole(role_list.admin), productTypeController.softDeleteProductTypeById);

module.exports = router;