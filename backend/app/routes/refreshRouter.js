const express = require("express");
const router = express.Router();
const cors = require("cors")

const corsCondition= {
    origin: "http://localhost:3000",
    credentials: true,
}
//import controller
const refreshController = require("../controller/refreshTokenController");

router.post("/refresh" ,cors(corsCondition) , refreshController.handleRefreshToken);

module.exports = router;