const express  = require("express");
const cors = require("cors");
const router = express.Router();

const countryController = require("../controller/countryController");


router.get("/country",cors(),countryController.getCountryData);

module.exports = router;
