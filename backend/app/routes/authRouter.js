const express = require("express");
const router = express.Router();
const cors = require("cors");


// Allow requests from any origin with credentials
const corsOptions = {
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,
    origin: "http://localhost:3000",
};
  
//import controller
const authController = require("../controller/authController");


router.post("/customerLogin", cors(corsOptions) ,authController.handleLoginCustomer);


module.exports = router;