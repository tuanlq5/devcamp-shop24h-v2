//khai báo router app
const express = require("express");
const router = express.Router();
const cors = require("cors")

const productCart = require("../controller/productCart");

const corsCondition = require("../config/Cross-origin").corsCondition;

router.post("/addProductCart/:userID", cors(corsCondition), productCart.addProductCart);

router.post("/removeProductCart/:userID", cors(corsCondition), productCart.removeProductCart);

router.post("/addProductCartNonUser",cors(corsCondition), productCart.addProductCartNonUserActivites);

router.post("/removeProdcutCartNonUser", cors(corsCondition), productCart.removeProductCartNonUserActivites);

module.exports = router;