// khai báo thư viện express js
const { response } = require("express");
const express = require("express");

//khai báo router app
const router = express.Router();
const cors = require("cors")

//import product controller
const orderController = require("../controller/orderController");

//import verifyJWT
const verifyJWT = require("../middleware/verifyJWT").verifyJWT; 

//import verifyRole
const verifyRole = require("../middleware/verifyRole").verifyRole;

//import cors Condition
const corsCondition =  require("../config/Cross-origin").corsCondition;

//import role list 
const roleList = require("../config/role_list").ROLE_LIST;

router.post("/customer/order", orderController.createOrderOfCustomer);

router.post("/orderGoogleAccount", orderController.createOrderForGoogleAccount);

router.get("/order",cors(corsCondition), verifyJWT ,verifyRole(roleList.admin), orderController.getOrderWithPanigate);

router.get("/customer/:customerId/order", orderController.getAllOrderOfCustomer);

router.get("/order/:orderId", orderController.getOrderById);

router.put("/order/:orderId", orderController.updateOrderByID);

router.delete("/customer/:customerId/order/:orderId", orderController.deleteOrderByID)

router.put("/orderStatus", orderController.changeOrderStatus);

module.exports = router;
