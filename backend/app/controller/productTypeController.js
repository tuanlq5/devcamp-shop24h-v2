//import thư viện mongoose
const mongoose = require("mongoose");

//import Review Model
const productTypeModel = require("../models/productType");
const orderModel = require("../models/order");
const order = require("../models/order");
const productType = require("../models/productType");

const createProductType = (request, response) =>{
     // B1: Chuẩn bị dữ liệu
     const body = request.body;
     // console.log(body)
        //   {
        //     "bodyName": "Panadon",
        //     "bodyDescription":"Trị đau đầu" 
        //  }
 
     // B2: Validate dữ liệu
     if(!body.bodyName){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Name không hợp lệ" 
        })
    }
 
     // B3: Thao tác với cơ sở dữ liệu
     const newProductType = {
        name: body.bodyName,
        description:  body.bodyDescription
     }
 
     productTypeModel.create(newProductType, (error, data) => {
         if (error) {
             return response.status(500).json({
                 status: "Internal server error",
                 message: error.message
             })
         }  
         return response.status(201).json({
            status: "Create product type: Successfull",
            data: data
        })
     })
}
const getAllProductType  = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    productTypeModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all productType: Successfull",
            data: data
        })
    })
}
const getProductTypeByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const productTypeID =  request.params.productTypeId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeID)){
     return response.status(400).json({
             status: "Bad request",
             message: "productTypeId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     productTypeModel.findById(productTypeID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail productType  successfull",
             data: data
             })
         }
     )  
}
const updateProductTypeById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const productTypeID =  request.params.productTypeId;
    const body = request.body;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeID)){
        return response.status(400).json({
                status: "Bad request",
                message: "ProductTypeId không hợp lệ" 
            })
        }
    
    if(body.name.trim() === ""){
        return response.status(400).json({
            status: "Bad request",
            message: "Name không hợp lệ"
        })
    }   
    
    let updateProductType = {
        name: body.name,
        description:  body.description,
        avatar: body.avatar
    }
    if(body.avatar === ""){
        updateProductType = {
            name: body.name,
            description:  body.description,
        }
    }
    //bước 3: gọi model tạo dữ liêu
    productTypeModel.findByIdAndUpdate(productTypeID,updateProductType,{new: true},(error,data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update productType  successfull",
            data: data
            })
        }
    )  
}
const deleteProductTypeById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const productTypeID = request.params.productTypeId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductType ID không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    productTypeModel.findByIdAndDelete(productTypeID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete product Type successfully"
        })
    })
}
const softDeleteProductTypeById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const productTypeID = request.params.productTypeId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productTypeID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "ProductType ID không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    productTypeModel.findByIdAndUpdate(productTypeID,{_delete: true});
}

const createCategoryPercentageRatioInYear = async(req, res) => {
    try {
        const order = await orderModel.find({status:"Confirmed"}).populate("orderDetail").populate( 
        {   
            path:"orderDetail",
            populate:{
                path:"product",
                    populate:{
                        path:"type"
                    }
                }
            }
        );

        // create product type list
        let productTypeList = [];
        for(let i = 0 ; i <  order.length; i++){
            for(let j = 0 ; j < order[i].orderDetail.length; j ++){
                productTypeList.push(order[i].orderDetail[j].product.type.name);
            }
        }

        let categoryList = {};
        //create a object container array of category and number of it's order  
        productTypeList.forEach(element => {
            categoryList[element] = (categoryList[element] || 0) + 1;
        });
        // separate it to array (category name and order number) 
        const labelCategory = Object.keys(categoryList);
        const categoryRatio = Object.values(categoryList);

        // function find 3 highest number in array
        function findIndicesOfMax(inp, count) {
            var outp = new Array();
            for (var i = 0; i < inp.length; i++) {
                outp.push(i);
                if (outp.length > count) {
                    outp.sort(function(a, b) { return inp[b] - inp[a]; });
                    outp.pop();
                }
            }
            return outp;
        }

        // 3 index of category have highest order 
        var threeHighestOrderCategoryIndex = findIndicesOfMax(categoryRatio, 3);
        
        // number of orders
        const orderNumber = categoryRatio.reduce((partialSum, a) => partialSum + a, 0);
        let topThreeCategory = [];
        for(let i = 0; i< threeHighestOrderCategoryIndex.length; i ++){
            
            let category = await productTypeModel.findOne({name: labelCategory[threeHighestOrderCategoryIndex[i]]})
            
            category.percentageOrder = Math.round(((categoryRatio[threeHighestOrderCategoryIndex[i]]/orderNumber)*100));

            topThreeCategory.push(category);
        }

        res.status(200).json(
            [labelCategory, categoryRatio, topThreeCategory]
        );
    } catch (error) {
        console.log(error)
        res.sendStatus(500)
    }
}

const getProductTypeWithPanigation = async(req, res) => {
    try {
        const skipPage = req.query.skipPage;
        // console.log("Hello")
        const limit = req.query.limit;
        // if(skipPage === undefined){
        //     skip = 0
        // }
        // if(limit === undefined){
        //     limit = 0
        // }
        // if(!skipPage){
        //     return response.status(400).json({
        //         status:"Bad request",
        //         message:"Skip page is not defined!"
        //     })
        // }
        // if(!limit){
        //     return response.status(400).json({
        //         status: "Bad request",
        //         message: "Limit product is not defined!"
        //     })
        // }
        const data = await productTypeModel.find().skip(skipPage*limit).limit(limit);
        const dataLength = await productTypeModel.find();
        return res.status(200).json(
            {
                productType: data,
                length: dataLength.length
            }
        )    
    } catch (error) {
        // console.log(error);
        return res.sendStatus(500);
    }
}
module.exports = {
    createProductType,
    getAllProductType,
    getProductTypeByID,
    updateProductTypeById,
    deleteProductTypeById,
    createCategoryPercentageRatioInYear,
    getProductTypeWithPanigation,
    softDeleteProductTypeById
}