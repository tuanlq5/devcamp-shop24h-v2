//import thư viện mongoose
const { response } = require("express");
const mongoose = require("mongoose");
const path = require("path");
const cloudinary  = require('cloudinary');
require('dotenv').config()

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_NAME,
    api_key: process.env.CLOUDINARY_KEY,
    api_secret: process.env.CLOUDINARY_SECRET
})

//import Review Model
const productModel = require("../models/product");



//call back function create product 
const createProduct = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "bodyName": "Thuốc",
    //     "bodyDescription": "Thuốc trị đau đầu",
    //     "bodyType": "6375edad2bd7724ea38ce201",
    //     "bodyImageURL": "https://i-cf65.ch-static.com/content/dam/cf-consumer-healthcare/panadol/vi_vn/vietnamproduct/panadol_regular_pack_shot_blue/product_detail/Desktop-455x455.png?auto=format",
    //     "bodyBuyPrice" : 60000,
    //     "bodyPromotionPrice": 56000,
    //     "bodyAmount": 60 
    // }
    //B2: validate dữ liệu
    if(!body.name){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Name không hợp lệ" 
        })
    }
    if(!body.type){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Type không hợp lệ" 
        })
    }
    if(!body.imageURL){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Image URL không hợp lệ" 
        })
    }
    if(!body.buyPrice){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "BuyPrice không hợp lệ" 
        })
    }
    if(!body.promotionPrice){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "promotionPrice không hợp lệ" 
        })
    }
    //B3: thao tác cơ sở dữ liệu
    const newProduct = {
        name: body.name,
        description: body.discription,
        type: body.type,
        imageURL: body.imageURL,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount
    }
    productModel.create(newProduct, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }  
        return response.status(201).json({
           status: "Create product : Successfull",
           data: data
       })
    })
}
const getAllProduct = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    productModel.find().limit(8).exec((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all product: Successfull",
            data: data
        })
    })
}
const getProductByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const productID =  request.params.productId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productID)){
     return response.status(400).json({
             status: "Bad request",
             message: "productTypeId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     productModel.findById(productID,(error,data) =>{
         return response.status(200).json({
             status: "Get detail product  successfull",
             data: data
             })
         }
     )  
 }
const updateProductById =  (request, response) =>{
// bước 1: chuẩn bị dữ liệu
const productID =  request.params.productId;
const body = request.body;
//bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productID)){
    return response.status(400).json({
            status: "Bad request",
            message: "ProductId không hợp lệ" 
        })
    } 
    if(!mongoose.Types.ObjectId.isValid(body.type)){
        return response.status(400).json({
                status: "Bad request",
                message: "type không hợp lệ" 
            })
        }  
    if(body.name !== undefined && body.name.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Name không hợp lệ" 
        })
    }
    if(body.type !== undefined && body.type.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Type không hợp lệ" 
        })
    }
   
    if(body.buyPrice !== undefined && (isNaN(body.buyPrice) || body.buyPrice < 0 || body.buyPrice === "")){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "BuyPrice không hợp lệ" 
        })
    }

    if(body.promotionPrice !== undefined && (isNaN(body.promotionPrice) || body.promotionPrice < 0 || body.promotionPrice === "")){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "promotionPrice không hợp lệ" 
        })
    }

    const updateProduct = {
        name: body.name,
        description: body.discription,
        type: body.type,
        buyPrice: body.buyPrice,
        promotionPrice: body.promotionPrice,
        amount: body.amount,
        imageURL: body.imageURL
    }
    //bước 3: gọi model tạo dữ liêu
    productModel.findByIdAndUpdate(productID, updateProduct,{new: true},(error,data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update product  successfull",
            data: data
            })
        }
    )  
}
const deleteProductById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const productID = request.params.productId;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(productID)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Product ID không hợp lệ"
        })
    } 
    //B3:  Gọi model tạo dữ liệu
    productModel.findByIdAndDelete(productID, (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(204).json({
            status: "Delete product successfully"
        })
    })
}
const getProductWithLimit = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    const limit = request.query.limit;
    //Bước2: kiểm tra dữ liệu
    if( limit < 0){
        return response.status(400).json({
            status:"Bad request",
            message: "Limit is not valid!"
        })
    }
    //B3: Lấy dữ liệu trả về 
    if(limit !== undefined){
        productModel.find().limit(limit).exec((error, data) => {
            if(error){
                return response.status(500).json({
                    status: "Internal server error!",
                    message: error.message
                })
            }
            return response.status(201).json({
                status: "Get Product successfull!",
                data: data,
                length: data.length
            })
        })
    }else{
        productModel.find().exec((error, data) => {
            if(error){
                return response.status(500).json({
                    status: "Internal server error!",
                    message: error.message
                })
            }
            return response.status(201).json({
                status: "Get Product successfull!",
                data: data
            })
        })
    }
}
const getProductWithFilterName =  (request, response) => {
    //B1: Chuẩn bị dữ liệu
    const productName = request.query.productName;
    //B2:kiểm tra dữ liệu
    if( productName === undefined){
        return response.status(400).json({
            status:"Bad request",
            message: "productName is not valid!"
        })
    }
    const condition = {
        name: {$regex: productName}
    }
    //B3: Thao tác với cơ sở dữ liệu
    productModel.find(condition).exec((error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error!",
                message: error.message
            })
        } 
        return response.status(200).json({
            status:"Get Product Filter By Name Successful!",
            data: data
        })
    })
} 
const getProductWithFilterPrice = (request,response) => {
    const minPrice = request.query.minPrice;
    const maxPrice = request.query.maxPrice;
    if( maxPrice < 0){
        return response.status(400).json({
            status:"Bad request",
            message: "Max price is not valid!"
        })
    }
    if( minPrice < 0){
        return response.status(400).json({
            status:"Bad request",
            message: "Min price is not valid!"
        })
    }

    const condition = {
        buyPrice: {$gte: minPrice, $lte: maxPrice}
    }

    productModel.find(condition).exec((error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error!",
                message: error.message
            })
        } 
        return response.status(200).json({
            statusbar:"Get Product Filter By Price Successful!",
            data: data
        })
    })
}
const getProductWithFilterProductType =  (request, response) => {
    const Type = request.query.type;
    if( Type  === undefined){
        return response.status(400).json({
            status:"Bad request",
            message: "type is not valid!"
        })
    }
    const condition = {
        type: Type
    }
    productModel.find(condition).exec((error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error!",
                message: error.message
            })
        } 
        return response.status(200).json({
            statusbar:"Get Product Filter By Type Successful!",
            data: data
        })
    })
}

const getProductEveryFilterOption = (request, response) => {
    const productName = request.query.name;
    const minPrice = request.query.minPrice;
    const maxPrice = request.query.maxPrice;
    const productType = request.query.type;

    let condition = {};

    if (productName !== undefined) {
         condition.name = { $regex: productName } ;
    }
    
    if (minPrice && maxPrice !== undefined) {
        condition.buyPrice = { $gte: minPrice, $lte: maxPrice } ;
    }
    
    if (productType !== undefined) {
        condition.type = productType;
    }
    productModel.find(condition).exec((error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error!",
                message: error.message
            })
        } 
        return response.status(200).json({
            statusbar:"Get Product Filter Successful!",
            data: data
        })
    })
    productModel.find
}
const getProductInPanigation = async (request, response) => {
    const skipPage = request.query.skipPage;
    const limit = request.query.limitProduct;
    const productName = request.query.name;
    const minPrice = request.query.minPrice;
    const maxPrice = request.query.maxPrice;
    const productType = request.query.type;

    let condition = {
        _Delete: false
    };

    if (productName !== undefined) {
         condition.name = { $regex: productName } ;
    }
    
    if (minPrice && maxPrice !== undefined) {
        condition.buyPrice = { $gte: minPrice, $lte: maxPrice } ;
    }
    
    if (productType !== undefined) {
        condition.type = productType;
    }

    if(!skipPage){
        return response.status(400).json({
            status:"Bad request",
            message:"Skip page is not defined!"
        })
    }
    if(!limit){
        return response.status(400).json({
            status: "Bad request",
            message: "Limit product is not defined!"
        })
    }

    const productData = await productModel.find();
    productModel.find().populate("type").skip(skipPage*limit).limit(limit).exec((error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error!",
                message: error.message
            })
        } 
        return response.status(200).json({
            status:"Get Product with Panigation Successful!",
            data: data,
            length: productData.length
        })
    })
}

//soft Delete Product
const softDeleteProduct = async(request, response) => {
    const productId = request.params.productId;

    if(!mongoose.Types.ObjectId.isValid(productId)){
        return response.status(400).json({
            status:"BAD REQUEST",
            message: "product Id is not valid"
        })
    }

    productModel.findByIdAndUpdate(productId, {_Delete: true} , (error, data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error!",
                message: error.message
            })
        } 
        return response.status(200).json({
            status: "Soft Delete Product Successful!",
            data: data
        })
    })
}

const uploadImage = async(request,response) => {
    try{
        const fileData = request.file;
        
        return response.status(200).json({
            data: fileData.path
        })
    }catch(error){
        console.log(error);
        return response.status(500).json({
            message: error.message
        })
    }
}

const deleteCloudinaryProductImage = async(request, response) => {
    const publicId = request.body.id;

    cloudinary.uploader.destroy(publicId, (result, error)  => {
        if(result.result === 'ok'){
            return response.status(202).json({
                status:"Successfully delete Image from cloudinary",
                data: result
            })
            
        }
        return response.status(500).json({
            status:"Internal server error",
            message: error
         })
    })
}
module.exports= {
    createProduct,
    getAllProduct,
    getProductByID,
    updateProductById,
    deleteProductById,
    getProductWithLimit,
    getProductWithFilterName,
    getProductWithFilterPrice,
    getProductWithFilterProductType,
    getProductInPanigation,
    getProductEveryFilterOption,
    softDeleteProduct,
    uploadImage,
    deleteCloudinaryProductImage
}