const customerModel = require("../models/Customer");
const refreshTokenModel = require("../models/refreshToken");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const { stringify } = require("querystring");

const handleRefreshToken = async (req, res) => {
  const cookies = req.cookies;

  if (!cookies?.jwt) return res.sendStatus(401);

  const refreshToken = cookies.jwt;
  const CustomerID = await refreshTokenModel.find({ token: refreshToken });
  const foundCustomer = await customerModel.findById(CustomerID[0].customer);
  if (!foundCustomer) return res.sendStatus(403);
  //evaluate jwt
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, decoded) => {
    if (err || foundCustomer.email !== decoded.email)
      return res.sendStatus(403);
    const roles = Object.values(foundCustomer.role);
    const accessToken = jwt.sign(
      {
        UserInfo: {
          email: foundCustomer.email,
          role: roles,
        },
      },
      process.env.ACCESS_TOKEN_SECRET,
      { expiresIn: "120s" }
    );
    res.json({ accessToken, email: decoded.email });
  });
};

module.exports = {
  handleRefreshToken,
};
