const customerModel = require("../models/Customer");
const refreshTokenModel = require("../models/refreshToken");
require ("dotenv").config();


//when Client Logout
const handleLogoutCustomer = async(req, res) => {
    //take the cookies
    const cookies = req.cookies;
    if(!cookies?.jwt) return res.sendStatus(204) //No content
    const refreshToken = cookies.jwt;

    //Is refreshToken is in DB
    const CustomerId = await refreshTokenModel.find({token: refreshToken});
    const Customer = await customerModel.findById(CustomerId[0].customer);
    if(!Customer || !CustomerId){
        res.clearCookie("jwt", {httpOnly: true})
        return res.sendStatus(204) // successful but no content
    }
    //Delete refreshToken in db
    customerModel.findByIdAndDelete({refreshToken: ""});
    refreshTokenModel.findOneAndDelete({token: refreshToken});
    //clear the cookie
    res.clearCookie("jwt", {httpOnly: true, secure: true, sameSite:"none"}); 
    res.clearCookie("refreshTokenIsExist", {
        sameSite: "None",
        secure: true,
    })
    res.sendStatus(204)
}

module.exports = {
    handleLogoutCustomer
}