const successPurchase = require("../models/successPurchase");
const order = require("../models/order");

const takeListSuccessPurchase = async (req, res) => {
    try{
        const data = await successPurchase.find();
        return res.status(200).json({
            data: data
        })
    }catch(error) {
        console.log(error)
        return res.sendStatus(500);
    }
}

const creatAllExistSuccessPurchase = async(req, res) => {
    try{
        const data =  await successPurchase.deleteMany();
        console.log(data);
        const successOrder = await order.find({ status: "Confirmed"});
        await successOrder.forEach(element => {
            successPurchase.create({order: element._id})
        });
        return res.sendStatus(201);
    }catch(error){
        console.log(error);
        return res.sendStatus(500)
    }
}
module.exports= {
    takeListSuccessPurchase,
    creatAllExistSuccessPurchase
}