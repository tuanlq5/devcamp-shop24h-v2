const customer = require("../models/Customer");
const mongoose = require("mongoose");
const userActivities = require("../models/userActivities");
const googleAccount = require("../models/GoogleaAccountInfo");


const addActivitiesToAllAccount = async(req, res) => {
    try{
        const allCustomer = await customer.find();

        await allCustomer.forEach(element => {
            const newUserActivities = {
                userID: element._id
            };
            userActivities.create(newUserActivities)
        });
        return res.sendStatus(200)
    }catch(error){
        return res.sendStatus(500)
    }
}

const getAllUserAcitvities = async(req, res) => {
    try {
        const data = await userActivities.find()
        .populate({
            path:"cartActivities",
            populate:{
                path:"addProductToCart", 
            }
        })
        .populate({
            path:"cartActivities",
            populate:{
                path:"removeProductCart", 
            }
        })
        return res.status(200).json(
            data
        );
    } catch (error) {
        return res.sendStatus(500)
    }
}
module.exports = {
    addActivitiesToAllAccount,
    getAllUserAcitvities
}