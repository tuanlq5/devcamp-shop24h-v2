const mongoose = require("mongoose");

const VoucherModel = require("../models/voucher");


//Get Voucher With Panigation
const getVoucherWithPanigation = async(request, response) => {
    // B1: Chuẩn bị dữ liệu
    let skip = request.query.skip;
    let limit = request.query.limit;
    if(skip === undefined){
        skip = 0
    }
    if(limit === undefined){
        limit = 0
    }
    let condition = {
        // _Delete: false
    };
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    const voucherLength = await VoucherModel.find();

    VoucherModel.find(condition).skip(skip*limit).limit(limit).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher successfully",
            data: data,
            length: voucherLength.length,
        })
    })
}
//call back function create order
const createVoucher = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    const body = request.body;

    if(!body.voucherCode){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "voucherCode không hợp lệ"
        })
    }
    if(body.voucherCode.length > 6 || body.voucherCode.length < 6){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "voucherCode must be 6 digit"
        })
    }
    if(!body.discountPercent){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "discountPercent chưa được nhập"
        })
    }
    if(isNaN(body.discountPercent)){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "discountPercent phải là số"
        }) 
    }
    if(body.discountPercent < 0){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "discountPercent phải là số dương"
        }) 
    }
    if(body.discountPercent === "" || body.discountPercent === " " || body.discountPercent === true || body.discountPercent === null ){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "discountPercent phải là số"
        }) 
    }

    const newVoucher = {
        voucherCode: body.voucherCode,
        discountPercent: body.discountPercent
    }

    VoucherModel.create(newVoucher, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Successfully create new voucher!",
            data: data
        })
    })
}

//call back get all voucher
const getAllVoucher = (request, response) => {
    VoucherModel.find((error, data)=>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get All voucher successfull",
            data: data
        })
    })
}

//call back get voucher by voucher Code
const getVoucherByVoucherCode = (request, response) => {
    const VoucherCode1 = request.params.voucherCode;
    
    if(VoucherCode1 === undefined){
        return response.status(400).json({
            status: "Bad Request",
            message: "Vouchercode is not valid"
        })
    }

    const condition = {
        voucherCode: VoucherCode1
    }
    VoucherModel.find(condition).exec((error,data)=> {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error!",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get Voucher Succesful!",
            data: data
        })
    })
}

const updateVoucherCode = (request, response) => {
    const body = request.body;
    const voucherId = request.params.voucherId

    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "voucher Id is not valid"
        }) 
    }

    if(!body.voucherCode){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "voucher code is not exist"
        })
    }
    if(!body.discountPercent){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "discount percent is not exist"
        })
    }

    const newVoucher = {
        voucherCode: body.voucherCode,
        discountPercent: body.discountPercent
    }

    VoucherModel.findByIdAndUpdate(voucherId, newVoucher, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        }
        return response.status(201).json({
            message: "Update coupon success",
            data: data
        })
    })
}

const softDeleteVoucher = (request, response) => {
    const voucherId = request.params.voucherId;
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "voucher Id is not valid"
        }) 
    }
    VoucherModel.findByIdAndUpdate(voucherId, {_Delete: true}, (error, data) => {
        if(error){
            return response.status(500).json({
                status: "Internal Server Error",
                message: error.message
            })
        }
        return response.status(201).json({
            message: "Soft Delete coupon success",
        })
    })
}
module.exports= {
    getAllVoucher,
    getVoucherByVoucherCode,
    createVoucher,
    getVoucherWithPanigation,
    updateVoucherCode,
    softDeleteVoucher
}