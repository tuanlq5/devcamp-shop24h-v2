//import thư viện mongoose
const mongoose = require("mongoose");
const brypt = require("bcrypt");
//import Review Model
const customerModel = require("../models/Customer");
const { request, response } = require("express");
const userActivities = require("../models/userActivities");

const getCustomerWithPanigation = async(request, response) => {
    // B1: Chuẩn bị dữ liệu
    let skip = request.query.skip;
    let limit = request.query.limit;
    //  check if it is string or number
    const regex = /^\d+$/;
    if(skip.match(regex) === null || limit.match(regex) === null){
        return response.status(400).json({
            status:"BAD REQUEST",
            message: "skip is not valid"
        })
    }
    
    if(skip === undefined){
        skip = 0
    }
    if(limit === undefined){
        limit = 0
    }

    let condition = {
        _Delete: false
    };
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    const customerLength = await customerModel.find();
  
    customerModel.find().populate("orders").skip(skip*limit).limit(limit).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all customer successfully",
            data: data,
            length: customerLength.length,
        })
    })
}
const createCustomer = async (request, response) =>{
     // B1: Chuẩn bị dữ liệu
     const body = request.body;
     // console.log(body)
        //   {
        //     "fullName": "Le Quang Tuan",
        //     "phone":"0965127997",
        //     "email": "maitostaham@gmail.com",
        //     "password": "Bentaiga@3133",
        //     "address" : "Thủ Đức",
        //     "city": "Sài Gòn",
        //     "country": "Việt Nam"
        //     "administrativeDivision": "Ho Chi Minh"
        //  }
     // B2: Validate dữ liệu
    if(!body.fullName){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "FullName không hợp lệ" 
        })
    }
    if(!body.phone){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Phone không hợp lệ" 
        })
    }
    if(!body.email){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Email không hợp lệ" 
        })
    }
    if(!body.password){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Password is empty!"
        })
    };
        //hash the password
        const hashedPwd = await brypt.hash(body.password, 10);

        // B3: Thao tác với cơ sở dữ liệu
        const newCustomer = {
            id: mongoose.Types.ObjectId,
            fullName: body.fullName,
            phone:  body.phone,
            email: body.email,
            password: hashedPwd,
            address: body.address,
            city:body.city,
            country: body.country,
            administrativeDivision: body.administrativeDivision
        };

        customerModel.create(newCustomer, (error, data) => {
            if(error)
            {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            } 
            const newUserActivities = {
                user: data._id
            }
            userActivities.create(newUserActivities)
            return response.status(201).json({
                status: "Create customer type: Successfull",
                data: data
            })
        })
}
const getAllCustomer  = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
    customerModel.find((error,data) => {
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all customer: Successfull",
            data: data
        })
    })
}
const getCustomerByEmail = (req, res) => {
    const emailParam = req.params.email;
    if(!emailParam) return res.sendStatus(400).json({
        message: "Email  không hợp lệ!"
    })
    customerModel.find({email: emailParam}).exec((error, data) => {
        if(error){
            return res.status(500).json({
                message: "Internal server error"
            })
        }
        return res.status(200).json({
            status: "Get customer successfull",
            data: data
        })
    })
}
const getCustomerByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const customerId =  request.params.customerId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
     return response.status(400).json({
             status: "Bad request",
             message: "customerId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
     customerModel.findById(customerId,(error,data) =>{
         return response.status(200).json({
             status: "Get detail customer successfull",
             data: data
             })
         }
     )  
}
const updateCustomerById =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const customerId =  request.params.customerId;
    const body = request.body;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(customerId)){
        return response.status(400).json({
                status: "Bad request",
                message: "CustomerId không hợp lệ" 
            })
        }
        
    if(body.bodyFullName !== undefined && body.bodyFullName.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "FullName không hợp lệ" 
        })
    }
    if(body.bodyPhone !== undefined && body.bodyPhone.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Phone không hợp lệ" 
        })
    }
    if(body.bodyEmail !== undefined && body.bodyEmail.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "Email không hợp lệ" 
        })
    } 
    if(body.administrativeDivision !== undefined && body.administrativeDivision.trim() === ""){
        return response.status(400).json({
            status: "BAD REQUEST",
            message: "administrativeDivision không hợp lệ" 
        })
    }
    const updateCustomer = {
        fullName: body.fullName,
        phone:  body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
        administrativeDivision: body.administrativeDivision,
        zipcode: body.zipcode
    }
    //bước 3: gọi model tạo dữ liêu
    customerModel.findByIdAndUpdate(customerId,updateCustomer,{new: true},(error,data) =>{
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update customer successfull",
                data: data
                })
            }
        )  
    }
    const deleteCustomerById = (request, response) => {
        //B1: chuẩn bị dữ liệu
        const customerId =  request.params.customerId;
    
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(customerId)){
            return response.status(400).json({
                status: "Bad Request",
                message: "Customer ID không hợp lệ"
            })
        } 
        //B3:  Gọi model tạo dữ liệu
        customerModel.findByIdAndDelete(customerId, (error, data)=> {
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(204).json({
                status: "Delete customer successfully"
            })
        })
    }

    const softDeleteCustomerById = (request, response) => {
        //B1: chuẩn bị dữ liệu
        const customerId =  request.params.customerId;
    
        //B2: Validate dữ liệu
        if(!mongoose.Types.ObjectId.isValid(customerId)){
            return response.status(400).json({
                status: "Bad Request",
                message: "Customer ID không hợp lệ"
            })
        } 
        //B3:  Gọi model tạo dữ liệu
        customerModel.findByIdAndUpdate(customerId,{_Delete: true}, (error, data)=> {
            if(error){
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }
            return response.status(204).json({
                status: "Soft Delete customer successfully"
            })
        })
    }
module.exports = {
    createCustomer,
    getAllCustomer,
    getCustomerByID,
    updateCustomerById,
    deleteCustomerById,
    getCustomerByEmail,
    getCustomerWithPanigation,
    softDeleteCustomerById,
}