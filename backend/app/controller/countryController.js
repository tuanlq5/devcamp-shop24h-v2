const countryModel = require("../models/country");

const getCountryData = (req, res) => {
    countryModel.find((error, data) => {
        if(error){
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
        return res.status(200).json({
            message: "Get all Country successful",
            data: data
        })

    })
}

module.exports = {
    getCountryData
}