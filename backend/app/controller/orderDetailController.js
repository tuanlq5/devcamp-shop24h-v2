//import thư viện mongoose
const mongoose = require("mongoose");

//import orderDetail & order Model
const orderModel = require("../models/order");
const orderDetailModel = require("../models/orderDetail");


const createOrderDetail = async (request, response) => {
    //B1:  chuẩn bị dữ liệu
    const body = request.body;
    // {
    //    "quantity": 6,
    //    "product": "63950565e2e3df3296ccb144"
    // }
    //B2: Validate dư liệu
    if(!mongoose.Types.ObjectId.isValid(body.product)){
        return response.status(400).json({
            status: "Bad Request",
            message: "product Id is not valid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.orderId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "order Id is not valid"
        })
    }
    if ( isNaN(body.quantity) || body.quantity < 0 || body.quantity === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Quantity không hợp lệ"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    const newOrderDetail = {
        quantity: body.quantity,
        product: body.product,
    }

    //B4: kết quả trả về
    orderDetailModel.create(newOrderDetail,(error, data) =>{
       if(error){
        return response.status(500).json({
            status:"Internal server error",
            message: error.message
        })
       } 
        orderModel.findByIdAndUpdate( body.orderId, 
            {$push: {orderDetail: data._id}},
            {new: true},
            (errorOrder, dataOrder) => {
                if(errorOrder){
                    return response.status(500).json({
                        status:"Internal server error",
                        message: errorOrder.message
                    })
                } 
                return response.status(200).json({
                    message: "create order Detail successful!",
                    data: dataOrder
                })
            }
        )
    })

}
const getAllOrderDetail = (request, response) =>{
    // Bước 1: chuẩn bị dữ liệu
    // Bước 2: validate dữ liệu
    // bước 3: Gọi Model tạo dữ liệu
   orderDetailModel.find((error,data) => {
       if(error){
           return response.status(500).json({
               status: "Internal server error",
               message: error.message
           })
       }
       return response.status(200).json({
           status: "Get all orderDetail: Successfull",
           data: data
       })
   })
}
const getOrderDetailByID =  (request, response) =>{
    // bước 1: chuẩn bị dữ liệu
    const orderDetailId =  request.params.orderDetailId;
    //bước 2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderDetailId)){
     return response.status(400).json({
             status: "Bad request",
             message: "productTypeId không hợp lệ" 
         })
     }
     //bước 3: gọi model tạo dữ liêu
    orderDetailModel.findById(orderDetailId,(error,data) =>{
         return response.status(200).json({
             status: "Get order detail  successfull",
             data: data
             })
         }
     )  
 }
 const getAllOrderDetailOfOrder = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderID = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Order ID không hợp lệ"
        })
    }
    // B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderID)
        .populate("orderDetail")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all orderDetail of order successfully",
                data: data
            })
        })
}
 const updateOrderDetailByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderDetailID = request.params.orderDetailId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderDetailID)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderDetail ID không hợp lệ"
        })
    }
    if ( body.bodyQuantity == undefined && (isNaN(body.bodyQuantity) || body.bodyQuantity > 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Quantity không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newOrderDetail = {
        quantity: body.bodyQuantity
    }

    orderDetailModel.findByIdAndUpdate(orderDetailID, newOrderDetail, {new:true},(error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update order detail successfully",
            data: data
        })
    })
}
const deleteOrderByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderDetailId =  request.params.orderDetailId;
    const orderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }
    if (!mongoose.Types.ObjectId.isValid(orderDetailId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderDetail Id không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    orderDetailModel.findByIdAndDelete(orderDetailId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        orderModel.findByIdAndUpdate(orderId, {
            $pull: { orderDetail: orderDetailId}
        }, (err, data) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
        })
        return response.status(204).json({
            status: "Delete orderDetail successfully"
        })
    })
}
module.exports= {
  createOrderDetail,
  getAllOrderDetail,
  getOrderDetailByID,
  getAllOrderDetailOfOrder,
  updateOrderDetailByID,
  deleteOrderByID
}