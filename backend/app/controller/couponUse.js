
const couponUse = require("../models/couponUse");
const order = require("../models/order");

const createCouponUse = async(req, res) =>{
    try{
        const data = await order.find({couponUse: {$exists: true}}).exec();
        await data.forEach(element => {
            const newCouponUse = {
                order: element._id,
                coupon: element.couponUse
            }
           
            couponUse.create(newCouponUse);
        });
        await couponUse.deleteMany();
        const list = await couponUse.find().populate("order").populate("coupon");
        res.status(200).json({
            data: list
        })
    }catch(error){
        console.log(error);
        res.sendStatus(500)
    }
}

module.exports = {
    createCouponUse
}