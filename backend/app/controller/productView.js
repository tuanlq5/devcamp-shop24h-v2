const productView = require("../models/productView");
const userActivities = require("../models/userActivities");
const mongoose = require("mongoose");


const createProductView = async(req, res) => {
    try{
        const userId = req.params.userId;
        const body =  req.body;
    
        if(!mongoose.Types.ObjectId.isValid(body.product)){
            return res.status(400).json({
                message: "product is not valid!"
            })
        }

        const data = await productView.create(body);
        
        await userActivities.findOneAndUpdate(
            {userID: userId},
            {$push: {productView: data._id}},
            {new: true}
            ) 

        res.sendStatus(201);       
    }catch(error){
        res.sendStatus(500);
        console.log(error);
    }
}

module.exports = {
    createProductView
}