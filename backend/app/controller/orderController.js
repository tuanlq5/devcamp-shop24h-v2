//import thử viện mongoose
const mongoose = require("mongoose");

//import Order model
const orderModel = require("../models/order");
const customerModel  = require("../models/Customer");
const order = require("../models/order");


const changeOrderStatus = (request, response) => {
    const  body = request.body;
    // {
    //     "status": "Confirmed",
    //     "orderId": "642bae1b45ba617283e0502a"
    // }
    const status = ["Confirmed", "Open", "Canceled", "Completed"];
    let statusFound = "";
    for( let i = 0; i < status.length ; i ++){
        if(body.status === status[i]){
            statusFound = status[i]
        }
    }
    if(!mongoose.Types.ObjectId.isValid(body.orderId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Order Id Không hợp lệ"
        })
    }
    if( statusFound === ""){
        return response.status(400).json({
            status: "Bad Request",
            message: "Status không hợp lệ"
        })   
    }
    
    orderModel.findByIdAndUpdate(body.orderId, {status: body.status},{new: true}, (error,data) => 
        {
            if(error){
                return response.status(500).json({
                    status: "Internal Server Error",
                    message: error.message
                })
            }
            return response.status(200).json({
                status: "Update order status successfull",
                data: data
            })
        }
    )
}
const createOrderForGoogleAccount = (req, res) => {
    //B1:  chuẩn bị dữ liệu
    const body = req.body;
    if (isNaN(body.cost) || body.cost < 0 || body.cost === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Cost không hợp lệ"
        })                                                                                                                                                  
    }
    const status = ["Confirmed", "Open", "Canceled", "Completed"];
    let statusFound = "";
    for( let i = 1; i < status.length ; i ++){
        if(body.status === status[i]){
            statusFound = status[i]
        }
    }
    if( statusFound === ""){
        return res.status(400).json({
            status: "Bad Request",
            message: "Status không hợp lệ"
        })   
    }
    //B3: Thao tác với cơ sở dữ liệu
    const newOrder = {
        orderDate : body.date,
        shippedDate: body.shipedDate,
        note: body.note,
        cost: body.cost,
        status: body.status,
        couponUse: body.couponUse,
        uid: body.uid,
        zipCode: body.zipCode
    }
    orderModel.create(newOrder, (error, data)=> {
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message:  error.message
            })
        }
        return res.status(200).json({
            message: "Create Order successful",
            data: data
        })
    })
    
}
const createOrderOfCustomer = (request, response) => {
    //B1:  chuẩn bị dữ liệu
    const body = request.body;
    // {
    //     "shipedDate" : "123",
    //     "note" : "NoThing", 
    //     "cost": 12000,
    //     "status": "Open"
    // }

    //B2: VAlidate dư liệu
    if(!mongoose.Types.ObjectId.isValid(body.customerId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Customer ID không hợp lệ"
        })
    }
    // const regexDateFormat = /^\d{4}-\d{2}-\d{2}$/;
    // if(!body.shipedDate.match(regexDateFormat)){
    //     return response.status(400).json({
    //         status: "Bad Request",
    //         message: "Shiped date format yyyy-mm-dd"
    //     })
    // }
    if (isNaN(body.cost) || body.cost < 0 || body.cost === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "Cost không hợp lệ"
        })                                                                                                                                                  
    }
    const status = ["Confirmed", "Open", "Canceled", "Completed"];
    let statusFound = "";
    for( let i = 1; i < status.length ; i ++){
        if(body.status === status[i]){
            statusFound = status[i]
        }
    }
    if( statusFound === ""){
        return response.status(400).json({
            status: "Bad Request",
            message: "Status không hợp lệ"
        })   
    }
    
    //B3: Thao tác với cơ sở dữ liệu
    const newOrder = {
        orderDate : body.date,
        shippedDate: body.shipedDate,
        note : body.note,
        cost: body.cost,
        status: body.status,
        couponUse: body.couponUse,
        customer: body.customerId
    }
    //B4: kết quả trả về
    orderModel.create(newOrder,(errorOrder, dataOrder) =>{
       if(errorOrder){
        return response.status(500).json({
            status:"Internal server error",
            message: errorOrder.message
            })
        }
        // Thêm ID của order mới vào mảng orders của customer đã chọn
        customerModel.findByIdAndUpdate(body.customerId, {
            $push: {
                orders: dataOrder._id
            }
        },{new:true}, (err, data) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
        })
        return response.status(201).json({
            status: "Create Order Successfully",
            data: dataOrder
        })

    })
}
const getOrderWithPanigate = async(request, response) => {
    // B1: Chuẩn bị dữ liệu
    let skip = request.query.skip;
    let limit = request.query.limit;
    if(skip === undefined){
        skip = 0
    }
    if(limit === undefined){
        limit = 0
    }
    // B2: Validate dữ liệu
    // B3: Gọi Model tạo dữ liệu
    const orderLength = await orderModel.find();
  
    orderModel.find().populate(
        {path:"orderDetail",
        populate:{
            path:"product"
        }
        }
    ).populate("customer").skip(skip*limit).limit(limit).exec((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all order successfully",
            data: data,
            length: orderLength.length,
        })
    })
}
const getAllOrderOfCustomer = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Customer ID không hợp lệ"
        })
    }

    // B3: Thao tác với cơ sở dữ liệu
    customerModel.findById(customerId)
        .populate("orders").populate("customer")
        .exec((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: error.message
                })
            }

            return response.status(200).json({
                status: "Get all order of customer successfully",
                data: data
            })
        })
}
const getOrderById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Order ID không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Get detail order successfully",
            data: data
        })
    })
}
const updateOrderByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const orderID = request.params.orderId;
    const body = request.body;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(orderID)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "OrderID không hợp lệ"
        })
    }

    const regexDateFormat = /^\d{4}-\d{2}-\d{2}$/;
    if(!body.bodyShipedDate.match(regexDateFormat)){
        return response.status(400).json({
            status: "Bad Request",
            message: "Shiped date format yyyy-mm-dd"
        })
    }
    if ( body.bodyCost == undefined && (isNaN(body.bodyCost) || body.bodyCost > 0)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Cost không hợp lệ"
        })
    }
    // B3: Gọi Model tạo dữ liệu
    const newOrder = {
        orderDate : body.bodyDate,
        shippedDate: body.bodyShipedDate,
        note : body.bodyNote,
        cost: body.bodyCost
    }

    orderModel.findByIdAndUpdate(orderID, newOrder, {new:true},(error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Update order successfully",
            data: data
        })
    })
}
const deleteOrderByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const customerId = request.params.customerId;
    const orderId = request.params.orderId;

    // B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(customerId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "Customer Id không hợp lệ"
        })
    }

    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "orderId không hợp lệ"
        })
    }

    // B3: Thao tác với CSDL
    orderModel.findByIdAndDelete(orderId, (error) => {
        if (error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        // Sau khi xóa xong 1 reivew khỏi collection cần cóa thêm reviewID trong course đang chứa nó
        customerModel.findByIdAndUpdate(customerId, {
            $pull: { orders: orderId}
        }, (err, data) => {
            if (err) {
                return response.status(500).json({
                    status: "Internal server error",
                    message: err.message
                })
            }
        })
        return response.status(204).json({
            status: "Delete order successfully"
        })
    })
}
module.exports = {
    createOrderOfCustomer,
    getOrderWithPanigate,
    getAllOrderOfCustomer,
    getOrderById,
    updateOrderByID,
    deleteOrderByID,
    changeOrderStatus,
    createOrderForGoogleAccount
}