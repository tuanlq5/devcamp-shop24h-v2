const  mongoose  = require("mongoose");
const addProduct = require("../models/addProduct");
const removeProduct = require("../models/removeProduct");
const userActivities = require("../models/userActivities");
const nonUserActivities  = require("../models/nonUserActivities");

const addProductCartNonUserActivites = async (req, res) => {
    try{
        const cookies = req.cookies;

        const body = req.body;

        if(isNaN(body.quantity) || body.quantity < 0 || body.quantity === ""){
            return  res.status(400).json({
                message: "Quantity has to be number"
            })
        }
        if(!mongoose.Types.ObjectId.isValid(body.product)){
            return  res.status(400).json({
                message: "product is not valid"
            })
        }
        const data = await addProduct.create(body);

        await nonUserActivities.findOneAndUpdate(
            {sessionId: cookies._nonU}, 
            {$push: {"cartActivities.addProductToCart":data._id}},
            {new: true}
        )

        res.sendStatus(200)
    }catch(error){
        res.sendStatus(500)
        console.log(error);
    }
}

const removeProductCartNonUserActivites = async (req, res) => {
    try{
        const cookies = req.cookies;

        const body = req.body;

        if(isNaN(body.quantity) || body.quantity < 0 || body.quantity === ""){
            return  res.status(400).json({
                message: "Quantity has to be number"
            })
        }
        if(!mongoose.Types.ObjectId.isValid(body.product)){
            return  res.status(400).json({
                message: "product is not valid"
            })
        }
        const data = await removeProduct.create(body);

        await nonUserActivities.findOneAndUpdate(
            {sessionId: cookies._nonU}, 
            {$push: {"cartActivities.removeProductCart":data._id}},
            {new: true}
        )

        res.sendStatus(200)
    }catch(error){
        res.sendStatus(500)
    }
}

const addProductCart = async(req, res) => {
    try{
        const userID = req.params.userID;
        const body = req.body;
        if(isNaN(body.quantity) || body.quantity < 0 || body.quantity === ""){
            return  res.status(400).json({
                message: "Quantity has to be number"
            })
        }
        if(!mongoose.Types.ObjectId.isValid(body.product)){
            return  res.status(400).json({
                message: "product is not valid"
            })
        }
        const data = await addProduct.create(body);

        await userActivities.findOneAndUpdate(
            {userID: userID},
            {$push: {"cartActivities.addProductToCart":data._id}},
            { new: true}
        )
        
        res.sendStatus(200);
    }catch(error)
    {
        return res.status(500)
    }
   
}

const removeProductCart = async(req, res) => {
    try{
        const userID = req.params.userID;
        const body = req.body
      
        if(isNaN(body.quantity) || body.quantity < 0 || body.quantity === ""){
            return res.status(400).json({
                message: "Quantity has to be number"
            })
        }
        if(!mongoose.Types.ObjectId.isValid(body.product)){
            return res.status(400).json({
                message: "product is not valid"
            })
        }
        const data = await removeProduct.create(body);
        await userActivities.findOneAndUpdate(
            {userID: userID},
            {$push: {"cartActivities.removeProductCart":data._id}},
            { new: true}
        )
        res.sendStatus(200);

    }
    catch(error)
    {
        return res.status(500)
    }
}
module.exports = {
    addProductCart,
    removeProductCart,
    addProductCartNonUserActivites,
    removeProductCartNonUserActivites
}