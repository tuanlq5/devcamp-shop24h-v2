const  mongoose  = require("mongoose");
const nonUserActivities = require("../models/nonUserActivities");

const createNonUserActivities = async(req, res) =>{
    try{ 
      
        const cookies = req.cookies;
  
        if(cookies._nonU) return res.sendStatus(201);

        const newNonUserActivities = await nonUserActivities.create({});

        res.cookie("_nonU", newNonUserActivities.sessionId, {
            httpOnly: true,
            sameSite: "None",
            secure: true,
            maxAge: 30 * 24 * 60 * 60 * 1000,
        })
        res.sendStatus(200);
    }catch(error){
        res.sendStatus(500);
        console.log(error);
    }
}

module.exports = {
    createNonUserActivities
}