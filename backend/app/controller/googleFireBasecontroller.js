const admin = require("firebase-admin");
const loginData = require("../models/loginData")
//intialize FireBase Admin SDK
const serviceAccount = require("../../devcamp-firebase-6a51a-firebase-adminsdk-tvj76-47bf7633f6.json");
const googleModel = require ('../models/GoogleaAccountInfo');
const { default: mongoose } = require("mongoose");


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://devcamp-firebase-6a51a-default-rtdb.asia-southeast1.firebasedatabase.app/"
})

//Check Authenticate google Acount with ADmin sdk
const authetocationGoogleAccount = async( req, res) => {
    const {token} = req.body;
    try{
        const decodedToken = await admin.auth().verifyIdToken(token);
        const uid = decodedToken.uid;
        const email  = (await admin.auth().getUser(uid)).email;
        // Create a new user object in your Firebase Realtime Database
        const database = admin.database();
        const usersRef = database.ref('users');
        const newUserRef = usersRef.push();
        newUserRef.set({
        uid: uid,
        });
        const loginGoogleAccount = {
            googleAccount: email
        };
        loginData.create(loginGoogleAccount);
        res.status(200).json({
            message: "Authentication succeded",
            uid: uid
        })
    }catch(error){
        res.status(400).json({
            message: "Authentication failed",
        })
    }
}


//Update info customer with google Account on the proceed check out 
const sendCustomerInfoWithGoogleAccount = (req, res) => {
    const body = req.body;
    // {
    //     "fullName": "Le Quang Tuan",
    //     "phone": "0965127997",
    //     "email": "maitostaham@gmail.com",
    //     "address":"34, 15 Street",
    //     "country":"VN",
    //     "zipcode": "70012",
    //     "city": "tp.Ho Chi Minh",
    //     "administrativeDivision": "Ho Chi Minh"
    //     "order": "642856f7c62e1be617f0cf3c"
    // }
    //validate
    if(!body.order){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "order Id is not valid!"
        })
    }
    
    if(!mongoose.Types.ObjectId.isValid(body.order)){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "order Id is not exist!"
        })
    }
    if(!body.country){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "country is not valid!"
        })
    }
    if(!body.fullName){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "fullname is not valid!"
        })
    }
    if(!body.email){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "email is not valid!"
        })
    }
    if(!body.address){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "address is not valid!"
        })
    }
    if(!body.city){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "city is not valid!"
        })
    }

    if(!body.zipcode){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "zipcode is not valid!"
        })
    }

    if(!body.administrativeDivision){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "administrativeDivision is not valid!"
        })
    }
    if(!body.phone){
        return res.status(400).json({
            status:"BAD REQUEST",
            message: "phone is not valid!"
        })
    }
    const newInfo = {
        fullName: body.fullName,
        phone: body.phone,
        email: body.email,
        address: body.address,
        city: body.city,
        country: body.country,
        administrativeDivision: body.administrativeDivision
    }
    googleModel.create(newInfo, (error, data)=> {
        if(error){
            return res.status(500).json({
                status: "Internal server error",
                message: error.message 
            })
        }
        return res.status(201).json({
            message: "Create new info for google account successful",
            data: data
        })
    })
}
module.exports = {
    authetocationGoogleAccount,
    sendCustomerInfoWithGoogleAccount
}