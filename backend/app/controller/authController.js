const mongoose = require("mongoose");
const customerModel = require("../models/Customer");
const refreshTokenModel = require("../models/refreshToken");
const loginData = require("../models/loginData");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

//When Client Login
const handleLoginCustomer = async (req, res) => {
    try{
        const { email, password } = req.body;
        // Check email and password is type in
        if (!email || !password) {
            return res.status(400).json({
            status: "BAD REQUEST",
            message: "Email or password are invalid!",
            });
        }
        customerModel.findOne({ email: email }).exec(async (error, data) => {
            let match = null;
            if(data){
                match = await bcrypt.compare(password, data.password);
            }else{
                return res.status(400).json({
                    status: "BAD REQUEST",
                    message: "Email or password are invalid!",
                }); 
            }
            if (match) {
                // create login data
                const loginBody = {
                    customerAccount: data._id
                }
                loginData.create(loginBody);
                const roles = Object.values(data.role);
                //create JWTs
                const accessToken = jwt.sign(
                //access Token
                {   
                    UserInfo: {
                        email: data.email,
                        role: roles
                    } 
                },
                process.env.ACCESS_TOKEN_SECRET,
                { expiresIn: "120s" }
                );
                const refreshToken = jwt.sign(
                    //refresh Token
                    { email: data.email },
                    process.env.REFRESH_TOKEN_SECRET,
                    { expiresIn: "7d" }
                );
                const currentCustomer = await customerModel.findOne({ email: email }); //current customer
                const newRefreshToken = await refreshTokenModel.create(
                    //Tạo Object Refresh Token
                    {
                    token: refreshToken,
                    customer: currentCustomer._id,
                    }
                );
                const customerUpdate = await customerModel //update customer with new refresh token
                .findOneAndUpdate(
                { email: email },
                { refreshToken: newRefreshToken._id },
                { new: true }
                )
                .populate("refreshToken");
    
                res.cookie("refreshTokenIsExist", true, {
                    sameSite: "None",
                    secure: true,
                    maxAge: 7 * 24 * 60 * 60 * 1000,
                })
                //set cookie
                res.cookie("jwt", refreshToken, {
                    httpOnly: true,
                    sameSite: "None",
                    secure: true,
                    maxAge: 7 * 24 * 60 * 60 * 1000,
                });
                res.status(200).json({
                    message: "Login successful",
                    accessToken: accessToken
                })
                } else {
                return res.status(401).json({
                    message: "Incorrect Password or Email!",
                });
            }
        });
    }catch(error){
        res.sendStatus(500);
        console.log(error);
    }
};

module.exports = {
  handleLoginCustomer,
};
