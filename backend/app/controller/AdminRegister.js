const customerModel = require("../models/Customer");
const mongoose = require("mongoose");
const brypt = require("bcrypt");
const roleList = require("../config/role_list").ROLE_LIST;
require("dotenv").config();
const createAdminAccount = async(request, response) => {
    try{
        // B1: Chuẩn bị dữ liệu
        const body = request.body;
        // console.log(body)
            //  {
            //     "fullName": "Le Quang Tuan",
            //     "phone":"0965127997",
            //     "email": "maitostaham@gmail.com",
            //     "password": "Bentaiga@3133",
            //     "address" : "Thủ Đức",
            //     "city": "Sài Gòn",
            //     "country": "Việt Nam",
            //     "administrativeDivision": "Ho Chi Minh",
            //     "adminCode": "*******************"
            //  }
        // B2: Validate dữ liệu
        if(!body.fullName){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "FullName không hợp lệ" 
            })
        }

        if(!body.phone){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Phone không hợp lệ" 
            })
        }

        if(!body.email){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Email không hợp lệ" 
            })
        }

        if(!body.password){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Password is empty!"
            })
        };

        if(!body.country){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Country is empty!"
            })
        }

        if(!body.city){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "City is empty!"
            })
        }

        if(!body.administrativeDivision){
            return response.status(400).json({
                status: "BAD REQUEST",
                message: "Administrative Division is empty!"
            })
        }

        if(body.adminCode !== process.env.ADMIN_SECRET_CODE ){
            return response.status(400).json({
                status: "BAD REQUEST",
                message:" Invalid adminCode!"
            })
        }
            //hash the password
            const hashedPwd = await brypt.hash(body.password, 10);
            // B3: Thao tác với cơ sở dữ liệu
            const newCustomer = {
                id: mongoose.Types.ObjectId,
                fullName: body.fullName,
                phone:  body.phone,
                email: body.email,
                password: hashedPwd,
                address: body.address,
                city:body.city,
                country: body.country,
                administrativeDivision: body.administrativeDivision,
                role: roleList
            };
            await customerModel.create(newCustomer)
            return response.status(201).json({
                status: "Create Admin Successfull"
            });
    }catch(error){
        return response.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

module.exports = {
    createAdminAccount
}